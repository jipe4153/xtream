
#
# dependency on stdutils (headers)
include(CMakeFindDependencyMacro)
find_dependency(stdutils)
include("${CMAKE_CURRENT_LIST_DIR}/xtreamTargets.cmake")
message("-- Found Xtream version ${xtream_VERSION} link targets with xtream")

