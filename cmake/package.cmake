
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

set (xtream_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set (xtream_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set (xtream_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(xtream_VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH})

#
# Head files to use
#
file(GLOB HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.cu*")

set_property(TARGET xtream PROPERTY INTERFACE_xtream_MAJOR_VERSION ${xtream_VERSION_MAJOR})
set_property(TARGET xtream PROPERTY INTERFACE_xtream_MINOR_VERSION ${xtream_VERSION_MINOR})
set_property(TARGET xtream PROPERTY INTERFACE_xtream_PATCH_VERSION ${xtream_VERSION_PATCH})
set_property(TARGET xtream APPEND PROPERTY COMPATIBLE_INTERFACE_STRING xtream_MAJOR_VERSION)

#
# Install files:
#
install(TARGETS xtream EXPORT xtreamTargets
  ARCHIVE DESTINATION lib/xtream-${xtream_VERSION}
  INCLUDES DESTINATION include/xtream-${xtream_VERSION}
)
#
# All the header sources get installed to include/xtream-*/
#
install(
  FILES
    ${HEADER_FILES}
  DESTINATION
    include/xtream-${xtream_VERSION}
  COMPONENT
    Devel
)
#
# Create a config file:
#
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/xtream-${xtream_VERSION}/xtreamConfigVersion.cmake"
  VERSION ${xtream_VERSION}
  COMPATIBILITY AnyNewerVersion
)
#
# @TODO: document
#
export(EXPORT xtreamTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/xtream-${xtream_VERSION}/xtreamTargets.cmake"
)
#
# Top-level config
configure_file(${PROJECT_SOURCE_DIR}/cmake/xtreamConfig.cmake
  "${CMAKE_CURRENT_BINARY_DIR}/xtream-${xtream_VERSION}/xtreamConfig.cmake"
  COPYONLY
)
#
# Install config and version files
#
set(ConfigPackageLocation lib/cmake/xtream-${xtream_VERSION})
install(EXPORT xtreamTargets
  FILE
    xtreamTargets.cmake
  DESTINATION
    ${ConfigPackageLocation}
)
install(
  FILES
    ${PROJECT_SOURCE_DIR}/cmake/xtreamConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/xtream-${xtream_VERSION}/xtreamConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
  COMPONENT
    Devel
)

#
# Create a binary package installer: (ex .deb)
#
# build a CPack driven installer package
include (InstallRequiredSystemLibraries)


#
# Get the machine arch name (ex i386-i686, amd64, x86_64, arm, arm64, etc)
set (UNAME_CMD "uname")
set (UNAME_ARG "-m")
execute_process(COMMAND ${UNAME_CMD} ${UNAME_ARG}
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    RESULT_VARIABLE UNAME_RESULT
    OUTPUT_VARIABLE UNAME_MACHINE)
string(REGEX REPLACE "\n$" "" UNAME_MACHINE "${UNAME_MACHINE}")


set(PACKAGE_GENERATOR "DEB" CACHE STRING "Tells CPACK which package generator to use")

set(CPACK_SET_DESTDIR "on")
set(CPACK_PACKAGING_INSTALL_PREFIX "/tmp")
set(CPACK_GENERATOR ${PACKAGE_GENERATOR})
 
set(CPACK_PACKAGE_DESCRIPTION "xtream CUDA/C++ productivity library")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${CPACK_PACKAGE_DESCRIPTION}")
set(CPACK_PACKAGE_VENDOR "HPC Sweden")
set(CPACK_PACKAGE_CONTACT "jimmy@hpcsweden.se")
set (CPACK_RESOURCE_FILE_LICENSE  
     "${PROJECT_SOURCE_DIR}/LICENSE.txt")
set (CPACK_PACKAGE_VERSION_MAJOR "${xtream_VERSION_MAJOR}")
set (CPACK_PACKAGE_VERSION_MINOR "${xtream_VERSION_MINOR}")
set (CPACK_PACKAGE_VERSION_PATCH "${xtream_VERSION_PATCH}")

set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}_${MAJOR_VERSION}.${MINOR_VERSION}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "gcc (>= 4.8), g++ (>=4.8)")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
set(CPACK_DEBIAN_ARCHITECTURE ${UNAME_MACHINE})

set(CPACK_PACKAGE_NAME xtream)
set(CPACK_PACKAGE_VERSION ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH})
set(CPACK_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}_${UNAME_MACHINE}")

include (CPack)












