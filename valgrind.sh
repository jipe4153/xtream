#/bin/bash!
valgrind --suppressions=valgrind_suppressions_external --leak-check=full --show-leak-kinds=all --log-file=valgrind-out.txt $1 $2
