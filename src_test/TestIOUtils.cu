/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "HostBuffer.cuh"
#include "IOUtils.cuh"

using namespace xpl;
using namespace std;


#include <vector>


TEST(IOUtils, readWriteInt)
{
	std::string tmpFileName = "/tmp/TestReadWriteInt.raw";

	Size sz(480,640);

	// Create buffer with values:
	HostBuffer<int> buff(sz);
	int iter = 0;
	buff.for_each([&](int& val){ val = iter; iter++;});

	// Write
	utils::writeRaw(buff, tmpFileName);
	//Read
	HostBuffer<int> ref_buf = utils::readRaw<int>(tmpFileName);
	test_utils::assert_equal(buff,ref_buf);
}
TEST(IOUtils, readWriteUint8)
{
	std::string tmpFileName = "/tmp/TestReadWriteUint8.raw";

	Size sz(480,640);

	// Create buffer with values:
	HostBuffer<uint8_t> buff(sz);
	int iter = 0;
	buff.for_each([&](uint8_t& val){ val = iter; iter++;});

	// Write
	utils::writeRaw(buff, tmpFileName);
	//Read
	HostBuffer<uint8_t> ref_buf = utils::readRaw<uint8_t>(tmpFileName);
	test_utils::assert_equal(buff,ref_buf);
}