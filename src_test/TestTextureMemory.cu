/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>

#include "TextureMemory.cuh"
#include "DeviceMemory.cuh"

#include "device_memcpy.cuh"

#include "device_lambda.cuh"

using namespace xpl;
 
using namespace std;


TEST(TextureMemory, Allocate)
{
	const int rows = 512;
	const int cols = 512;

	for(int r = 0; r < rows; r+=47)
		for(int c = 0; c < cols; c+=17)
		{
			Size sz(rows,cols);
			// allocated
			TEST_CUDA_ERROR_ASSERT();
			TextureMemory<float>* tex_mem = new TextureMemory<float>(sz);
			TEST_CUDA_ERROR_ASSERT();
			// deacllocate
			delete tex_mem;
			TEST_CUDA_ERROR_ASSERT();
			
		}
}
void Device2TextureCopyTest(const int rows, const int cols)
{
	//
	// Setup a reference buffer on the host:
	HostBuffer<float> h_ref(rows, cols);

	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
		{
			h_ref(i,j) = float(j+i*cols);
		}
	}
	TEST_CUDA_ERROR_ASSERT();

	//
	// Setup a DeviceMemory buffer and a TextureMemory buffer
	//
	Size sz(rows,cols);
 	auto d_a_ptr = std::make_shared<DeviceMemory<float> >(sz);
 	auto tex_a_ptr = std::make_shared<TextureMemory<float> >(sz);
 	//
 	// Setup a device buffer to easily be able to manipulate device memory
 	//
 	DeviceBuffer<float> a_buff(d_a_ptr);
 	//
 	// Fill upp de device memory with reference memory
 	a_buff = h_ref;

	TEST_CUDA_ERROR_ASSERT();
	//
	// Memcpy from device memory to texture memory
    device::memcpy(*tex_a_ptr, *d_a_ptr);
 
	TEST_CUDA_ERROR_ASSERT(); 
	
	HostBuffer<float> h_texture(rows, cols);

	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
		{
			h_texture(i,j) = tex_a_ptr->getValueAt(i,j,0);
		}
	}

	TEST_CUDA_ERROR_ASSERT();
	test_utils::assert_equal(h_ref, h_texture);
	TEST_CUDA_ERROR_ASSERT();
}

TEST(TextureMemory, Device2TextureCopy)
{
	Device2TextureCopyTest(64,64);
	Device2TextureCopyTest(64,64);
	Device2TextureCopyTest(67,17);
	Device2TextureCopyTest(509,23);
	Device2TextureCopyTest(200,1);
	Device2TextureCopyTest(1,200);
	Device2TextureCopyTest(768,799);
}

TEST(TextureMemory, Texture2DeviceCopy)
{

	const int rows = 512;
	const int cols = 512;

	Size sz(rows,cols);

	DeviceMemory<float> d_a(sz);
	TextureMemory<float> tex_a(sz);


	TEST_CUDA_ERROR_ASSERT();

	device::memcpy(d_a, tex_a);

	TEST_CUDA_ERROR_ASSERT();

}
/**
*	Test referencing a region of texture memory, copy data from it 
*/
#include "device_memcpy.cuh"
void TestRegionsReference(int rows, int cols)
{

	//
	// Setup a device buffer:
	DeviceBuffer<float> in(rows, cols);
	DeviceBuffer<float> in2(rows, cols);

	TEST_CUDA_ERROR_ASSERT();

	//#define stdx::lambdaKernel [=] __device__ () mutable

	//typedef [=] __device__ () mutable stdx::lambdaKernel
	// Set each point to some value:
	auto kernel = [=] __device__ () mutable
	{

		int i = device::getY();
		int j = device::getX();

		if (i < in.rows() && j < in.cols() )
		{
			in(i,j) = float(j);
		}

	};
	device::execute(DeviceGrid(in.size()),kernel);
	in.stream().synchronize();

	DeviceMemory<float>& dev_memory = in.memory();
	DeviceMemory<float>& dev_memory2 = in2.memory();

	// Sub region of device memory
	Region r( 0, rows-1, cols/2, cols-1);
	//Region r( 0, rows-1, 0, cols-1);

	std::shared_ptr<DeviceMemory<float> > sub_region = dev_memory.referenceRegion(r);

	TextureMemory<float> tex_mem(in.size());
	TextureStruct<float> tex_struct(tex_mem);

	auto setTextureZero = [=] __device__ () mutable
	{
		int i = device::getY();
		int j = device::getX();

		if (i < tex_struct.rows() && j < tex_struct.cols() )
			tex_struct.write(0.0f, i, j, 0 );

	};
	device::execute( DeviceGrid(in.size()) , setTextureZero);


	TEST_CUDA_ERROR_ASSERT();

	std::shared_ptr<TextureMemory<float> > tex_sub_region = tex_mem.referenceRegion(r);

	TEST_CUDA_ERROR_ASSERT();

	// Sometimes works
	device::memcpy( *tex_sub_region, *sub_region);


	TEST_CUDA_ERROR_ASSERT();

	DeviceStream().synchronize();

	for(int i = 0; i < rows; i++)
	{
		for(int j = cols/2; j < cols; j++)
		{
			ASSERT_TRUE( test_utils::equal( tex_mem.getValueAt(i,j,0), float(j) ) );
		}
	}

	TEST_CUDA_ERROR_ASSERT();


}

TEST(TextureMemory, RegionReference)
{

	int rows = 8;
	int cols = 8;

	TestRegionsReference(rows,cols);

}

