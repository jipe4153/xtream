/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>

#include "PinnedBuffer.cuh"

using namespace xpl;
 
using namespace std;


template<class DeviceBufferType>
__global__ void increment_kernel(DeviceBufferType ds)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if( x < ds.cols() && y < ds.rows())
	{
		float val = ds(x,y,0);
		val = val + 1.0f;
		ds(x,y,0) = val;
	}
}

TEST(PinnedBuffer, AllocateAndFree)
{

	uint rows = 512;
	uint cols = 512;
	const float REF_VAL = 42.0f;

	PinnedBuffer<float> pinned_buffer(rows,cols);

	TEST_CUDA_ERROR_ASSERT();

	// Increment all the values:
	for (uint i = 0; i < rows; ++i)
	{
		for (uint j = 0; j < cols; ++j)
		{
			pinned_buffer(i,j) = REF_VAL;
		}
		/* code */
	}
	//
	// Increment values on the device:
	// setup kernel
	const int DIM_X = 64;
	const int DIM_Y = 8;
	int X_BLOCKS = (DIM_X + cols -1) / DIM_X;
	int Y_BLOCKS = (DIM_Y + rows -1) / DIM_Y;
	dim3 grid(X_BLOCKS, Y_BLOCKS);
	dim3 block(DIM_X, DIM_Y);

	increment_kernel<<<grid,block>>>(pinned_buffer);
	//
	// Make sure host is synchronized with the device:
	pinned_buffer.stream().synchronize();
	//
	// Check all values of pinned buffer:
	//
	test_utils::assert_equal(pinned_buffer, REF_VAL+1.0f );

	TEST_CUDA_ERROR_ASSERT();
}
TEST(PinnedBuffer, CopyHostConstructor)
{
	uint rows = 44;
	uint cols = 33;
	uint depth = 3;

	HostBuffer<float> host(rows,cols,depth);
	host.for_each([&](float& val){val=float(std::rand()); });

	PinnedBuffer<float> pinned(host);

	for(uint k = 0; k < depth; k++)
		for(uint i = 0; i < rows; i++)
			for(uint j = 0; j < cols; j++)
			{
				float val = pinned(i,j,k);
				bool ok = test_utils::equal(val, host(i,j,k));
				ASSERT_TRUE(ok) << "device = " << val << " host = " << host(i,j,k);
			}

}



