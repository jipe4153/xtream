/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <memory>
#include <stdio.h>  
#include <stdlib.h>
#include <random>

using namespace std;

namespace test_utils
{

	template<class T>
	class BasicRand
	{
	public:
		BasicRand()
		{
			rd = make_shared< random_device>();
			gen = make_shared< mt19937 >( (*rd)() );
			normal_dist = make_shared< std::normal_distribution<T> >(0.5, 0.23);

			uniform_dist = make_shared< std::uniform_real_distribution<T> >(0,1);

		}

		T nextNormalDistValue()
		{
			return (*normal_dist)(*gen);
		}
		T nextUniformDistValue()
		{
			return (*uniform_dist)(*gen);
		}

	private:
		
		std::shared_ptr< std::random_device> rd;
		std::shared_ptr< std::mt19937 > gen;
		std::shared_ptr< std::normal_distribution<T> > normal_dist;
		std::shared_ptr< std::uniform_real_distribution<T> > uniform_dist;

	};
} // namespace utils
