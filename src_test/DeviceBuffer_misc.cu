/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "Position.cuh"
#include "Size.cuh"
#include "DeviceBuffer.cuh"

#include "cuda_profiler_api.h"

using namespace xpl;

TEST(DeviceBuffer, StdString)
{


	DeviceBuffer<float> a(Size(4,4));

	for(int i = 0; i < 4; i++)
		for(int j = 0; j < 4; j++)
			a(i,j) = 42.0f;


	std::string str = (std::string)a;

	ASSERT_TRUE( str.length() > 0);


}