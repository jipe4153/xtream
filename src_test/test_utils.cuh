#include <stdio.h>
#ifdef _WIN32
#include <windows.h>
#include <winbase.h>
#else
#include <sys/time.h>
#endif

namespace xpl
{
	namespace test_utils
	{
		#ifdef _WIN32
		/**
		*	@return time in seconds
		*/
		inline double get_clock()
		{
			LARGE_INTEGER ticksPerSecond;
			LARGE_INTEGER timeStamp;
			QueryPerformanceFrequency(&ticksPerSecond);
			QueryPerformanceCounter(&timeStamp);
			return double(timeStamp.QuadPart)/(double)ticksPerSecond.QuadPart; // returns timestamp in secounds
		};
		#else
		/**
		*	@return time in seconds
		*/
		inline double get_clock()
		{
			timeval current_time;
			gettimeofday(&current_time, NULL);
			double current_time_in_ms = current_time.tv_sec * 1000.0 + current_time.tv_usec / 1000.0;
			return current_time_in_ms / 1000.0;
		};
		#endif

	}

}