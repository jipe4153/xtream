/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "Position.cuh"
#include "Size.cuh"
#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "BasicRand.cuh"

#include "CheckEqual.cuh"

#include "cuda_profiler_api.h"

using namespace xpl;
 
using namespace std;


TEST(DeviceBuffer, RegionCopy3D)
{

	//cudaDeviceReset();

	for(int iter=0; iter < 4; iter++)
	{

		TEST_CUDA_ERROR_ASSERT();

		const int rows = 4;
		const int cols = 4;
		const int depth = 2;   

		DeviceBuffer<float> a(rows, cols, depth);
		DeviceBuffer<float> b(a.size());

		TEST_CUDA_ERROR_ASSERT();

		const float A_VAL = 1.0f;
		const float B_VAL = 2.0f;
		//
		// Note: this way of initializing single values to the gpu is of course very slow
		for(int i = 0; i < rows; i++)
			for(int j = 0; j < cols; j++)
				for(int k = 0; k < depth; k++)
				{
					a(i,j,k) = A_VAL;
					b(i,j,k) = B_VAL;
				}

		cudaDeviceSynchronize();  

		TEST_CUDA_ERROR_ASSERT();
		// Define a region
		Region a_region(0, rows-1, 0, cols-1, 1, 1);
		//Region a_region(0, rows-1, 0, cols-1, 0, 0);
		Region b_region(0, rows-1, 0, cols-1, depth-1, depth-1);
		//
		// copy from one region to another:
		a(a_region) = b(b_region);

		TEST_CUDA_ERROR_ASSERT();

		cudaDeviceSynchronize();
		
		//DeviceStream().synchronize();

		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
				for(uint k = 0; k < a.depth(); k++)
				{

					if( a_region.isInside(Position(j,i,k)) == true ) // inside region should be changed
					{
						float a_val = a(i, j, k) ;
						ASSERT_FLOAT_EQ(a_val, B_VAL) << "at (i,j,k) = " << i << ", " << j << " , " << k << std::endl;	  
					}
					else // OUtside region should be unchanged
					{
						float a_val = a(i, j, k) ;
						ASSERT_FLOAT_EQ(a_val, A_VAL) << "at (i,j,k) = " << i << ", " << j << " , " << k << std::endl;
					}

				}

		TEST_CUDA_ERROR_ASSERT();

	}
}

TEST(DeviceBuffer, RegionCopy3DLarge)
{

	TEST_CUDA_ERROR_ASSERT();

	const int rows = 128;
	const int cols = 128;
	const int depth = 36;   

	DeviceBuffer<float> a(rows, cols, depth);
	DeviceBuffer<float> b(a.size());


	HostBuffer<float> h_a(a.size());

	const float A_VAL = 13.0f;

	for(uint i = 0; i < rows; i++)
		for(uint j = 0; j < cols; j++)
			for(uint k = 0; k < depth; k++)
			{
				h_a(i,j,k) = A_VAL;
			}

	// Copy to device:
	a = h_a;
	//
	// Do region copy:
	Region b_reg(0,rows-1,  0,cols-1, 17, 32);

	b(b_reg) = a(Region(0,rows-1,  0,cols-1, 1, 16));

	TEST_CUDA_ERROR_ASSERT();
	//
	// Validate b_region
	// Transfer to a host buffer:
	HostBuffer<float> small_region(b_reg.size() );
	small_region = b(b_reg);

	test_utils::assert_equal(  small_region ,  A_VAL );
}

TEST(DeviceBuffer, RegionCopy2D)
{
	TEST_CUDA_ERROR_ASSERT();

	DeviceBuffer<float> a(Size(8,8));
	DeviceBuffer<float> b(Size(8,8));

	TEST_CUDA_ERROR_ASSERT();


	const float B_VAL = 1.234f;

	for(int i = 0; i < 8; i++)
		for(int j = 0; j < 8; j++)
		{
			a(i,j) = -1.0f;
			b(i,j) = B_VAL;

		}

	TEST_CUDA_ERROR_ASSERT();
	// Define a region
	Position p_a(3,0);
	Position p_b(0,0);

	Region a_region(p_a, Size(4,4));
	Region b_region(p_b, Size(4,4));
	// copy from one region to another:
	a(a_region) = b(b_region);

	TEST_CUDA_ERROR_ASSERT();


	for(uint i = 0; i < a_region.size().rows(); i++)
		for(uint j = 0; j < a_region.size().cols(); j++)
		{
			float a_val = a(i + p_a.y(), j + p_a.x());
			ASSERT_FLOAT_EQ(a_val, B_VAL) << " 'a' is " << std::endl << a << " 'b' is " << std::endl << b;
		}


	TEST_CUDA_ERROR_ASSERT();
}

/*
*	Test large region copies
*
*/
TEST(DeviceBuffer, RegionCopy2DLarge)
{

	uint s_rows = 256;
	uint s_cols = 256;
	//
	// Stress test over multiple sizes
	for(uint rows = s_rows; rows < 1057; rows+=67)
		for(uint cols = s_cols; cols < 1057; cols+=87)
		{
			DeviceBuffer<float> a(rows, cols);
			HostBuffer<float> h_a(a.size() );

			// right side 
			Region right_region(0, rows-1, cols/2, cols-1); 

			DeviceBuffer<float> b(right_region.size());
			HostBuffer<float> h_right_region(b.size());


			const float LEFT_VAL = 1.0f;
			const float RIGHT_VAL = 2.0f;
			for(uint i = 0; i < h_a.rows(); i++)
				for(uint j = 0; j < h_a.cols(); j++)
				{

					if( j < cols/2)
						h_a(i,j) = LEFT_VAL;
					else
						h_a(i,j) = RIGHT_VAL;

				}

			// To device:
			a = h_a;
			//
			// Copy right side of region containing RIGHT_VAL
			b = a(right_region);

			//
			// Copy left region to host:
			h_right_region = b;
			// sync
			b.stream().synchronize();
			TEST_CUDA_ERROR_ASSERT();
			//
			// Assert that right region has the right values
			test_utils::assert_equal(h_right_region, RIGHT_VAL);

	}

}


 













