/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
/*
    This file has compile time assert utilities for the testing
*/

//
// Assert that CUDA has not had a failure
//
#define TEST_CUDA_ERROR_ASSERT() \
    {\
    	fflush(stdout); \
        cudaDeviceSynchronize();\
        cudaError_t err = cudaGetLastError();\
        ASSERT_TRUE( err == cudaSuccess) << "Failed at line: " << __LINE__ << "\n" << "File: " << __FILE__ << "\n Error message: " << cudaGetErrorString(err);\
    }
    