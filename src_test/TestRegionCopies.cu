/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "HostBuffer.cuh"
#include "IOUtils.cuh"

using namespace xpl;
using namespace std;


#include <vector>



template<class BufferT1, class BufferT2>
void testSlice(const BufferT1& buffer, const BufferT2& slice, const Position& buffOffset=Position(0,0,0), const string message="")
{

	for(uint k = 0; k < slice.depth(); k++)
		for(uint i = 0; i < slice.rows(); i++)
			for(uint j = 0; j < slice.cols(); j++)
			{
				auto bi = i + buffOffset.y();
				auto bj = j + buffOffset.x();
				auto bk = k + buffOffset.z();
				ASSERT_EQ(slice(i,j,k), buffer(bi,bj,bk)) << "At: (" << i << "," << j << "," << k << ")" << " -" << message;
			}
}

template<class Buffer>
void TestRegionValues()
{

	int rows = 13;
	int cols = 18;
	int depth = 4;
	Buffer a(rows, cols, depth);
	// Set some random values:
	for(uint k = 0; k < a.depth(); k++)
		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
			{
				a(i,j,k) = j + i*a.cols() + k*10;//rand.nextNormalDistValue();
			}
	// Loop over depth
	for(int d = 0; d < depth; d++)
	{
		// Define slices
		Region slice0(0,rows-1, 0, cols-1, d,d);
		Region slice0Left(0,rows-1, 0, (cols/2)-1, d,d);
		Region slice0Right(0,rows-1, (cols/2), cols-1, d,d);

		Region slice0Middle(0,rows-1, 
							6, 6+6-1, // middle values
							d, d);

		Region slice0Center(5,10, 
							6, 6+6-1, // middle values
							d, d);

		testSlice(a, *a.region(slice0), slice0.position(), "slice0"); 	
		testSlice(a, *a.region(slice0Left), slice0Left.position(), "slice0Left"); 	
		testSlice(a, *a.region(slice0Right), slice0Right.position(), "slice0Right");
		testSlice(a, *a.region(slice0Middle), slice0Middle.position(), "slice0Middle");
		testSlice(a, *a.region(slice0Center), slice0Center.position(), "slice0Center");
	}	
}

TEST(HostBuffer, regionValues)
{
	TestRegionValues<HostBuffer<int>>();
}
TEST(DeviceBuffer, regionValues)
{
	TestRegionValues<DeviceBuffer<int>>();
}
TEST(PinnedBuffer, regionValues)
{
	TestRegionValues<PinnedBuffer<int>>();
}
TEST(TextureBuffer, regionValues)
{
	TestRegionValues<TextureBuffer<int>>();
}
template<class InBuffer, class OutBuffer=InBuffer>
void TestRegionToBuffer()
{
	int rows = 13;
	int cols = 18;
	int depth = 4;
	InBuffer a(rows, cols, depth);
	// Set some random values:
	for(uint k = 0; k < a.depth(); k++)
		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
			{
				a(i,j,k) = j + i*a.cols() + k*10;//rand.nextNormalDistValue();
			}


	// Define a region in center of A
	Region middleRegion(0,rows-1, 
							6, 6+6-1, // middle values
							2,2);

	// Allocate buffer of slice size
	OutBuffer b(middleRegion.size());

	// copy from a into b
	b = a(middleRegion);

	testSlice(a, b, middleRegion.position());
}
TEST(HostBuffer, HostRegionToHostBuffer)
{
	TestRegionToBuffer<HostBuffer<int>, HostBuffer<int>>();
}
TEST(HostBuffer, HostRegionToDeviceBuffer)
{
	TestRegionToBuffer<HostBuffer<int>, DeviceBuffer<int>>();
}
TEST(HostBuffer, HostRegionToPinnedBuffer)
{
	TestRegionToBuffer<HostBuffer<int>, PinnedBuffer<int>>();
}
TEST(HostBuffer, HostRegionToTextureBuffer)
{
	TestRegionToBuffer<HostBuffer<int>, TextureBuffer<int>>();
}
template<class SrcBuffer, class DstBuffer>
void TestBufferToRegion()
{


	int rows = 13;
	int cols = 18;
	int depth = 4;
	DstBuffer a(rows, cols, depth);
	// Set some random values:
	for(uint k = 0; k < a.depth(); k++)
		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
			{
				a(i,j,k) = j + i*a.cols() + k*10;//rand.nextNormalDistValue();
			}

	// Define a region in center of A
	Region middleRegion(0,rows-1, 
							6, 6+6-1, // middle values
							2,2);

	TEST_CUDA_ERROR_ASSERT();

	// Allocate buffer of slice size
	SrcBuffer b(middleRegion.size());
	// Setup b
	for(uint k = 0; k < b.depth(); k++)
		for(uint i = 0; i < b.rows(); i++)
			for(uint j = 0; j < b.cols(); j++)
			{
				b(i,j,k) = 	j + i*b.cols() + k*10 + 4200;  
			}

	TEST_CUDA_ERROR_ASSERT();
	// copy from b into a
	a(middleRegion) = b;
	TEST_CUDA_ERROR_ASSERT();
	testSlice(a, b, middleRegion.position());
	TEST_CUDA_ERROR_ASSERT();
	

}

/**
*	HostBuffer to different regions
*/
TEST(HostBuffer, toHostRegion){ 		TestBufferToRegion<HostBuffer<int>, 	HostBuffer<int>>(); }
TEST(HostBuffer, toDeviceRegion){ 		TestBufferToRegion<HostBuffer<int>, 	DeviceBuffer<int>>(); }
TEST(HostBuffer, toPinnedRegion){ 		TestBufferToRegion<HostBuffer<int>, 	PinnedBuffer<int>>(); }
TEST(HostBuffer, toTextureRegion){ 		TestBufferToRegion<HostBuffer<int>, 	TextureBuffer<int>>(); }

TEST(DeviceBuffer, toHostRegion){ 		TestBufferToRegion<DeviceBuffer<int>, 	HostBuffer<int>>(); }
TEST(DeviceBuffer, toDeviceRegion){ 	TestBufferToRegion<DeviceBuffer<int>, 	DeviceBuffer<int>>(); }
TEST(DeviceBuffer, toPinnedRegion){ 	TestBufferToRegion<DeviceBuffer<int>, 	PinnedBuffer<int>>(); }
TEST(DeviceBuffer, toTextureRegion){ 	TestBufferToRegion<DeviceBuffer<int>, 	TextureBuffer<int>>(); }

TEST(PinnedBuffer, toHostRegion){ 		TestBufferToRegion<PinnedBuffer<int>, 	HostBuffer<int>>(); }
TEST(PinnedBuffer, toDeviceRegion){ 	TestBufferToRegion<PinnedBuffer<int>, 	DeviceBuffer<int>>(); }
TEST(PinnedBuffer, toPinnedRegion){ 	TestBufferToRegion<PinnedBuffer<int>, 	PinnedBuffer<int>>(); }
TEST(PinnedBuffer, toTextureRegion){ 	TestBufferToRegion<PinnedBuffer<int>, 	TextureBuffer<int>>(); }

TEST(TextureBuffer, toHostRegion){ 		TestBufferToRegion<TextureBuffer<int>, 	HostBuffer<int>>(); }
TEST(TextureBuffer, toDeviceRegion){ 	TestBufferToRegion<TextureBuffer<int>, 	DeviceBuffer<int>>(); }
TEST(TextureBuffer, toPinnedRegion){ 	TestBufferToRegion<TextureBuffer<int>, 	PinnedBuffer<int>>(); }
TEST(TextureBuffer, toTextureRegion){ 	TestBufferToRegion<TextureBuffer<int>, 	TextureBuffer<int>>(); }

template<class Buffer>
void TestRegionToRegionCopy()
{

	int rows = 13;
	int cols = 18;
	int depth = 4;
	Buffer a(rows, cols, depth);

	// Set some unique values:
	for(uint k = 0; k < a.depth(); k++)
		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
			{
				a(i,j,k) = j + i*a.cols() + k*10;//rand.nextNormalDistValue();
			}

	TEST_CUDA_ERROR_ASSERT();
	// Allocate buffer of slice size
	Buffer b(Size(33,33,7));

	// Setup b
	for(uint k = 0; k < a.depth(); k++)
		for(uint i = 0; i < a.rows(); i++)
			for(uint j = 0; j < a.cols(); j++)
			{
				b(i,j,k) = 	j + i*a.cols() + k*10 + 4200;
			}

	TEST_CUDA_ERROR_ASSERT();
	// copy from b into a
	Region a_4x4 = Region(Position(7,7,3),Size(4,4));
	Region b_4x4 = Region(Position(3,7,2), Size(4,4));

	a(a_4x4) = b(b_4x4);
	TEST_CUDA_ERROR_ASSERT();
	test_utils::assert_equal(*a.region(a_4x4),*b.region(b_4x4));
}
TEST(HostBuffer, regionToRegionCopy)
{
	TestRegionToRegionCopy<HostBuffer<int> >();
}
TEST(DeviceBuffer, regionToRegionCopy)
{
	TestRegionToRegionCopy<DeviceBuffer<int> >();
}