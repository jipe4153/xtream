/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "HostBuffer.cuh"
#include "test_utils.cuh"
#include "device_iterate.cuh"
#include "host_iterate.cuh"
#include "toFloat.cuh"

using namespace xpl;
using namespace std;


template<class BufferType>
void TestIterator(BufferType& buff)
{

	auto iter = [=] __device__ (const xpl::Index& ind) mutable
	{
		buff(ind) = ind.x() + ind.y()*buff.cols();
	};
	
	device::iterate(buff.size(),iter);

	HostBuffer<float> h_buff(xpl::toFloat(buff));
	for(uint k = 0; k < buff.depth(); k++)
		for(uint i = 0; i < buff.rows(); i++)
			for(uint j = 0; j < buff.cols(); j++)
			{
				float val = h_buff(i,j,k);
				ASSERT_EQ((int)val, int(j+i*buff.cols())) << "";
			}
}
// Specialize for host side iterators
template<>
void TestIterator(HostBuffer<int>& buff)
{

	auto iter = [=] __host__(const xpl::Index& ind) mutable
	{
		buff(ind) = ind.x() + ind.y()*buff.cols();
	};
	
	host::iterate(buff.size(),iter);

	for(uint k = 0; k < buff.depth(); k++)
		for(uint i = 0; i < buff.rows(); i++)
			for(uint j = 0; j < buff.cols(); j++)
			{
				int val = buff(i,j,k);
				ASSERT_EQ(val, int(j+i*buff.cols())) << "";
			}
}

TEST(Iterator, HostBuffer)
{
	HostBuffer<int> buff(237,13,19);
	TestIterator(buff);
}
TEST(Iterator, DeviceBufferInt)
{
	DeviceBuffer<int> buff(33,47,17);
	TestIterator(buff);
}
TEST(Iterator, DeviceBufferFloat)
{
	DeviceBuffer<float> buff(33,47,17);
	TestIterator(buff);
}
TEST(Iterator, TextureBufferInt)
{
	TextureBuffer<int> buff(33,47,17);
	TestIterator(buff);
}
TEST(Iterator, TextureBufferFloat)
{
	TextureBuffer<float> buff(33,47,17);
	TestIterator(buff);
}
TEST(Iterator, PinnedBufferInt)
{
	PinnedBuffer<int> buff(33,47,17);
	TestIterator(buff);
}
TEST(Iterator, PinnedBufferFloat)
{
	PinnedBuffer<float> buff(33,47,17);
	TestIterator(buff);
}


template<class BufferType>
void TestBufferHostAccess(BufferType& buff)
{

	buff.iterate([=]__device__(Index& ind) mutable 
		{
			buff(ind) = ind.x()+ind.y()*buff.cols() + ind.z()*buff.cols()*buff.rows();
		}
		);

	device::synchronize();

	for(int i : {1,2,8,10})
		for(int j : {1,2,8,10})
		{
			float h_val = buff(xpl::Index(i,j,0));
			ASSERT_EQ(h_val, float(j+i*buff.cols()));
		}
}
TEST(Iterator, DeviceBufferHostAccess)
{
	DeviceBuffer<float> buff(33,47,17);
	TestBufferHostAccess(buff);
}
TEST(Iterator, TextureBufferHostAccess)
{
	TextureBuffer<float> buff(33,47,17);
	TestBufferHostAccess(buff);
}
TEST(Iterator, PinnedBufferHostAccess)
{
	PinnedBuffer<float> buff(33,47,17);
	TestBufferHostAccess(buff);
}
void TestHostBufferIteratorPerformance(HostBuffer<int>& buff, int seed)
{
	auto iter = [&](const xpl::Index& ind) mutable
	{
		buff(ind) = buff(ind) + ind.x() + ind.y()*buff.cols() + seed;
	};
	buff.iterate(iter);
}
void TestHostBufferIteratorPerformance_C_Style(HostBuffer<int>& buff, int seed)
{ 
	//uint N = buff.rows()*buff.cols();   
	int* data_ptr = buff.memory().data();

	for(uint k = 0; k < buff.depth(); k++)
	for(uint i = 0; i < buff.rows(); i++)
		for(uint j = 0; j < buff.cols(); j++)
		{
			//buff(i,j,k) = buff(i,j,k)  + j + i*buff.cols() + seed;
			data_ptr[j+i*buff.cols()+ k*buff.rows()*buff.cols()] = data_ptr[j+i*buff.cols()] + j + i*buff.cols() + seed;
		}
}
TEST(Iterator, DISABLED_HostBufferPerformance)
{
	// Create large buffer:
	HostBuffer<int> buff(4*4096,4096*4);
	buff.for_each([&](int& val){val = rand();});
	float data_gb = 2.0f * float(buff.size().getNbElements()*sizeof(int)) * float(1E-9);

	int seed = rand();

	{
		printf("\n\n C-style");
		auto startTime = xpl::test_utils::get_clock();
		TestHostBufferIteratorPerformance_C_Style(buff, seed);
		auto dt = xpl::test_utils::get_clock() - startTime;
		printf("\n Time [ms] = %0.3f", float(dt*1000.0f));
		float gb_s = data_gb / float(dt);
		printf("\n Data [GB] = %0.3f", data_gb);
		printf("\n Bandwidth [GB/s] = %0.3f", gb_s);
	}
	 {
	 	printf("\n\n Index");
		auto startTime = xpl::test_utils::get_clock();
		TestHostBufferIteratorPerformance(buff, seed);
		auto dt = xpl::test_utils::get_clock() - startTime;
		printf("\n Time [ms] = %0.3f", float(dt*1000.0f));
		float gb_s = data_gb / float(dt);
		printf("\n Data [GB] = %0.3f", data_gb);
		printf("\n Bandwidth [GB/s] = %0.3f", gb_s);
	}
}
