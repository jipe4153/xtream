/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/


#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>

#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"

#include "device_lambda.cuh"

using namespace xpl;
 
using namespace std;



void test_foreach_lambda(DeviceBuffer<float>& a, float input_value)
{
	//auto set_value_lambda =  [=] __device__  (float& val) 
	auto set_value_lambda =  device_for_each(float& val) 
	{ 
		val = input_value;
	};

	a.for_each( set_value_lambda );
	a.stream().synchronize();
}

TEST(Lambdas, ForEachSetValue)
{

	DeviceBuffer<float> a(720,1280);

	test_utils::BasicRand<float> br;
	float input_value = br.nextUniformDistValue();

	test_foreach_lambda(a, input_value);

	//
	// Assert all values are set:
	//
	test_utils::assert_equal(a, input_value);

}
TEST(Lambdas, ForEachSetValueOddSize)
{

	DeviceBuffer<float> a(720+31,1280+17,17);

	test_utils::BasicRand<float> br;
	float input_value = br.nextUniformDistValue();

	test_foreach_lambda(a, input_value);
	//
	// Assert all values are set:
	//
	test_utils::assert_equal(a, input_value);  
}

/**
*	Test a simple lambda performing a memory copy
*/
void test_generic_lambda()
{

	int rows = 2077;
	int cols = 3277;	

	DeviceBuffer<float> input(rows, cols);		
	DeviceBuffer<float> output(rows, cols);

	const float REF_VAL = 12.0f;

	//
	// Memset each value in input
	//	
	auto my_lambda = [=]__device__(float& val)
	{
		val = REF_VAL; 
	};
	
	input.for_each( my_lambda );
	//
	// As a test, copy from input to output
	//
	auto lambda = [=] __device__ () mutable
	{
		//
		// indices
		int j = device::getX();
		int i = device::getY();

		if( j < input.cols() && i < input.rows() )
		{
			output(i,j) = input(i,j);
		}

	};
	//
	// Submit lambda to que on default stream
	device::execute(DeviceGrid(input.size()), lambda);
	//
	// Synchronize default stream
	DeviceStream().synchronize();

	test_utils::assert_equal(output, REF_VAL);
}
TEST(Lambdas, GenericLambda)
{
	test_generic_lambda();
}
/**
*	Test executing a lambda on the device with more complex mapping of threads in the x-direcdtion,
*	a number of X elements per thread are copied instead of one element per thread.
*	This makes the device grid definitions slightly more complex
*/
void test_generic_lambda_coverage()
{

	//int rows = 3*1024+37;
	//int cols = 3*1024+31;	

	int rows = 256+37; //3*1024+37;
	int cols = 256+31; //3*1024+31;	


	DeviceBuffer<float> input(rows, cols);		
	DeviceBuffer<float> output(rows, cols);

	const float REF_VAL = 12.0f;

	//
	// Memset each value in input
	//	
	input.for_each( [=]__device__(float& val){val = REF_VAL; } );
	// memset output
	output.for_each(  [=]__device__(float& val){val = 0.0f; } );
	//
	// As a test, copy from input to output
	//
	const int DIM_X = 64;
	const int DIM_Y =1;
	const int X_ElsPerThread = 4;
	const int X_BLOCK_DATA_COVERAGE = DIM_X * X_ElsPerThread;

	auto lambda = [=] __device__ () mutable
	{
		//
		// indices
		int j = device::getX<X_BLOCK_DATA_COVERAGE>();
		int i = device::getY<DIM_Y>();

		if( (j+X_BLOCK_DATA_COVERAGE-1) < input.cols() && (i) < input.rows() )
		//if( (j) < input.cols() && (i) < input.rows() )
		{
			#pragma unroll
			for(int iter=0 ; iter < X_ElsPerThread; iter++)
			{
				output(i, j + iter*DIM_X) = input(i, j + iter*DIM_X);
			}
		}
		else if( j < input.cols() && (i) < input.rows() )
		{

			for(int iter=j ; iter < input.cols(); iter+=DIM_X)
			{
				output(i,iter) = input(i,iter);
			}
		}

	};  
	//auto deviceMapping = DeviceMapping(DIM_X, 1, Coverage(2, 1));
	//auto grid = DeviceGrid(input.size(), deviceMapping);
	//
	// Submit lambda to que on default stream
	device::execute(DeviceGrid(input.size(), DeviceMapping(DIM_X, 1, BlockCoverage(X_BLOCK_DATA_COVERAGE, 1)) ), lambda);

	//
	// Synchronize default stream
	DeviceStream().synchronize();

	test_utils::assert_equal(output, REF_VAL);
}
/**
*	See test_generic_lambda_coverage() function doc
*/
TEST(Lambdas, GenericCoverage)
{
	test_generic_lambda_coverage();
}


__constant__ float c_param[1];
/**
*  	Simple test of reading constant parameters into a lambda
*/
void lambda_with_constant_params()
{
	float ref_val = 11.0f;
	DeviceBuffer<float> a(256,256);
	//
	// Setup a constant parameter on the device
	cudaMemcpyToSymbol(c_param, &ref_val, sizeof(float));
	//
	// Set each value to constant parameter
	a.for_each( [=] __device__ (float& val) 
		{ 
			val = c_param[0]; 
		});
	//
	// Test
	test_utils::assert_equal(a, ref_val);

}
TEST(Lambdas, ConstantParamReading)
{
	lambda_with_constant_params();
}

/**
*	Lambdas.HostForeach
*	performs a memset operation for each value in host buffer
*/
TEST(Lambdas, HostForeach)
{

	uint rows = 720;
	uint cols = 1280;
	const float REF_VAL = 32.0f;

	HostBuffer<float> buff(rows,cols);
	//
	// Create and execute set_values lambda
	//
	auto set_values =  [=] (float& val) { val = REF_VAL;	};
	buff.for_each( set_values );

	test_utils::assert_equal( buff, REF_VAL);
}















