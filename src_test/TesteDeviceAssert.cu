/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "DeviceBuffer.cuh"
#include "device_lambda.cuh"
#include "device_foreach.cuh"

using namespace xpl;


void AssertOnDeviceFunc();

/**
*	Tests that device asserts have correct behaviour
*
*	WARNING: due to the fact that device side errors break the GPU context (we've been unable to reset it)
*	this test has been disabled
*	@TODO: fix the popping of CUDA errors, potentially by creating a new separate CUDA context
*/
TEST(DeviceAssert, DISABLED_AssertOnDevice)
{
	AssertOnDeviceFunc();
}
__global__ void custom_kernel(int* d_ptr, int rows, int cols, int pitch)
{
	int i = device::getY();
	int j = device::getX();

	if( i < rows && j < cols )
	{
		bool ok = false;

		int val = d_ptr[j + i*pitch];

		if( val==42 )
		{
			ok = false;
		}
		else
		{
			ok = true;
		}
		DEVICE_ASSERT(ok, "Value is not allowed");
		d_ptr[j + i*pitch] = val + (int)ok;
	}
}
void AssertOnDeviceFunc()
{
	DeviceBuffer<int> in(128,128);

	in.for_each(device_for_each(int& val)
	{
		val = 2;
	});
	in(33,33) = 42;

	auto grid = DeviceGrid(in.size());
	custom_kernel<<< grid.blocks(), grid.threads(), grid.smemSize(),  0>>>(in.memory().data(), in.rows(), in.cols(), in.pitch());

	cudaDeviceSynchronize();
	cudaError_t err = cudaGetLastError();
	#ifdef GPU_DEBUG_MODE
		ASSERT_TRUE(err!=cudaSuccess) << "Expected a cuda failure";
	#else
		ASSERT_TRUE(err==cudaSuccess) << "Expected a cuda success";
	#endif

}
