/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "BasicRand.cuh"
#include "CheckEqual.cuh"

#include <stdio.h>  
#include <stdlib.h>
#include "Position.cuh"
#include "Size.cuh"
#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "cuda_profiler_api.h"

#include <memory>

using namespace xpl;

/*
*	Host2DeviceCopySimpleBase2
*	Copy to and from device for base2 size, checks validity	
*/
TEST(Memcpy, Host2DeviceCopySimpleBase2)
{

	uint rows = 1024;
	uint cols = 1024;

	HostBuffer<float> h_buff(rows, cols);
	DeviceBuffer<float> d_buff(h_buff.size());

	test_utils::BasicRand<float> rand;

	for(int i = 0; i < (int)rows; i++)
		for(int j = 0; j < (int)cols; j++)
		{
			h_buff((uint)i,(uint)j) = rand.nextUniformDistValue();
		}

	//
	// Copy values to device
	d_buff = h_buff;
	TEST_CUDA_ERROR_ASSERT();

	HostBuffer<float> h_res(d_buff.size());
	//
	// Copy back to host result buffer
	h_res = d_buff;
	//
	// Make sure all operations hav finalized:
	d_buff.stream().synchronize();


	test_utils::assert_equal(h_res, h_buff);

	TEST_CUDA_ERROR_ASSERT();
}

/*
*	Host2DeviceCopySimple
*	Copy to and from device, checks validity	
*/
TEST(Memcpy, Host2DeviceCopySimple)
{
	uint rows = 512+67;
	uint cols = 1024+37;

	HostBuffer<float> h_buff(rows, cols);
	DeviceBuffer<float> d_buff(h_buff.size() );
	HostBuffer<float> h_res(h_buff.size() );

	test_utils::BasicRand<float> rand;

	//
	// init values:
	for(int i = 0; i < (int)rows; i++)
		for(int j = 0; j < (int)cols; j++)
		{
			h_buff(i,j) = rand.nextUniformDistValue();
		}
	// To device
	d_buff = h_buff;
	// Back to host
	h_res = d_buff;
	// sync
	d_buff.stream().synchronize();
	// Check results:

	test_utils::assert_equal(h_res, h_buff);
}
/*
*	GPU kernel that increments each element
*/
template<class T>
__global__ void increment_values_kernel(DeviceBuffer<T> buff, T inc_value)
{
	uint j = threadIdx.x + blockIdx.x*blockDim.x;
	uint i = threadIdx.y + blockIdx.y*blockDim.y;

	if( j < buff.cols() && i < buff.rows() )
	{
		T value = buff(i,j,0);
		T newVal = value + inc_value;
		buff(i,j,0) = newVal;
	}

}
/*
*	IncrementOnDevice
*	Increments a value separately on the device and hosts and checks that they are valid	
*/
TEST(Memcpy, IncrementOnDevice)
{
	uint rows = 512+67;
	uint cols = 1024+37;

	HostBuffer<float> h_buff(rows, cols);
	DeviceBuffer<float> d_buff(h_buff.size() );
	HostBuffer<float> h_res(h_buff.size() );

	test_utils::BasicRand<float> rand;

	const float INC_VAL = 1.0f;
	//
	// init values:
	for(int i = 0; i < (int)rows; i++)
		for(int j = 0; j < (int)cols; j++)
		{
			h_buff(i,j) = rand.nextUniformDistValue();
		}
	// To device
	d_buff = h_buff;

	const int DIM_X = 32;
	const int DIM_Y = 8;
	int X_BLOCKS = (DIM_X + cols - 1) / DIM_X;
	int Y_BLOCKS = (DIM_Y + rows - 1) / DIM_Y;

	dim3 block(DIM_X, DIM_Y);
	dim3 grid(X_BLOCKS, Y_BLOCKS);

	increment_values_kernel<<< grid, block >>>(d_buff, INC_VAL);

	// Back to host
	h_res = d_buff;
	// sync
	d_buff.stream().synchronize();
	
	// Increment on host reference:
	for(int i = 0; i < (int)rows; i++)
		for(int j = 0; j < (int)cols; j++)
		{
			h_buff(i,j) = h_buff(i,j) + INC_VAL;
		}
	// Check results:
	test_utils::assert_equal(h_res, h_buff);
}