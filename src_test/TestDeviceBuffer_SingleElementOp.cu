/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "Position.cuh"
#include "Size.cuh"
#include "DeviceBuffer.cuh"

#include "cuda_profiler_api.h"

#include "device_lambda.cuh"
#include "CheckEqual.cuh"

using namespace xpl;
 
using namespace std;

__global__ void kernel_test(float* d_ptr, float* d_result)
{
	float val = d_ptr[0];
	val = val + 1.0f;
	d_result[0] = val;
}

/*
*	Tests single element modification of input and results
*	The CPU sets the input value, GPU modified the value,
*	result is then accessed on the CPU 
*/
TEST(DeviceBuffer, SingleElementTest) 
{
	DeviceBuffer<float> input(1);
	DeviceBuffer<float> result(1);

	// Modify value on CPU 
	input(0) = 42.0f;
	// print value on GPU
	kernel_test<<<  1, 1>>>(input.memory().data(), result.memory().data());
	
	// Check value of GPU
	float expected = input(0)+1.0f;
	float gpu_result = result(0);
	ASSERT_FLOAT_EQ(expected, gpu_result) << "GPU or CPU data modifiers failed";

}
TEST(DeviceBuffer, SingleElementTest2) 
{
	int N = 32;
	DeviceBuffer<float> input(N);
	DeviceBuffer<float> result(N);

	// Modify value on CPU 
	input(0) = 42.0f;

	// print value on GPU
	kernel_test<<<  1, 1>>>(input.memory().data(), result.memory().data());
	
	// Check value of GPU
	float expected = input(0)+1.0f;
	float gpu_result = (float)result(0);
	ASSERT_FLOAT_EQ(expected, gpu_result) << "GPU or CPU data modifiers failed";

}


void FunctionLargeSingleElementTest(const int N)
{


	DeviceBuffer<float> input(N);
	DeviceBuffer<float> result(N);
	// Modify value on CPU 
	for( int i = 0; i < N; i++)
	{
		input(i) = 42.0f;
	}

	//cudaDeviceSynchronize();
	input.stream().synchronize();

	// GPU kernel : add a value	
	auto kernel = [=] __device__ () mutable
	{
		int x = device::getX();
		if ( x < N)
		{
			result(x) = input(x) + 1.0f;
		}
	};
	device::execute( DeviceGrid(input.size()), kernel);
	// Check value of GPU
	test_utils::assert_equal(result, 43.0f);

}

TEST(DeviceBuffer, LargeSingleElementTest) 
{
	int N = 1024*4;

	for( int t = 0; t < 5; t++)
		FunctionLargeSingleElementTest(N);
}

