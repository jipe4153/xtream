/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "Position.cuh"
#include "Size.cuh"
#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "BasicRand.cuh"
#include <cstdlib>
#include "CheckEqual.cuh"

#include "cuda_profiler_api.h"

using namespace xpl;
 
using namespace std;


TEST(DeviceBuffer, CopyHostConstruction)
{

	uint rows = 44;
	uint cols = 33;
	uint depth = 7;

	HostBuffer<float> host(rows,cols,depth);
	host.for_each([&](float& val){val=float(std::rand()); });

	DeviceBuffer<float> device(host);

	for(uint k = 0; k < depth; k++)
		for(uint i = 0; i < rows; i++)
			for(uint j = 0; j < cols; j++)
			{
				float val = device(i,j,k);
				bool ok = test_utils::equal(val, host(i,j,k));
				ASSERT_TRUE(ok) << "device = " << val << " host = " << host(i,j,k);
			}

}

/**
*	Maps a device pointer to a DevieBuffer, makes sure that modifications to mapped
*\n	pointers do take place on.
*/
void DeviceBufferMap()
{

	int rows = 128;
	int cols = 128;

	size_t pitchInBytes = 0;
	float* in_ptr = NULL;
	cudaMallocPitch(	(void**)&(in_ptr),  &pitchInBytes, cols*sizeof(float),	rows);
	int pitch = pitchInBytes / sizeof(float);

	// Modify mapped buffer:
	DeviceBuffer<float> in = DeviceBuffer<float>::map(in_ptr, Size(rows,cols), pitch);
	in.for_each([=]__device__(float& val){ val = 42.0f;});

	// Copy mapped buffer to host:
	HostBuffer<float> h1(in);

	// Manually copy to host from original pointer:	
	HostBuffer<float> h2(in.size());
	// Copy from GPU to CPU
	cudaMemcpy2DAsync	(
 					h2.memory().data(),				// dst ptr
 					h2.pitch()*sizeof(float),	// dst pitch in bytes
 					in_ptr,				// src ptr
 					pitch*sizeof(float),	// src pich in bytes
					cols*sizeof(float), 	// width in bytes
					rows,				// height / rows
					cudaMemcpyDeviceToHost,
					0
					);
 	
 	TEST_CUDA_ERROR_ASSERT();

 	for(uint i = 0; i < h1.rows(); i++)
 		for(uint j = 0; j < h1.cols(); j++)
 		{
 			ASSERT_NEAR(h1(i,j), h2(i,j), 0.001f) << "";
 		}

 	cudaFree(in_ptr);
}
TEST(DeviceBuffer, Map)
{
	DeviceBufferMap();
}
