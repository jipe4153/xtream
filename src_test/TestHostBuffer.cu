/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "HostBuffer.cuh"

using namespace xpl;
using namespace std;


#include <vector>


TEST(HostBuffer, Create)
{
	const int iters = 10;
	int N = 1024*512+ 33;
	std::vector<int> vec;

	int ref_sum = 0;
	for(int i = 0; i < N; i++)
	{
		vec.push_back(i);
		ref_sum+=i;
	}
	for(int i = 0; i< iters; i++)
	{
		// Create new host copy buffer referncing the underlying memory
		auto host = xpl::HostBuffer<int>::create(vec);
		int sum = 0;
		host->for_each([&](int& val){ sum += val;});
		ASSERT_EQ(sum, ref_sum) << "Sums did not match";
	}
}
TEST(HostBuffer, CreateModify)
{
	int N = 1024*512+ 33;
	std::vector<int> vec(N);

	auto host = xpl::HostBuffer<int>::create(vec);
	for(int i = 0; i < N; i++)
	{
		(*host)(i) = i;
	}
	// Check that vector data has been modifed
	for(int i = 0; i < N; i++)
		ASSERT_EQ(vec[i], i);
}
TEST(HostBuffer, Map)
{
	const int iters = 10;
	int N = 1024*512+ 33;
	std::vector<int> vec;

	int ref_sum = 0;
	for(int i = 0; i < N; i++)
	{
		vec.push_back(i);
		ref_sum+=i;
	}
	for(int i = 0; i< iters; i++)
	{
		// Create new host copy buffer referncing the underlying memory
		HostBuffer<int> host = xpl::HostBuffer<int>::map(vec);

		int sum = 0;
		host.for_each([&](int& val){ sum += val;});
		ASSERT_EQ(sum, ref_sum) << "Sums did not match";
	}

}
TEST(HostBuffer, MapModify)
{
	int N = 1024*512+ 33;
	std::vector<int> vec(N);

	HostBuffer<int> host = xpl::HostBuffer<int>::map(vec);
	for(int i = 0; i < N; i++)
	{
		host(i) = i;
	}
	// Check that vector data has been modifed
	for(int i = 0; i < N; i++)
		ASSERT_EQ(vec[i], i);
}
TEST(HostBuffer, ReadWrite)
{


	int rows = 256;
	int cols = 256;
	int depth = 128;

	HostBuffer<float> a(rows,cols, depth);
	HostBuffer<float> b(a.size());

	b.for_each([&](float& val)
		{
			val = 0.0f;
		});
	
	int i = 0;
	a.for_each([&](float& val)
		{
			val = float(i);
			i++;
		});

	// Copy to b:
	for(int k = 0; k < depth; k++)
		for(int i = 0; i < rows; i++)
			for(int j = 0; j < cols; j++)
			{
				float a_val = a(i,j,k);
				b(i,j,k) = a_val;
			}

	test_utils::assert_equal(a,b);
}



TEST(HostBuffer, ReadWriteManyBuffers)
{
	int rows = 256;
	int cols = 73;
	int depth = 17;

	HostBuffer<float> a(rows,cols, depth);
	HostBuffer<float> b(a.size());
	HostBuffer<float> c(a.size());


	auto setZero = [&](float& val)
	{
		val = 0.0f;
	};
	auto setOne = [&](float& val)
	{
		val = 1.0f;
	};

	b.for_each(setZero);
	c.for_each(setOne);
	
	int i = 0;
	a.for_each([&](float& val)
		{
			val = float(i);
			i++;
		});

	// Copy to b:
	for(int k = 0; k < depth; k++)
	{
		for(int i = 0; i < rows; i++)
		{
			for(int j = 0; j < cols; j++)
			{
				float a_val = a(i,j,k);
				b(i,j,k) = a(i,j,k) + c(i,j,k);
			}
		}
	}

	float b_sum = 0.0f;
	b.for_each([&](float& val)
		{
			b_sum += val;
		});

	float a_sum = 0.0f;
	a.for_each([&](float& val)
		{
			a_sum += val;
		});

	float expected_sum = a_sum + float(c.size().getNbElements());
	bool equal =  test_utils::equal(b_sum, a_sum, float(1E-5));
	ASSERT_TRUE(equal);
}
