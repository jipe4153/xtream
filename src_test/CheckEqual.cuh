	/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include <limits>
#include "IBuffer.cuh"
#include "HostBuffer.cuh"

namespace test_utils
{

/*
*	Compute the relative difference between 2 values
*
*	Ex:
*	ref = 0 and res = .2 => rel_diff = |0-0.2| / max(|0|,|0.2|) = 1
*	ref = 1.0 and res = 0.9 => rel_diff = |1-0.9| / max(|1|,|0.9|) = 0.1/1 = 0.1 => 10% difference
*	ref = 1 and res = 1.2 => rel_diff = |1-1.2| / max(|1|,|1.2|) = 0.2/1.2 = 2/12 = 1/6 = 0.1666 => ~16.7s % difference
*	
*	@param ref  - input reference value
*	@param res  - input result value
* 	@return relative differece
*/
template<class T>
T relative_difference(const T& ref, const T& res)
{
	//
	// Special case:
	if( ref == (T)0.0 && res == (T)0.0)
		return (T)0.0;

	//
	// Compute relative difference:
	T abs_max = std::max( std::abs(ref) , std::abs(res) );
	T diff = std::abs( ref-res );	
	// Guaranteed non-zero division
	T rel_diff = diff/abs_max;

	return rel_diff;
}




// Will handle integers only
template<class T>
bool equal(const T& ref, const T& res, T epsilon=std::numeric_limits<T>::epsilon())
{

	//  upsample to 64 bit integer
	int64_t iref = (int64_t)ref;
	int64_t ires = (int64_t)res;

	if( (iref-ires) == 0)
		return true;
	else
		return false;

}
template<>
inline
bool equal(const float& ref, const float& res, float epsilon)
{
	float rel_diff = relative_difference(ref, res);
	if( rel_diff < epsilon)
		return true;
	else
		return false;
}
template<>
inline
bool equal(const double& ref, const double& res, double epsilon)
{

	double rel_diff = relative_difference(ref, res);
	if( rel_diff < epsilon)
		return true;
	else
		return false;

}
/**
*	Checks that all 'ref'  IBuffer values are equal to value
*/
template<class BufferType>
void assert_equal_val(const BufferType& res, const float& value)
{

	for(uint k = 0; k < res.depth(); k++)
		for(uint i = 0; i < res.rows(); i++)
			for(uint j = 0; j < res.cols(); j++)
			{
				bool same = equal( (float)res(i,j,k) , value);

				ASSERT_TRUE(same) <<  "Not equal at: " << "i j k -> " 
									<< i << " " << j << " " << k 
									<< "\n res = " << res(i,j,k)
									<< "\n ref = " << value;

			}
}
template<class BufferType>
void assert_equal_val(const BufferType& res, const int& value)
{

	for(uint k = 0; k < res.depth(); k++)
		for(uint i = 0; i < res.rows(); i++)
			for(uint j = 0; j < res.cols(); j++)
			{
				bool same = equal( (int)res(i,j,k) , value);

				ASSERT_TRUE(same) <<  "Not equal at: " << "i j k -> " 
									<< i << " " << j << " " << k 
									<< "\n res = " << res(i,j,k)
									<< "\n ref = " << value;

			}
}
template<class BufferType>
void assert_equal(const BufferType& ref, const float& value)
{
	xpl::HostBuffer<float> ref_copy(ref.size());
	ref_copy = ref;
	assert_equal_val(ref_copy, value);
}
template<class BufferType>
void assert_equal(const BufferType& ref, const int& value)
{
	xpl::HostBuffer<int> ref_copy(ref.size());
	ref_copy = ref;
	assert_equal_val(ref_copy, value);
}
template<class BufferType1, class BufferType2>
void assert_equal(const BufferType1& ref, const BufferType2& res)
{

	ASSERT_TRUE(ref.size() == res.size() ) << "Buffers must be of equal size to be compared";


	for(uint k = 0; k < ref.depth(); k++)
		for(uint i = 0; i < ref.rows(); i++)
			for(uint j = 0; j < ref.cols(); j++)
			{
				bool same = equal( ref(i,j,k) , res(i,j,k) );

				ASSERT_TRUE(same) <<  "Not equal at: " << "i j k -> " 
									<< i << " " << j << " " << k 
									<< "\n ref = " << ref(i,j,k)
									<< "\n res = " << res(i,j,k);

			}


}
template<class T>
void assert_equal(const xpl::DeviceBuffer<T>& ref, const xpl::DeviceBuffer<T>& res)
{

	xpl::HostBuffer<T> host_ref(ref.size());
	xpl::HostBuffer<T> host_res(res.size());
	host_ref = ref;
	host_res = res;

	assert_equal(host_ref,host_res);
}


} // end namespace