/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "HostBuffer.cuh"
#include "device_iterate.cuh"
#include "host_iterate.cuh"
#include "toFloat.cuh"

using namespace xpl;
using namespace std;



void TestDeviceToFloat()
{
	DeviceBuffer<int> int_buff(480,1220);
	int_buff.iterate( [=]__device__(const xpl::Index& ind) mutable
		{ 
			int_buff(ind) = ind.x() + ind.y()*int_buff.cols();
		}
		);
	
	HostBuffer<float> ref(xpl::toFloat(int_buff));

	ref.iterate( [=]__host__(const xpl::Index& ind) mutable
		{
			float val = ref(ind);
			float exp_val = float(ind.x() + ind.y()*int_buff.cols() );

			float rd  = test_utils::relative_difference(val, exp_val);
			ASSERT_LT(rd, float(1E-6)) << "val = " << val << " exp_val" << exp_val;
		}
		);
}
TEST(toFloat, DeviceBuffer)
{
	TestDeviceToFloat();
}
TEST(toFloat, HostBuffer)
{

	HostBuffer<int> a(33,55,37);
	//
	// Generate randome values
	a.iterate([=](const Index& ind) mutable
		{
			a(ind) = rand()/10000;
		}
		);
	// To float operation
	HostBuffer<float> b = xpl::toFloat(a);
	//
	// Test values
	b.iterate([=](const Index& ind) mutable
	{
		float b_val = float(b(ind));
		float a_val = float(a(ind));
		float rd  = test_utils::relative_difference(a_val,b_val);
		ASSERT_LT(rd, float(1E-6)) << "Expected: " << b_val << " to be: " << a_val;
	}
	);
}
