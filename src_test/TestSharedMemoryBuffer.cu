
#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>
#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "device_lambda.cuh"
#include "Smem.cuh"
#include "TimingRecord.cuh"



using namespace xpl;
 
using namespace std;



void TestSharedMemoryBufferStd();
void TestSharedMemoryBufferMultiple();
void TestSharedMemoryBufferStencilValues();
void TestSharedMemoryBufferNDFilter();

void nFilter(	HostBuffer<float>& dst, 
				const HostBuffer<float>& src,
				const uint fBorder);


TEST(SharedMemoryBuffer, std)
{
	TestSharedMemoryBufferStd();
}
TEST(SharedMemoryBuffer, TestSharedMemoryBufferMultiplee)
{
	TestSharedMemoryBufferMultiple();	
}

TEST(SharedMemoryBuffer, StencilValues)
{
	TestSharedMemoryBufferStencilValues();
}
TEST(SharedMemoryBuffer, NFilter)
{
	TestSharedMemoryBufferNDFilter();
}


void Test1();

TEST(SharedMemoryBuffer, Test1)
{
	Test1();
}

void Test1()
{

	int rows = 16;
	int cols = 16;


	DeviceBuffer<float> in(rows,cols);
	DeviceBuffer<float> in2(10,10,4);

	DeviceBuffer<float> out(in.size());
	in.for_each([=]__device__(float& val){	val = 1.0f;}  );
	in2.for_each([=]__device__(float& val){	val = -1;}  );

	// Set regions:
	in2.region(Region(0,9,0,9, 0,0))->for_each( [=]__device__(float& val){	val = 1;}   );
	in2.region(Region(0,9,0,9, 1,1))->for_each( [=]__device__(float& val){	val = 2;}   );
	in2.region(Region(0,9,0,9, 2,2))->for_each( [=]__device__(float& val){	val = 3;}   );
	in2.region(Region(0,9,0,9, 3,3))->for_each( [=]__device__(float& val){	val = 4;}  );

	//auto grid = DeviceGrid(out.size(), DeviceMapping(10,10));

	auto grid = DeviceGrid(dim3(2,2), dim3(8,8));

	Smem<float> smem( grid, Coverage(8,8),BlockStencil(1, 1));

	auto kernel = xpl_device_lambda()
	{

		smem.position( 0,0, blockIdx.x + blockIdx.y*2 );
		smem.cache_async(in2);
		__syncthreads();
		

		int i = threadIdx.y + blockIdx.y*8;
		int j = threadIdx.x + blockIdx.x*8;

		if( i < out.rows() && j < out.cols())
		{
			// Compute sum of elements
			float sum  = 0.0f;
			for(int y = -1; y < 2; y++)
				for(int x = -1; x < 2; x++)
				{
					sum += smem(threadIdx.y + y, threadIdx.x + x);
				}

			out(i,j) = sum;
		}
	};

	device::execute(grid, kernel);
}
void TestSharedMemoryBufferNDFilter()
{

	int rows = 537;
	int cols = 333;

	int filterSize = 11;
	int fBorder = (filterSize-1)/2;
	int fs = -fBorder;
	int fe = fBorder;

	DeviceBuffer<float> in(rows,cols);
	DeviceBuffer<float> out(in.size());
	
	in.for_each([=]__device__(float& val){	val = 1.0f;}  );

	in(44,44) = 44.0f;
	out(44,44) = 44.0f + in(44,44);

	auto grid = DeviceGrid(in.size());

	Smem<float> smem( grid, BlockStencil(fBorder, fBorder));

	auto kernel = xpl_device_lambda()
	{
		smem.cache(in);

		int i = device::getY();
		int j = device::getX();


		if(i < in.rows()-fBorder && j < in.cols()-fBorder && i >= fBorder && j >= fBorder)
		{
			float sum = 0.0f;
			for(int y=fs; y <= fe; y++)
			{
				for(int x=fs; x <= fe; x++)
				{
					float val = smem(threadIdx.y+y, threadIdx.x+x);
					sum += val;
				}
			}
			out(i,j) = sum;  
		}
		else
		{
			if(i < in.rows() && j < in.cols() )
				out(i,j) = in(i,j);
		}
	

	};

	device::execute(grid, kernel);
	CUDA_ERROR_ASSERT();
	//
	// Compute CPU reference
	//
	HostBuffer<float> h_in(in);
	HostBuffer<float> h_out_ref(in.size());

	nFilter(h_out_ref, h_in, (uint)fBorder);
	//
	// Copy device output to host and validate:
	HostBuffer<float> h_out(out);
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
		{
			ASSERT_NEAR(h_out_ref(i,j), h_out(i,j), 0.00001f) << "At: " << i << " " << j;
		}
	}
}

void TestSharedMemoryBufferStencilValues()
{

	int rows = 1024;
	int cols = 1024;

	DeviceBuffer<float> in(rows,cols);
	DeviceBuffer<float> out(in.size());
	
	in.for_each([=]__device__(float& val){	val = 1.0f;}  );

	auto grid = DeviceGrid(in.size());

	Smem<float> smem( grid, BlockStencil(1,1));

	auto kernel = xpl_device_lambda()
	{
		smem.cache(in);

		int i = device::getY();
		int j = device::getX();


		if(i < in.rows()-1 && j < in.cols()-1 && i > 0 && j > 0)
		{
			float sum = 0.0f;
			for(int y : {-1,0,1})
			{
				for(int x : {-1,0,1})
				{

					float val = smem(threadIdx.y+y, threadIdx.x+x);
					sum += val;
				}
			}
			out(i,j) = sum;  
		}
		else
		{
			out(i,j) = in(i,j);
		}
	

	};
	device::execute(grid, kernel);
	CUDA_ERROR_ASSERT();
	//
	// Compute CPU reference
	//
	HostBuffer<float> h_in(in);
	HostBuffer<float> h_out_ref(in.size());

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
		{

			if( i >0 && i <rows-1 && j >0 && j<cols-1)
			{
				float sum = 0.0f;
				for(int y : {-1,0,1})
				{
					for(int x : {-1,0,1})
					{
						sum += h_in(i+y, j+x);
					}
				}
				h_out_ref(i,j) = sum;
			}
			else
			{
				h_out_ref(i,j) = h_in(i,j);
			}

		}
	//
	// Copy device output to host and validate:
	HostBuffer<float> h_out(out);
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
		{
			ASSERT_NEAR(h_out_ref(i,j), h_out(i,j), 0.00001f) << "At: " << i << " " << j;
		}
	}

}





__device__ void doFunc(DeviceBuffer<float>& out, Smem<float>& smem)
{


	int i = device::getY();
	int j = device::getX();

	
	out(i,j) = (float)smem(threadIdx.y, threadIdx.x);

}




void TestSharedMemoryBufferStd()
{
	int rows = 513;
	int cols = 366+17;

	DeviceBuffer<float> in(rows,cols);
	DeviceBuffer<float> out(in.size());
	
	in.for_each([=]__device__(float& val){	val = 42.0f+device::getX();}  );
	
	auto grid = DeviceGrid(in.size(), DeviceMapping(32,32));
	
	Smem<float> smem( grid );

	auto kernel = xpl_device_lambda()
	{
		smem.cache(in);
		
		int i = device::getY();
		int j = device::getX();

		if(i < in.rows() && j < in.cols())
		{
			out(i,j) = (float)smem(threadIdx.y, threadIdx.x);
		}
	};
	device::execute(grid, kernel);

	CUDA_ERROR_ASSERT();

	HostBuffer<float> h_in_ref(in);
	HostBuffer<float> h_out_ref(out);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
		{
			ASSERT_NEAR(h_in_ref(i,j), h_out_ref(i,j), 0.00001f) << "At: " << i << " " << j;
		}

}
void TestSharedMemoryBufferMultiple()
{
	int rows = 513;
	int cols = 366+17;

	DeviceBuffer<float> in1(rows,cols);
	DeviceBuffer<float> in2(rows,cols);

	DeviceBuffer<float> out1(in1.size());
	DeviceBuffer<float> out2(in1.size());
	

	in1.for_each([=]__device__(float& val){	val = 42.0f+device::getX();}  );
	in2.for_each([=]__device__(float& val){	val = 2.0f;}  );

	auto grid = DeviceGrid(in1.size());
	
	Smem<float> smem1( grid );
	Smem<float> smem2( grid );

	auto kernel = xpl_device_lambda()
	{
		smem1.cache(in1);
		smem2.cache(in2);

		int i = device::getY();
		int j = device::getX();

		if(i < in1.rows() && j < in1.cols())
		{
			out1(i,j) = smem1(threadIdx.y, threadIdx.x);
			out2(i,j) = smem2(threadIdx.y, threadIdx.x);
		}
	};
	device::execute(grid, kernel);

	CUDA_ERROR_ASSERT();

	HostBuffer<float> h_in1_ref(in1);
	HostBuffer<float> h_out1_ref(out1);

	HostBuffer<float> h_in2_ref(in2);
	HostBuffer<float> h_out2_ref(out2);

	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
		{
			ASSERT_NEAR(h_in1_ref(i,j), h_out1_ref(i,j), 0.00001f) << "At: " << i << " " << j;
			ASSERT_NEAR(h_in2_ref(i,j), h_out2_ref(i,j), 0.00001f) << "At: " << i << " " << j;
		}
}


void nFilter(	HostBuffer<float>& dst, 
				const HostBuffer<float>& src,
				const uint fBorder)
{

	ASSERT_TRUE(dst.size()==src.size()) << "Invalid size";

	for(uint i = 0; i < src.rows(); i++)
	{
		for(uint j = 0; j < src.cols(); j++)
		{
			if(		i < src.rows()-fBorder && 
					j < src.cols()-fBorder && 
					i >= fBorder && 
					j >= fBorder)
			{
				float sum = 0.0f;
				for(int y=-(int)fBorder; y <= (int)fBorder; y++)
				{
					for(int x=-(int)fBorder; x <= (int)fBorder; x++)
					{
						sum += src(i+y, j+x);
					}
				}
				dst(i,j) = sum;
			}
			else
			{
				dst(i,j) = src(i,j);
			}
		}
	}
} // END func














