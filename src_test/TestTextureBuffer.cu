/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#include "gtest/gtest.h"

#include "TestAssert.cuh"
#include "CheckEqual.cuh"
#include "BasicRand.cuh"
#include <stdio.h>  
#include <stdlib.h>

#include "TextureBuffer.cuh"
#include "device_memcpy.cuh"
#include "device_lambda.cuh"
#include "device_foreach.cuh"

using namespace xpl;
using namespace std;


void TextureBufferForEach();
void TextureBufferReadWrite2D();
void TextureBufferReadWrite3D();
void TextureBufferFilteringModes();
void TextureBufferWrite();

TEST(TextureBuffer, ForEach)
{
	TextureBufferForEach();
}

TEST(TextureBuffer, ReadWrite2D)
{
	TextureBufferReadWrite2D();
}
TEST(TextureBuffer, ReadWrite3D)
{
	TextureBufferReadWrite3D();
}
TEST(TextureBuffer, FilteringModes)
{
	TextureBufferFilteringModes();
}
TEST(TextureBuffer, Write)
{
	TextureBufferWrite();
}
TEST(TextureBuffer, CopyHostConstruct)
{

	uint rows = 44;
	uint cols = 33;
	uint depth = 3;

	HostBuffer<float> host(rows,cols,depth);
	host.for_each([&](float& val){val=float(std::rand()); });

	TextureBuffer<float> texture(host);

	for(uint k = 0; k < depth; k++)
		for(uint i = 0; i < rows; i++)
			for(uint j = 0; j < cols; j++)
			{
				float val = texture(i,j,k);
				bool ok = test_utils::equal(val, host(i,j,k));
				ASSERT_TRUE(ok) << "device = " << val << " host = " << host(i,j,k);
			}

}

void TextureBufferFilteringModes()
{
	int rows = 256;
	int cols = 256;

	TextureBuffer<float> tex(rows, cols);
	DeviceBuffer<float> output(tex.size());

	tex.for_each(device_for_each(float& val){ val = float(device::getX()); });
	TEST_CUDA_ERROR_ASSERT();
	// Try setting filterin mode:
	tex.setFilterMode(cudaFilterModeLinear);
	TEST_CUDA_ERROR_ASSERT();
	// Do some filtering:
	const float shift = 0.5f;

	auto filterKernel = xpl_device_lambda()
	{
		int i = (threadIdx.y + blockIdx.y * blockDim.y);
		int j = (threadIdx.x + blockIdx.x * blockDim.x);
		float y = (float)i;
		float x = (float)j + shift;

		float filteredValue = tex(y+0.5f,x+0.5f);

		if( i < rows && j < cols)
			output(i,j) = filteredValue;

	};
	device::execute(DeviceGrid(output.size()), filterKernel);
	output.stream().synchronize();
	
	HostBuffer<float> h_out(output);
	HostBuffer<float> h_in(tex);

	//
	// We shifted by 0.5 and hence expect output(i,j) = (output(i,j) + output(i,j+1))/2;
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols-1; j++)
		{

			float expectValue = (h_in(i,j) + h_in(i,j+1))/2.0f;
			ASSERT_FLOAT_EQ(h_out(i,j), expectValue) << "Missmatched value";			
		}
}
void TextureBufferReadWrite2D()
{
	int rows = 256+17;
	int cols = 256+33;

	TextureBuffer<float> tex(rows,cols);
	TextureBuffer<float> tex_out(rows,cols);

	// assign a unique value to each point in the buffer
	tex.for_each([=]__device__(float& val) { val = (float)(device::getX() + device::getY()*cols);});

	auto readWriteKernel = xpl_device_lambda()
	{

		int i = device::getY();
		int j = device::getX();

		if(i < tex.rows() && j < tex.cols())
		{
			tex_out(i,j) = (float)tex(i,j);
		}

	};
	device::execute(DeviceGrid(tex.size()), readWriteKernel);
	DeviceStream().synchronize();

	CUDA_ERROR_ASSERT();


	HostBuffer<float> h_tex(tex);
	HostBuffer<float> h_tex_out(tex_out);

	for(uint i = 0; i < h_tex.rows(); i++)
		for(uint j = 0; j < h_tex.cols(); j++)
		{
			ASSERT_FLOAT_EQ(h_tex(i,j), h_tex_out(i,j)) << "Missmatched value";
		}
		

}
void TextureBufferReadWrite3D()
{
	int rows = 256+17;
	int cols = 256+33;
	int depth = 17;
	TextureBuffer<float> tex( rows, cols, depth);
	TextureBuffer<float> tex_out(rows, cols, depth);
	//
	// assign a unique value to each point in the buffer
	tex.for_each([=]__device__(float& val) { val = (float)(device::getX() + device::getY()*cols + device::getZ()*rows*cols);});

	auto readWriteKernel = xpl_device_lambda()
	{

		int i = device::getY();
		int j = device::getX();
		int k = device::getZ();

		if(i < tex.rows() && j < tex.cols() && k < tex.depth() )
		{
			tex_out(i,j,k) = (float)tex(i,j,k);
		}

	};
	device::execute(DeviceGrid(tex.size()), readWriteKernel);
	DeviceStream().synchronize();

	CUDA_ERROR_ASSERT();


	HostBuffer<float> h_tex(tex);
	HostBuffer<float> h_tex_out(tex_out);

	for(uint i = 0; i < h_tex.rows(); i++)
		for(uint j = 0; j < h_tex.cols(); j++)
			for(uint k = 0; k < h_tex.depth(); k++)
			{
				ASSERT_FLOAT_EQ(h_tex(i,j,k), h_tex_out(i,j,k)) << "Missmatched value";
			}
		

}

void TextureBufferWrite()
{
	int rows = 256;
	int cols = 256;

	DeviceBuffer<float> in(rows,cols);
	TextureBuffer<float> tex(rows,cols);
	tex.for_each([=]__device__(float& val) { val = 3.0f;});

	auto readWriteKernel = xpl_device_lambda()
	{

		int i = device::getY();
		int j = device::getX();

		if(i < tex.rows() && j < tex.cols())
		{
			tex(i,j) = in(i,j);
		}

	};
	device::execute(DeviceGrid(tex.size()), readWriteKernel);
	DeviceStream().synchronize();

	CUDA_ERROR_ASSERT();


	HostBuffer<float> h_in(in);
	HostBuffer<float> h_tex(tex);

	for(uint i = 0; i < h_in.rows(); i++)
		for(uint j = 0; j < h_in.cols(); j++)
		{
			ASSERT_FLOAT_EQ(h_in(i,j), h_tex(i,j)) << "Missmatched value";
		}
		

}
void TextureBufferForEach()
{
	int rows = 256;
	int cols = 256;

	const float FLT_VAL = 42.0f;
	TextureBuffer<float> tex(rows,cols);

	tex.for_each(device_for_each(float& val) { val = FLT_VAL;});
	HostBuffer<float> host_ref(tex);
	
	tex.stream().synchronize();

	host_ref.for_each(
		[=]__host__(float& val)
		{
			EXPECT_FLOAT_EQ(val, FLT_VAL) << "Missmatched value";			
		}
		);
}
