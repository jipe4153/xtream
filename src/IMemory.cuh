/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Region.cuh"
#include <memory>
#include <sstream>
#include <ostream>
#include <iostream>

namespace xpl
{
	/**
	*	Standard memory interface
	*/
	template<class T>
	class IMemory
	{

	public:
		
		virtual ~IMemory() {};
		/**
		*	Retrieves value from memory at (row, col, depth) = (i,j,k)
		*	@param i - row index
		*	@param j - column index
		*	@param k - depth index
		*/
		virtual T getValueAt(uint i, uint j, uint k) const = 0;
		/**
		*	Region coverage of this buffer
		*/
		virtual Region getRegion() const = 0;
		/**
		*	Returns a string representation of memory contents
		*/
		virtual operator std::string() const = 0;
		/**
		*	Gets the number of rows in Y-dimensions
		*/
		virtual uint rows() 			const = 0;
		/**
		*	Gets the number of columns in X-dimension (memory direction)
		*/
		virtual uint cols() 			const = 0;
		/**
		*	Gets the depth of the buffer in Z dimension (3D is max)
		*/
		virtual uint depth() 		const = 0;
		/**
		*	Gets the pitch of the memory in the column / X-dimension
		*	the pitch is always greater than or equal to the number of columns
		*/
		virtual uint pitch() 		const = 0;
		/**
		*	Gets the pitch of the memory in the row / Y-dimension 
		*/
		virtual uint depthPitch() 	const = 0;
		/**
		*	Gets the size of the buffer {rows, cols, depth}
		*/
		virtual const Size& size()	const = 0;
	};
	/**
	*	Convenience function for printing memory contents
	*	@param[in] str - output stream
	*	@param[in] v - buffer to print out
	*	@return updated output stream for printing
	*/
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const IMemory<T>& v) 
	{ 
		// print something from v to str, e.g: Str << v.getX();
		Str << (std::string)v;
		return Str;
	}


}