/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "DataDescriptor.cuh"
namespace xpl
{



	template<class T>
	struct RegionPtrData
	{
		T* data_ptr;
		uint depthPitch;
	};

	/**
	*	Computes new pointer and depthPitch based on region
	*	@return pointer to start of new region
	*/
	template<class T>
	static RegionPtrData<T> offsetToRegion(	const DataDescriptor<T>& dataDesc, 
											const Region& newRegion, 
											T* data_ptr)
	{
		uint sx = newRegion.size().cols();
		uint sy = newRegion.size().rows();
		uint sz = newRegion.size().depth();
		uint px = newRegion.position().x();
		uint py = newRegion.position().y();
		uint pz = newRegion.position().z();
		//
		// Contracts
		ASSERT( (px+sx) <= dataDesc.size().cols(),  "Cannot reference region outside of memory");
		ASSERT( (py+sy) <= dataDesc.size().rows(),  "Cannot reference region outside of memory");
		ASSERT( (pz+sz) <= dataDesc.size().depth(), "Cannot reference region outside of memory");
		//
		// Offset Basememory ptr to position
		T* new_data_ptr = dataDesc.referenceToRegion(newRegion, data_ptr);
		//
		// depth pitch ( how far to next segment )
		//
		uint depthPitch = dataDesc.size().rows();
		return (RegionPtrData<T>){new_data_ptr, depthPitch};
	}

}