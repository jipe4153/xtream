/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

namespace xpl
{
	/**
	*	Defines how many blocks should be created for a given mesh size and blockDimension,
	*\n	this is based on the coverage definition: how much data a block or individual thread should cover.
	*\n
	*\n	Hence the coverage class computes number of blocks to compute based on the coverage definition in use.
	*/
	class Coverage
	{
	public:
		Coverage(uint x_cov=1, uint y_cov=1, uint z_cov=1)
		: m_x_cov(x_cov),  m_y_cov(y_cov),  m_z_cov(z_cov)
		{}
		__host__ __device__ ~Coverage(){}
		uint x() const{return m_x_cov;}
		uint y() const{return m_y_cov;}
		uint z() const{return m_z_cov;}
		/**
		*	Computes number of blocks dependant on coverage type
		*	@param[in] N - number of elements in this dimension
		*	@param[in] blockDimX - number of threads in X-dimension
		*	@return number of blocks based in X-dimension
		*	NOTE: default is BlockCoverage implementation
		*/
		virtual uint xBlocks(int N, uint blockDimX) const
		{
			return (N   + m_x_cov - 1)/m_x_cov;
		}
		/**
		*	Computes number of blocks dependant on coverage type
		*	@param[in] N - number of elements in this dimension
		*	@param[in] blockDimY - number of threads in Y-dimension
		*	@return number of blocks based in Y-dimension
		*	NOTE: default is BlockCoverage implementation
		*/
		virtual uint yBlocks(int N, uint blockDimY) const
		{
			return (N  + m_y_cov - 1)/m_y_cov;
		}
		/**
		*	Computes number of blocks dependant on coverage type
		*	@param[in] N - number of elements in this dimension
		*	@param[in] blockDimZ - number of threads in Z-dimension
		*	@return number of blocks based in Z-dimension
		*	NOTE: default is BlockCoverage implementation
		*/
		virtual uint zBlocks(int N, uint blockDimZ) const
		{
			return (N  + m_z_cov - 1)/m_z_cov;

		}
	protected:
		uint m_x_cov;
		uint m_y_cov;
		uint m_z_cov;
	};
	/**
	* 	Class that symbolizes data coverage of a block
	*	The input coverage parameters describe how much 
	*	data is covered in X,Y,Z by each block
	*/
	class BlockCoverage : public Coverage
	{
	public:
		BlockCoverage(uint x_cov, uint y_cov, uint z_cov)
		: Coverage(x_cov, y_cov, z_cov)
		{

		}
		BlockCoverage(uint x_cov, uint y_cov)
		: BlockCoverage(x_cov, y_cov, 1)
		{
		}
		BlockCoverage(uint x_cov)
		: BlockCoverage(x_cov, 1, 1)
		{
		}
		/** See Coverage */
		uint xBlocks(int N, uint blockDimX) const
		{
			return (N   + m_x_cov - 1)/m_x_cov;
		}
		/** See Coverage */
		uint yBlocks(int N, uint blockDimY) const
		{
			return (N  + m_y_cov - 1)/m_y_cov;
		}
		/** See Coverage */
		uint zBlocks(int N, uint blockDimZ) const
		{
			return (N  + m_z_cov - 1)/m_z_cov;

		}
	};
	/**
	* 	Class that symbolizes data coverage of a thread
	*	The input coverage parameters describe how much 
	*	data is covered in X,Y,Z by each thread
	*/	
	class ThreadCoverage : public Coverage
	{
	public:
		ThreadCoverage(uint x_cov, uint y_cov, uint z_cov)
		: Coverage(x_cov, y_cov, z_cov)
		{

		}
		ThreadCoverage(uint x_cov, uint y_cov)
		: ThreadCoverage(x_cov, y_cov, 1)
		{
		}
		ThreadCoverage(uint x_cov)
		: ThreadCoverage(x_cov, 1, 1)
		{
		}
		/** See Coverage */
		uint xBlocks(int N, uint blockDimX) const
		{
			return (N   + blockDimX*m_x_cov - 1)/m_x_cov;
		}
		/** See Coverage */
		uint yBlocks(int N, uint blockDimY) const
		{
			return (N  + blockDimY*m_y_cov - 1)/m_y_cov;
		}
		/** See Coverage */
		uint zBlocks(int N, uint blockDimZ) const
		{
			return (N  + blockDimZ*m_z_cov - 1)/m_z_cov;
		}
	};
} // END xpl