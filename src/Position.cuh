/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <sstream>
#include <ostream>

namespace xpl
{
	class Position
	{
	public:
		Position(const uint _x,	const uint _y,const uint _z) 
		: m_x(_x), m_y(_y), m_z(_z)
		{
		}
		Position(const uint _x,	const uint _y) 
		: m_x(_x), m_y(_y), m_z(0)
		{
		}
		Position(const uint _x) 
		: m_x(_x), m_y(0), m_z(0)
		{
		}
		uint x() const	{ return m_x; }
		uint y() const	{ return m_y; }
		uint z() const	{ return m_z; }
		void x(uint x) { m_x = x; }
		void y(uint y) { m_y = y; }
		void z(uint z) { m_z = z; }
	private:
		uint m_x;
		uint m_y; 
		uint m_z; 
	};
	inline
	std::ostream & operator<<(std::ostream & Str, const Position& p) 
	{ 
		Str << " x: " << p.x();
		Str << " y: " << p.y();
		Str << " z: " << p.z();
		return Str;
	}
}




