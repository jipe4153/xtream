/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "DeviceStream.cuh"
#include "ISingleElement.cuh"
#include <memory>
#include "DeviceStruct.cuh"
#include "IBuffer.cuh"

namespace xpl
{
	/**
	*	Base classes for buffers management of streams, utilizes DeviceStream class
	*	Defaults to stream0 
	*/
	class DeviceStreamBase
	{
	public:

		/**
		*	Construcs a default stream
		*/
		DeviceStreamBase()	
		: m_stream(std::make_shared<xpl::DeviceStream >() )
		{	
		}
		virtual ~DeviceStreamBase() {}
		/**
		*	Stream that buffer operations may default to
		*/
		virtual const DeviceStream& stream() const {	return *m_stream;	}
		/**
		*	Set a new stream
		*	@param[in] _stream - new stream 
		*/
		virtual void setStream(std::shared_ptr<DeviceStream > _stream) {	m_stream = _stream;	}
		/*
		*	@return  a stream reference
		*/
		virtual std::shared_ptr<DeviceStream > getStream() const	{ return m_stream;}
		/*
		*	Helper function to setup for concurrent processing
		*	Running this function will setup this buffer to run on a concurrent stream (any non-default stream)d
		*	@param[in] _name - TBD streams will be named in future
		*/
		virtual void setupConcurrentStream(const std::string _name="")
		{
			m_stream.reset(new DeviceStream(concurrent_stream));
		}
	protected:
		/** Device stream on which all operations are queued, defaults to stream 0*/
		std::shared_ptr<DeviceStream > m_stream;
	};

}