/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Size.cuh"
#include "Position.cuh"
#include <CommonAssert.h>
#include "IMemory.cuh"

namespace xpl
{

	/**
	*	DataDescriptor
	*	Describes data orientation on the host/device for a memory, it has knowledge of the 
	*	buffer size and it's data layout ( pitch )
	*/
	template<class T>
	class DataDescriptor
	{

	public:
		/**
		*	@param[in] size - size of memory
		*	@param[in] pitch - memory pitch in x-direction
		*	@param[in] depthPitch - memory pitch in y-direction
		*/
		DataDescriptor(const Size& size, const uint pitch, const uint depthPitch)
		:
			m_size(size),
			m_pitch(pitch),
			m_depthPitch(depthPitch)
		{
		}
		/**
		*	@param[in] memory - memory to construct descriptor from
		*/
		DataDescriptor(const IMemory<T>& memory)
		: DataDescriptor( memory.size(), memory.pitch(), memory.depthPitch())
		{
		}
		/**
		*	@param[in] ptr - data ptr to offset from
		*	@return a memory reference to a region
		*/
		T* referenceToRegion(const Region& r, T* ptr) const
		{
			return &ptr[getDataIndex(r.position())];
		}
		/**
		*	@param[in] p - position to get data index for
		*	@return the data index of the Position 'p', this can be used to access the descriptors buffer
		*/
		uint getDataIndex(const Position& p) const
		{
			ASSERT( m_size.isInSide(p) , "Invalid Position requested");
			return p.x() + p.y() * m_pitch + p.z() * (m_depthPitch * m_pitch);
		}
		/** @return size of data layout*/
		const Size& size() const { return m_size; }
		/** @return memory pitch in x-direction*/
		uint pitch() const { return m_pitch; }
		/** @return memory pitch in y-direction*/
		uint depthPitch() const { return m_depthPitch; }
	private:
		xpl::Size m_size;
		uint m_pitch;
		uint m_depthPitch;
	};
} // END xpl