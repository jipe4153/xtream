/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include "Region.cuh"

namespace xpl
{
	namespace iterator
	{

		template<class Buffer, class BufferIterator>
		void memcpy(Buffer& dstBuffer, const BufferIterator& src)
		{
			Region region(Region(Position(0,0), dstBuffer.size()));
			// Copy from other region to this region
			dstBuffer( region ) = src;
		}
	}  // end namespace iterator
} // end namespace xpl