/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <iomanip>
namespace xpl
{
	/**
	*	Defines the conversion of a scalar value to a string
	*/
	template<class T>
	std::string valueToString(const T& value)
	{
		return "NDEF";
	}
	/**	Defines the conversion of a scalar float value to a string */
	template<> inline std::string valueToString(const float& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar double value to a string */
	template<> inline std::string valueToString(const double& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar uint8_t value to a string */
	template<> inline std::string valueToString(const uint8_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar uint16_t value to a string */
	template<> inline std::string valueToString(const uint16_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar uint32_t value to a string */
	template<> inline std::string valueToString(const uint32_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar uint64_t value to a string */
	template<> inline std::string valueToString(const uint64_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar int8_t value to a string */
	template<> inline std::string valueToString(const int8_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar int16_t value to a string */
	template<> inline std::string valueToString(const int16_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar int32_t value to a string */
	template<> inline std::string valueToString(const int32_t& value) { return std::to_string(value); }
	/**	Defines the conversion of a scalar int64_t value to a string */
	template<> inline std::string valueToString(const int64_t& value) { return std::to_string(value); }
	/**
	*	Defines the conversion of a scalar int2 value to a string
	*/
	template<>
	inline
	std::string valueToString(const int2& value)
	{
		return "["+ std::to_string(value.x) + "," + std::to_string(value.y) + "]";
	}
	/**
	*
	*/
	template<>
	inline
	std::string valueToString(const uchar4& value)
	{
		return "["+ 
				  std::to_string(value.x) + "," 
				+ std::to_string(value.y) + ","
				+ std::to_string(value.z) + ","
				+ std::to_string(value.w)
				+ "]";
	}
	/**
	*	Defines the conversion of a scalar float2 value to a string
	*/
	template<>
	inline
	std::string valueToString(const float2& value)
	{
		return "["+ std::to_string(value.x) + "," + std::to_string(value.y) + "]";
	}
	template<class T>
	std::string toString(const IMemory<T>& memory, const int prec=4)
	{
		//FIXME: Add HostBuffer class here, copy to a host buffer class first
		std::stringstream ss;
		ss << "Memory dimensions: " << memory.rows() << " x " << memory.cols() << " x " << memory.depth();
		// Iterate
		for(uint k = 0; k < memory.depth(); k++)
		{
			ss << std::endl << "Matrix segment: " << k << std::endl;
			for(uint i = 0; i < memory.rows(); i++)
			{
				ss << std::endl;
				for(uint j = 0; j < memory.cols(); j++)
				{
					T value = memory.getValueAt(i,j,k);
					ss << std::setprecision(prec) << valueToString(value) << " ";
				}
			}
		}
		std::string ans = "";
		ans = ss.str();
		return ans;

	}
}