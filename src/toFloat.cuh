/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once


#include <DeviceBuffer.cuh>
#include <TextureBuffer.cuh>
#include <PinnedBuffer.cuh>
#include <device_lambda.cuh>

namespace xpl
{
	/**
	*	Takes in any buffer and outputs a floating point copy
	*/
	template<class BufferOut, class BufferIn>
	void toFloat(BufferOut& output, const BufferIn& input)
	{
		auto copyKernel = xpl_device_lambda()
		{

			int i = device::getY();
			int j = device::getX();
			int k = device::getZ();

			if( i < input.rows() && j < input.cols() && k < input.depth() )
			{
				auto in_val = input(i,j,k);
				output(i,j,k) = (float)in_val;
			}

		};
		device::execute(input.size(),copyKernel);
	}
	/**
	*	 @returns new floating point device buffer
	*/
	template<class T>
	DeviceBuffer<float> toFloat(const DeviceBuffer<T>& input)
	{
		DeviceBuffer<float> output(input.size());
		toFloat(output,input);
		return output;
	}
	/**
	*	 @returns new floating point device buffer
	*/
	template<class T>
	TextureBuffer<float> toFloat(const TextureBuffer<T>& input)
	{
		TextureBuffer<float> output(input.size());
		toFloat(output,input);
		return output;
	}
	template<class T>
	PinnedBuffer<float> toFloat(const PinnedBuffer<T>& input)
	{
		PinnedBuffer<float> output(input.size());
		toFloat(output,input);
		return output;
	}
	template<class T>
	void toFloat(HostBuffer<float>& output,const HostBuffer<T>& input)
	{
		ASSERT(output.size()==input.size(), "Dimension missmatch");

		auto toFloat = [&](const xpl::Index& index) mutable
		{
			output(index) = (float)input(index);
		};
		output.iterate(toFloat);
	}

	template<class T>
	HostBuffer<float> toFloat(const HostBuffer<T>& input)
	{
		HostBuffer<float> output(input.size());
		toFloat(output, input);
		return output;
	}
} // end namespace
