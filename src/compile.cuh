#pragma once
#if __CUDA_ARCH__
#define DEVICE_COMPILE 1
#define HOST_COMPILE 0
#else
#define DEVICE_COMPILE 0
#define HOST_COMPILE 1
#endif
