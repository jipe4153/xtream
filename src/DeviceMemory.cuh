/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Region.cuh"
#include <memory>
#include <memory>

#include <sstream>
#include <ostream>
#include "DeviceStruct.cuh"
#include "IMemory.cuh"
#include <CommonAssert.h>

namespace xpl
{
		/**
		*	DeviceMemory, manages memory on the device.
		*	This represents standard malloc:ed buffers on the GPU utilizing pitched memory.
		*	The maximum dimensionality is 3 - rows X cols X depth
		*/
		template<class T>
		class DeviceMemory : public IMemory<T>
		{
		public:
			/**
			* 	Construct device memory of size - @size
			* 	param[in] size - size in rows x cols x depth
			*/
			DeviceMemory(const Size& size);
			/**
			* 	Create a DeviceMemory object mapped to @data_ptr described by @size
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			*/
			DeviceMemory(T* data_ptr, const Size& size);
			/**
			* 	Create a DeviceMemory object mapped to @data_ptr described by @size
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			* 	param[in] pitch 	- memory pitch in x-direction in number of <T> elements
			*/
			DeviceMemory(T* data_ptr, const Size& size, uint pitch);
			/**
			*	Constructs DeviceMemory that maps an existing data_ptr into it,
			*	this takes care of the @pitch in x-direction and in the y-direction @depthPitch
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			* 	param[in] pitch 	- memory pitch in x-direction in number of <T> elements
			* 	param[in] depthPitch- memory pitch in y-direction in number of <T> elements
			*/
			DeviceMemory(T* data_ptr, const Size& size, uint pitch, uint depthPitch);
			~DeviceMemory();
			/**
			*	@param[in] r - memory region to reference
			*	@return DeviceMemory region with data defined by region @r
			*/
			std::shared_ptr<DeviceMemory<T> > referenceRegion(const Region& r) const;
			/** @return reference to device data ptr */
			T* data() const;
			/** See IMemory for documentation*/
			T  getValueAt(uint i, uint j, uint k) const;
			/** See IMemory for documentation*/
			Region getRegion() const;
			/** See IMemory for documentation*/
			operator std::string() const;
			/** See IMemory for documentation*/
			uint rows() 			const;
			/** See IMemory for documentation*/
			uint cols() 			const;
			/** See IMemory for documentation*/
			uint depth() 		const;
			/** See IMemory for documentation*/
			uint pitch() 		const;
			/** See IMemory for documentation*/
			uint depthPitch() 	const;
			/** See IMemory for documentation*/
			const Size& size()	const;
			/**
			*	@param[in] stream - sets the stream for memory to operate on
			*/
			void setStream(const cudaStream_t& stream);
		private:

			/**Memory allocation private helper function */
			void allocate();
			/** Pointer to host side data */
			T* m_device_ptr;
			/** Indicate ownership of physical memory */
			bool m_memory_owner;
			/** Size in {rows, cols, depth} */
			Size m_size;
			/**  byte alignment in memory X-direction */
			uint m_pitch;
			/**
			* Pitch in nb elements until next valid memory row starts
			* This is generally == m_rows, until an object created by referenceRegion(...)
			* which may update m_rows of the new memory reference but the depthPitch remains the same
			*/
			uint m_depthPitch;
			/** Stream that the memory operates on */
			cudaStream_t m_stream;
		}; // END CLASS
} // namespace xpl
#include "DeviceMemory.cut"