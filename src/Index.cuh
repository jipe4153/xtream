/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <memory>
#include <cuda.h>

namespace xpl
{
	/**
	*	Index container class, contains an index in X,Y,Z (row, col, depth)
	*	Where X/col is the fast dimension
	*
	*	NOTE: This class uses 'int' datatypes to be compatible with both host and device iterators
	*/
	class Index
	{
	public:
		__host__ __device__ Index(  const int row,
									const int col,
									const int depth)
		:
		m_row(row),
		m_col(col),
		m_depth(depth)
		{
		}
		/** @return X-index or column index */
		inline __host__ __device__ int x() const { return m_col; }
		/** @return Y-index or row index */
		inline __host__ __device__ int y() const { return m_row; }
		/** @return Z-index or depth index */
		inline __host__ __device__ int z() const { return m_depth; }
		/**
		*	Check if this index is equal to 'other' xpl::Index
		*	@param[in] other index
		*	@return true if indices are the same
		*/
		inline __host__ __device__ bool operator==(const Index& other) const
		{
			bool same = m_row == other.y() &&
						m_col == other.x() &&
						m_depth == other.z();
			return same;
		}
	private:
		int m_row;
		int m_col;
		int m_depth;


	};
	/**
	*	ToString implementation
	*/
	inline
	std::ostream & operator<<(std::ostream & Str, const Index& ind) 
	{ 
		Str << " x: " << ind.x();
		Str << " y: " << ind.y();
		Str << " z: " << ind.z();
		return Str;
	}

}