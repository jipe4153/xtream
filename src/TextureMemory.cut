/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "TextureMemory.cuh"
#include <CommonAssert.h>
#include "Size.cuh"
#include <memory>
#include "SupportedClassTypes.cuh"
#include "toString.cuh"

namespace xpl
{
	template<class T>
	TextureMemory<T>::TextureMemory(const Size& size)
	: m_size(size),
		m_cuArray(NULL),
		m_texture(0),
		m_surface(0),
		m_stream(0),
		m_x_offset(0),
		m_y_offset(0),
		m_readMode(cudaReadModeElementType),
		m_filterMode(cudaFilterModePoint),
		m_addressMode(cudaAddressModeClamp),
		m_useNormalizedCoords(false),
		m_pitch(size.cols()),
		m_depthPitch(size.rows())
	{
		this->allocate();
		this->createTexturingObjects();
	}
	template<class T>
	TextureMemory<T>::TextureMemory(cudaArray* cuArray, const Size& size)
	: m_size(size),
		m_cuArray(cuArray),
		m_texture(0),
		m_stream(0),
		m_surface(0),
		m_x_offset(0),
		m_y_offset(0),
		m_readMode(cudaReadModeElementType),
		m_filterMode(cudaFilterModePoint),
		m_addressMode(cudaAddressModeClamp),
		m_useNormalizedCoords(false),
		m_pitch(size.cols()),
		m_depthPitch(size.rows())
	{
		this->createTexturingObjects();
	}
	template<class T>
	TextureMemory<T>::TextureMemory(const TextureMemory<T>& other, const Size& size, uint x_offset, uint y_offset)
	{
		// Copy all members
		m_memory_owner = false;
		m_size = size;
		m_x_offset = x_offset;
		m_y_offset = y_offset;
		m_readMode = other.m_readMode;
		m_filterMode = other.m_filterMode;
		m_addressMode = other.m_addressMode;
		m_useNormalizedCoords = other.m_useNormalizedCoords;
		m_cuArray = other.m_cuArray;
		m_texture = other.m_texture;
		m_surface = other.m_surface;
		m_resDesc = other.m_resDesc;
		m_stream = other.m_stream;
	}
	template<class T>
	TextureMemory<T>::~TextureMemory()
	{

		if( m_memory_owner )
		{
			cudaDestroySurfaceObject(m_surface);
			cudaDestroyTextureObject(m_texture);
			cudaFreeArray(m_cuArray);
		}
	}
	template<class T>
	void TextureMemory<T>::setStream(const cudaStream_t& stream)
	{
		m_stream = stream;
	}
	template<class T>
	std::shared_ptr<TextureMemory<T> > TextureMemory<T>::referenceRegion(const Region& r ) const
	{
		uint sx = r.size().cols();
		uint sy = r.size().rows();
		uint sz = r.size().depth();
		uint px = r.position().x();
		uint py = r.position().y();
		uint pz = r.position().z();
		//
		// Contracts
		ASSERT( (px+sx) <= cols(),  "Cannot reference region outside of memory");
		ASSERT( (py+sy) <= rows(),  "Cannot reference region outside of memory");
		ASSERT( (pz+sz) <= depth() , "Cannot reference region outside of memory");
		//
		// Offset Basememory ptr to position
		uint x_offset = px;
		uint y_offset = py + (pz)*m_depthPitch;
		//
		// Create a new texture memory object with size of the region and the offsets
		//
		return std::make_shared<TextureMemory<T> >(*this, r.size() , x_offset, y_offset);
	}
	template<class T>
	void TextureMemory<T>::createTextureObject()
	{

		if( m_texture != 0)
			cudaDestroyTextureObject(m_texture);

			cudaTextureDesc texDesc;
		memset(&texDesc, 0, sizeof(texDesc));

		texDesc.addressMode[0]   = m_addressMode;
		texDesc.addressMode[1]   = m_addressMode;
		texDesc.filterMode       = m_filterMode;
		texDesc.readMode         = m_readMode;
		texDesc.normalizedCoords = m_useNormalizedCoords ? 1 : 0;
		
		// Create texture object
		cudaCreateTextureObject(&this->m_texture, &this->m_resDesc, &texDesc, NULL);
		ASSERT(m_texture != 0, " cudaCreateTextureObject gave NULL texture");
		CUDA_ERROR_ASSERT();

	}
	template<class T>
	void TextureMemory<T>::allocate()
	{
		//
		// allocate array
		//
		cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<T>();
		cudaArray* _cu_array = NULL;
    	cudaMallocArray(  &_cu_array, &channelDesc, cols(), rows()*depth(), 
    					cudaArraySurfaceLoadStore // Read and writeable
    					); 
    	this->m_cuArray = _cu_array;
    	ASSERT(m_cuArray != NULL, "cudaMallocArray failure");
    	CUDA_ERROR_ASSERT();
        
	}
	template<class T>
	void TextureMemory<T>::createTexturingObjects()
	{
		ASSERT(m_cuArray!=NULL, "Cannot create texturing objects without valid cuda array");
    	//
    	//  cuArray Resource descriptor
    	//
		memset(&this->m_resDesc, 0, sizeof(m_resDesc));
		m_resDesc.resType = cudaResourceTypeArray;
		m_resDesc.res.array.array = m_cuArray;
		//
    	// Create & setup texture used for reading from cudaArray
		createTextureObject();
		//
		// Create surface object used for writing to cudaArray
		// reference to surface to write data to:
		cudaCreateSurfaceObject(&this->m_surface, &this->m_resDesc);
        CUDA_ERROR_ASSERT();
	}
	template<class T>
	T TextureMemory<T>::getValueAt(uint i, uint j, uint k) const
	{
		uint x_index = j;
		uint y_index = i + k*m_depthPitch;
		//
		// Apply offset (default offset is zero):
		x_index += m_x_offset;
		y_index += m_y_offset;
		// contracts:
		ASSERT(j < cols(), "");
		ASSERT(i < rows(), "");
		ASSERT(k < depth(), "");

		T value;
    	cudaMemcpyFromArrayAsync(&value, this->array(), x_index*sizeof(T), y_index, sizeof(T), cudaMemcpyDeviceToHost, m_stream);
		return value;
	}
	template<class T>
	void TextureMemory<T>::setAddressMode(const cudaTextureAddressMode& mode)
	{
		m_addressMode = mode;
		createTextureObject();
	}
	
	template<class T>
	void TextureMemory<T>::setFilterMode(const cudaTextureFilterMode& mode)
	{
		m_filterMode = mode;
		createTextureObject();
	}
	template<class T>
	void TextureMemory<T>::setReadMode(const cudaTextureReadMode& mode)
	{
		m_readMode = mode;
		createTextureObject();
	}
	template<class T>
	void TextureMemory<T>::setUseNormalizedCoords(const bool useNormalizedCoords)
	{
		m_useNormalizedCoords = useNormalizedCoords;
		createTextureObject();
	}
	template<class T>
	cudaTextureObject_t TextureMemory<T>::texture() const
	{
		return m_texture;
	}
	template<class T>
	cudaSurfaceObject_t TextureMemory<T>::surface() const
	{
		return m_surface;
	}
	template<class T>
	cudaArray* TextureMemory<T>::array() const
	{
		return m_cuArray;
	}

	template<class T>
	int TextureMemory<T>::xOffset() const
	{
		return m_x_offset;
	}
	template<class T>
	int TextureMemory<T>::yOffset() const
	{
		return m_y_offset;
	}
	template<class T>
	TextureMemory<T>::operator std::string() const
	{
		return xpl::toString(*this);			
	}
	template<class T>
	Region TextureMemory<T>::getRegion() const
	{
		return Region(Position(0,0,0), size());
	}
	template<class T>
	uint TextureMemory<T>::rows() 			const
	{
		return m_size.rows();
	}	
	template<class T>
	uint TextureMemory<T>::cols() 			const
	{
		return m_size.cols();
	}
	template<class T>
	uint TextureMemory<T>::depth() 		const
	{
		return m_size.depth();
	}
	template<class T>
	uint TextureMemory<T>::pitch() 		const
	{
		return m_pitch;
	}
	template<class T>
	uint TextureMemory<T>::depthPitch() 	const
	{
		return m_depthPitch;
	}
	template<class T>
	const Size& TextureMemory<T>::size()	const
	{
		return m_size;
	}
} // END NAMESPACE xpl





