/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#pragma once
#include <stdint.h>

/**
*	Helper macro to take in full class name and compile.
*	example: 
*	adding: COMPILE_CLASS_DATA_TYPES_EXTENDED(xpl::DeviceBuffer) to the end of DeviceBuffer.cu file
*	makes sure that this class is compiled for all supported data-types
*	Will 
*
*/
/*
#define COMPILE_CLASS_DATA_TYPES(NAMESPACE_CLASS_NAME) \
template class NAMESPACE_CLASS_NAME<double>;
#define COMPILE_CLASS_DATA_TYPES_EXTENDED(NAMESPACE_CLASS_NAME) \
COMPILE_CLASS_DATA_TYPES(NAMESPACE_CLASS_NAME)
*/

#define COMPILE_CLASS_DATA_TYPES(NAMESPACE_CLASS_NAME) \
template class NAMESPACE_CLASS_NAME<float>; \
template class NAMESPACE_CLASS_NAME<int>; \
template class NAMESPACE_CLASS_NAME<uint16_t>; \
template class NAMESPACE_CLASS_NAME<uint8_t>; \
template class NAMESPACE_CLASS_NAME<uint>;


#define COMPILE_CLASS_DATA_TYPES_EXTENDED(NAMESPACE_CLASS_NAME) \
COMPILE_CLASS_DATA_TYPES(NAMESPACE_CLASS_NAME) \
template class NAMESPACE_CLASS_NAME<double>;

