/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#pragma once
#include <cuda.h>
#include "DataDescriptor.cuh"
#include "ISingleElement.cuh"
#include "DeviceStream.cuh"

namespace xpl
{
	
	/**
	*	Defines reading and writing of single elements to/from the device
	*/
	template<class T>
	class DeviceSingleElement : public ISingleElement<T>
	{
	public:
		/**
		*	DeviceSingleElement is configured with a data descriptor and a position
		*	This class reads from or writes to an element at 'p'
		*/
		DeviceSingleElement(T* data_ptr=NULL, const DeviceStream& stream=DeviceStream())
		: m_stream(stream), m_element_ptr(data_ptr)
		{
		}
		~DeviceSingleElement()
		{}
		void setElementPtr(T* data_ptr)
		{
			m_element_ptr = data_ptr;
		}
		void setStream(const DeviceStream& stream)
		{
			m_stream = stream;
		}
		/**
		* 	Returns a value at a buffer position
		*/
		virtual operator T() const
		{
			T value;
			cudaMemcpyAsync(&value, m_element_ptr, sizeof(T), cudaMemcpyDeviceToHost, m_stream.get());
			return value;
		}
		/**
		* 	Set device to this value: a Positions indicated in constuctor
		*	@param value indicates the value to write to the device
		*	@return element object from which the value may be get and set
		*/
		virtual ISingleElement<T>& operator=(const T& value)
		{
			cudaMemcpyAsync(m_element_ptr, &value, sizeof(T), cudaMemcpyHostToDevice, m_stream.get());
			return *this;
		}

	private:
		T* m_element_ptr;
		DeviceStream m_stream;
	};
}


