/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "TextureMemory.cuh"

namespace xpl
{
	/**
	*	SurfaceTexture manages device side reading and writing to an array 
	*	via either surface or texture objects.
	*
	*	The reading/writing address is set via x,y,z floats.
	*	NOTE: this class is intented for internal use only by for example TextureStruct<T> objects
	*/
	template<class T>
	class SurfaceTexture
	{
	public:
		/*
		*	@param[in] memory - memory to construct surface texture from
		*/
		SurfaceTexture(const TextureMemory<T>& memory);
		SurfaceTexture();
		/** @return texture fetchec value at (x,y,z)*/
		__device__ __forceinline__ operator T() const;
		/** 
		*	@param[in] value - will be written to a surface at (x,y,z)
		* 	@return SurfaceTexture object
		*/
		__device__ __forceinline__ SurfaceTexture<T>& operator=(T value);
		/** 
		*	Writes to an array via a surface object
		*	@param[in] value - will be written to a surface at (x,y,z)
		*	@param[in] i - row index to write to
		*	@param[in] j - column index to write to
		*	@param[in] k - depth index to write to
		*/
		__device__ __forceinline__ void write(const T& value, int i, int j, int k);
		/** 
		*	Reads from an array via a texture object
		*	@param[in] y - float row index to fetch from
		*	@param[in] x - float col index to fetch from
		*	@param[in] z - float depth index to fetch from
		*	@return texture fetched value from array at (x,y,z)
		*/
		__device__ __forceinline__ T read(float y, float x, float z) const;

				
		float m_x;
		float m_y;
		float m_z;
		float m_x_offset;
		float m_y_offset;
		float m_depthPitch;
		int32_t m_depthPitch_INT32;
		int m_rows;
		int m_cols;
		int m_depth;

		cudaSurfaceObject_t m_surface;
		cudaTextureObject_t m_texture;
	}; // END SurfaceTexture
} // END xpl
#include "SurfaceTexture.cut"