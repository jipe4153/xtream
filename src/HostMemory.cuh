/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Region.cuh"
#include <memory>
#include <sstream>
#include <ostream>

#include "IMemory.cuh"

namespace xpl
{
		/**
		*	HostMemory, manages memory on the host, see IMemory for further documentation.
		*/
		template<class T>
		class HostMemory : public IMemory<T>
		{
			typedef IMemory<T> super;

		public:
			/**
			* Construct host memory of @size
			* param[in] size - size in rows x cols x depth
			*/
			HostMemory(const Size& size);
			/**
			* 	Create a HostMemory object mapped to @data_ptr described by @size
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			*/
			HostMemory(T* data_ptr, const Size& size);
			/**
			* 	Create a HostMemory object mapped to @data_ptr described by @size
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			* 	param[in] pitch 	- memory pitch in x-direction in number of <T> elements
			*/
			HostMemory(T* data_ptr, const Size& size, uint pitch);
			/**
			*	Constructs HostMemory that maps an existing data_ptr into it,
			*	this takes care of the @pitch in x-direction and in the y-direction @depthPitch
			* 	param[in] data_ptr 	- pointer to contiguos memory region
			* 	param[in] size 		- size in rows x cols x depth
			* 	param[in] pitch 	- memory pitch in x-direction in number of <T> elements
			* 	param[in] depthPitch- memory pitch in y-direction in number of <T> elements
			*/
			HostMemory(T* data_ptr, const Size& size, uint pitch, uint depthPitch);
			~HostMemory();
			/** @return reference to host data ptr */
			T* data() const;
			/**
			*	Copies data @data_ptr into this HostMemory, assumes a contiguos
			*	data region of size = rows X cols X depth with cols as the fast dimension
			*	@param[in] data_ptr data to be copied to this host memory
			*/
			void store(T* data_ptr);
			/**
			*	@param[in] r - memory region to reference
			*	@return HostMemory region with data defined by region @r
			*/
			std::shared_ptr<HostMemory<T> > referenceRegion(const Region& r ) const;
			
			/** See IMemory for documentation*/
			T getValueAt(uint i, uint j, uint k) const;
			/** See IMemory for documentation*/
			Region getRegion() const;
			/** See IMemory for documentation*/
			operator std::string() const;
			/** See IMemory for documentation*/
			uint rows() 			const;
			/** See IMemory for documentation*/
			uint cols() 			const;
			/** See IMemory for documentation*/
			uint depth() 		const;
			/** See IMemory for documentation*/
			uint pitch() 		const;
			/** See IMemory for documentation*/
			uint depthPitch() 	const;
			/** See IMemory for documentation*/
			const Size& size()	const;
		private:
			/**Memory allocation private helper function */
			void allocate();
			/** Pointer to host side data */
			T* m_host_ptr;
			/** Indicate ownership of physical memory */
			bool m_memory_owner;
			/** Size in {rows, cols, depth} */
			Size m_size;
			/**  byte alignment in memory X-direction */
			uint m_pitch;
			/**
			* Pitch in nb elements until next valid memory row starts
			* This is generally == m_rows, until an object created by referenceRegion(...)
			* which may update m_rows of the new memory reference but the depthPitch remains the same
			*/
			uint m_depthPitch;
		}; // END CLASS
} // END xpl
#include "HostMemory.cut"