/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <CommonAssert.h>
// (*_*)
//#include "DeviceBuffer.cuh"
#include "HostMemory.cuh"
#include "PinnedMemory.cuh"
#include "cuda.h"
//#include "HostBuffer.cuh"

#include "memcpyDeviceToHost.cuh"
#include "memcpyTextureToHost.cuh"


namespace xpl
{
	namespace cross
	{
		
		/*
		*	Copy between buffer on device side and host side
		*	
		*/
		template<class T>
		void memcpy(HostBuffer<T>& dst, const DeviceBuffer<T>& src)
		{
			memcpyDeviceToHost(dst.memory(), src.memory(), src.stream().get());
		}
		template<class T>
		void memcpy(HostBuffer<T>& dst, const PinnedBuffer<T>& src)
		{
			memcpyDeviceToHost(dst.memory(), src.memory(), src.stream().get());
		}
		template<class T>
		void memcpy(HostBuffer<T>& dst, const TextureBuffer<T>& src)
		{
			memcpyTextureToHost(dst.memory(), src.memory(), src.stream().get());
		}
		template<class T>
		void memcpy(DeviceBuffer<T>& dst, const HostBuffer<T>& src)
		{
			memcpyHostToDevice(dst.memory(), src.memory(), src.stream().get());
		}
		template<class T>
		void memcpy(PinnedBuffer<T>& dst, const HostBuffer<T>& src)
		{
			memcpyHostToDevice(dst.memory(), src.memory(), src.stream().get());
		}
		template<class T>
		void memcpy(TextureBuffer<T>& dst, const HostBuffer<T>& src)
		{
			memcpyHostToTexture(dst.memory(), src.memory(), src.stream().get());
		}
		
		//
		// MEMORY
		//
		template<class T>
		void memcpy(HostMemory<T>& dst, const DeviceMemory<T>& src, cudaStream_t stream)
		{
			memcpyDeviceToHost(dst, src, stream);
		}
		template<class T>
		void memcpy(HostMemory<T>& dst, const PinnedMemory<T>& src, cudaStream_t stream)
		{
			memcpyDeviceToHost(dst, src, stream);
		}
		template<class T>
		void memcpy(HostMemory<T>& dst, const TextureMemory<T>& src, cudaStream_t stream)
		{
			memcpyTextureToHost(dst, src, stream);
		}
		template<class T>
		void memcpy(DeviceMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
		{
			memcpyHostToDevice(dst, src, stream);
		}
		template<class T>
		void memcpy(PinnedMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
		{
			memcpyHostToDevice(dst, src, stream);
		}
		template<class T>
		void memcpy(TextureMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
		{
			memcpyHostToTexture(dst, src, stream);
		}
	}
}