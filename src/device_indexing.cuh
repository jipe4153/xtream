/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

namespace xpl
{

namespace device
{
	template<int DIM_X>
	__device__ inline int getX()
	{
		return threadIdx.x + blockIdx.x*DIM_X;
	}
	__device__ inline int getX()
	{
		return threadIdx.x + blockIdx.x*blockDim.x;
	}
	template<int DIM_Y>
	__device__ inline int getY()
	{
		return threadIdx.y + blockIdx.y*DIM_Y;
	}
	__device__ inline int getY()
	{
		return threadIdx.y + blockIdx.y*blockDim.y;
	}
	template<int DIM_Z>
	__device__ inline int getZ()
	{
		return threadIdx.z + blockIdx.z*DIM_Z;
	}
	__device__ inline int getZ()
	{
		return threadIdx.z + blockIdx.z*blockDim.z;
	}
}
}