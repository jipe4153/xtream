/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

namespace xpl
{
	/**
	*	An interface class for how to set and get single elements of a data object
	*/
	template<class T>
	class ISingleElement
	{
		public:
			virtual ~ISingleElement() {}
			/**
			*	Operator which will return a single element T (typically a POD)
			*	@return a single element
			*/
			virtual operator T() const = 0;
			/**
			*	@param value value to be set by this operator
			*	@return reference to *this
			*/
			virtual	ISingleElement<T>& operator=(const T& value) = 0;
	};

}