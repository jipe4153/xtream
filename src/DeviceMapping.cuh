/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Coverage.cuh"
#include "Size.cuh"

namespace xpl
{
	/**
	*	Describes the size of a thread block and how much data it should cover.
	*	It essentially maps threads to data elements and describes their data-coverage
	*	NOTE: we've separated the notion of block dimensions (X,Y,and Z dim of a thread block) and data coverage of said block
	*/
	class DeviceMapping
	{

	public:
		/**
		*	Specify number of threads for grid creation thread<-->data mapping (coverage)
		*\n
		*\n	Coverage is:
		*\n	How much data the block covers in X, Y, and Z direcdtion
		*\n	analogy: Covering more data means we will have fewer blocks in the grid
		*
		*	@param[in] xThreads - number of x-threads in thread block
		*	@param[in] yThreads - number of y-threads in thread block
		*	@param[in] zThreads - number of z-threads in thread block
		*	@param[in] coverage - data coverage by a thread or entire thread block (see BlockCoverage & ThreadCoverage)
		*/
		DeviceMapping(uint xThreads, uint yThreads, uint zThreads, 
						const Coverage& coverage)
		: 
			m_dataCoverage(coverage)		
		{
			//
			// Block dimensions
			m_blockDim.x = xThreads;
			m_blockDim.y = yThreads;
			m_blockDim.z = zThreads;

		}
		/**
		*	Specify number of threads for grid creation thread<-->data mapping is default (1-1)
		*	@param[in] xThreads - number of x-threads in thread block
		*	@param[in] yThreads - number of y-threads in thread block
		*	@param[in] zThreads - number of z-threads in thread block
		*/
		DeviceMapping(uint xThreads, uint yThreads, uint zThreads)
		: 
			m_dataCoverage(BlockCoverage(xThreads,yThreads,zThreads))		
		{
			m_blockDim.x = xThreads;
			m_blockDim.y = yThreads;
			m_blockDim.z = zThreads;
		}		
		/**
		*	Specify number of threads for grid creation thread<-->data mapping is default (1-1)
		*	@param[in] xThreads - number of x-threads in thread block
		*	@param[in] yThreads - number of y-threads in thread block
		*/
		DeviceMapping(uint xThreads, uint yThreads)
		: DeviceMapping(xThreads, yThreads, 1, BlockCoverage(xThreads, yThreads))
		{
		}
		/**
		*	Specify number of threads for grid creation thread<-->data mapping is default (1-1)
		*	@param[in] xThreads - number of x-threads in thread block
		*/
		DeviceMapping(uint xThreads)
		: DeviceMapping(xThreads, 1, 1, BlockCoverage(xThreads) )
		{
		}
		/**
		*	Specify number of threads for grid creation thread<-->data mapping (coverage)
		*	Coverage is:
		* 	How much data the block covers in X, Y, and Z direction
		* 	analogy: Covering more data means we will have fewer blocks in the grid
		*	@param[in] xThreads - number of x-threads in thread block
		*	@param[in] yThreads - number of y-threads in thread block
		*	@param[in] coverage - data coverage by a thread or entire thread block (see BlockCoverage & ThreadCoverage)
		*/
		DeviceMapping(uint xThreads, uint yThreads, const BlockCoverage& coverage)
		: DeviceMapping(xThreads, yThreads, 1, coverage)
		{
		}
		/**
		*	Specify number of threads for grid creation thread<-->data mapping (coverage)
		*	Coverage is:
		* 	How much data the block covers in X, Y, and Z direction
		* 	analogy: Covering more data means we will have fewer blocks in the grid
		*	@param[in] xThreads - number of x-threads in thread block
		*	@param[in] coverage - data coverage by a thread or entire thread block (see BlockCoverage & ThreadCoverage)
		*/
		DeviceMapping(uint xThreads, const BlockCoverage& coverage)
		: DeviceMapping(xThreads, 1, 1, coverage)
		{
		}
		/*
		*	A default standard mapping between thread blocks and data
		*	This construcdtor does it 1 to 1
		*/ 
		DeviceMapping()
		: DeviceMapping(32,8,1, BlockCoverage(32,8,1))
		{
		}
		/**
		*	Creates a thread block to data coverage mapping based on the intended mesh size
		*	utilizes some default thread block and coverage configurations.
		*	@param[in] meshSize - size of data to map to
		*/
		DeviceMapping(const Size& meshSize)
		{

			uint xThreads = 32;
			uint yThreads = 8;
			uint zThreads = 1;

			if( meshSize.rows()==1)
			{
				xThreads = 4*xThreads;
				yThreads = 1;
				zThreads = 1;
			}

			m_dataCoverage = BlockCoverage(xThreads, yThreads, zThreads);
			//
			// Block dimensions
			m_blockDim.x = xThreads;
			m_blockDim.y = yThreads;
			m_blockDim.z = zThreads;

		}
		/** @return block dimensison*/
		dim3 blockDim() const
		{
			return m_blockDim;
		}
		/** @return data coverage*/
		const Coverage& dataCoverage() const
		{
			return m_dataCoverage;
		}
		/** 
		*	@param[in] meshSize - size of data to map our thread block to
		*	@return computes the grid dimensions based on the @meshSize
		*/
		dim3 getGridDim(const Size& meshSize) const
		{
			dim3 gridDim;

			gridDim.x = m_dataCoverage.xBlocks(meshSize.cols(), m_blockDim.x);
			gridDim.y = m_dataCoverage.yBlocks(meshSize.rows(), m_blockDim.y);
			gridDim.z = m_dataCoverage.zBlocks(meshSize.depth(), m_blockDim.z);

			return gridDim;
		}

	private:
		/** Thread block dimensions in x,y,z */
		dim3 m_blockDim;
		/** How much data each thread is expected to cover */
		Coverage m_dataCoverage;
	};


}