/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "DeviceStruct.cuh"
#include "TextureStruct.cuh"
#include <CommonAssert.h>
#include "device_indexing.cuh"
#include "device_lambda.cuh"

namespace xpl
{
	namespace device
	{
		/**
		*	Executes a device "for_each" element lambda on a device seide buffer
		*	@param buffer - a device side buffer
		*	@param lambda - device lambda to execute (required form: [=]__device__(T& val))
		*/
		template<class Lambda, class BufferType>
		void for_each(BufferType& buffer, Lambda lambda)
		{

			auto for_each_kernel = xpl_device_lambda()
			{
				int i = device::getY();
				int j = device::getX();
				int k = device::getZ();

				if( j < buffer.cols() && i < buffer.rows() && k < buffer.depth())
				{
					auto val = buffer.read(i,j,k);
					lambda(val);
					buffer.write(val, i,j,k);
				}
			};
			device::execute(DeviceGrid(buffer.size()), buffer.stream(), for_each_kernel);
		}
	} // END device
} // END xpl
/** 
*	Helper macfor for device lambdas
*
*	Ex:
*	DeviceBuffer<float> a(size);
*	
*	a.for_each(device_for_each(float& val)
*	{
*		val += 42.0f;
*	});
*
*/
#define device_for_each [=]__device__