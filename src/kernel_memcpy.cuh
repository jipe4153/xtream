/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include "device_lambda.cuh"
namespace kernel
{

    //
    // Memcpy between device buffers
    //
    // TODO: refactor these memcpy calls
    template<class DstStruct, class SrcStruct>
    void memcpy_call(   DstStruct dst,
                        const SrcStruct src, 
                        const cudaStream_t stream)
    {

        const int DIM_X = 128;
        //ASSERT(src.size() == dst.size(),"Dimensions missmatch");

        xpl::Size sz(src.rows(), src.cols(), src.depth());

        auto grid = xpl::DeviceGrid( sz, xpl::DeviceMapping(DIM_X));
        /**
        *   memcpy_kernel
        *   Performs memory copy on GPU from src to destination
        */
        auto memcpy_kernel = xpl_device_lambda()
        {
            int x = threadIdx.x  + DIM_X * blockIdx.x;
            int y = blockIdx.y;
            int z = blockIdx.z;

            if(x < src.cols()  && y < src.rows())
            {
                // read from src
                //T src_val = src.read(y, x, z);
                // write to dst
                dst.write( src.read(y, x, z) ,y, x, z);
            }
        };
        xpl::device::execute(grid, memcpy_kernel);
        CUDA_ERROR_ASSERT();
    }


} // namespace kernel