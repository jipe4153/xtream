/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Region.cuh"
#include <memory>
#include <memory>

#include <sstream>
#include <ostream>
#include <CommonAssert.h>
#include "IMemory.cuh"

namespace xpl
{
		/**
		*	TextureMemory class
		*	Textures allow for 2D & 3D spatial data caching aswell as fast hardware addressing and interpolation.
		*	This class fetches (reads) data from an array via textures and writes them to the same array via surfaces.
		*	NOTE: Supports efficient 2D interpolation but not 3D (TODO)
		*/
		template<class T>
		class TextureMemory : public IMemory<T>
		{
		public:
			TextureMemory(const Size& size);
			/**
			*	Creates a texture memory object from an existing cudaArray of some @size
			*	A new texture and surface reference will be bound to this array
			*	@param[in] cuArray - cuda array memory
			*	@param[in] size - dimensions os memory
			*/
			TextureMemory(cudaArray* cuArray, const Size& size);
			/** 
			*	Constructor for creating a memory reference with given size and buffer offsets
			*/
			TextureMemory(const TextureMemory<T>& other, const Size& size, uint x_offset, uint y_offset);
			~TextureMemory();
			/**
			*	Addressing mode of the texture at boundaries:
			*
			*	cudaAddressModeWrap 	Wrapping address mode
			*	cudaAddressModeClamp 	Clamp to edge address mode
			*	cudaAddressModeMirror 	Mirror address mode
			*	cudaAddressModeBorder 	Border address mode
			*	@param[in] mode - addressing mode to use
			*
			*	@NOTE this will re-create the 
			*/
			void setAddressMode(const cudaTextureAddressMode& mode);
			/**
			*	Nearest point (nearest neighboor) or linear pixel filtering
			*
			*	Modes:
			*	cudaFilterModePoint 	Point filter mode
			*	cudaFilterModeLinear 	Linear filter mode
			*	
			*	@param[in] mode - texture filtering mode
			*/
			void setFilterMode(const cudaTextureFilterMode& mode);
			/**
			*	Read mode determines if value should be converted to <float> and value range normalized when a
			*	texure fetch occurs
			*	
			*	A read of 8- or 16-bit data is converted to a float in the range of [0,1.0f] for unsigned integers
			*	and [-1.0f, 1.0f] for signed integers. (32-bit integer is undocumented in CUDA PG).
			*
			*	cudaReadModeElementType 	- Read texture as specified element type (original data)
			*	cudaReadModeNormalizedFloat - Read texture as normalized float (will convert to <float>)
			*
			*	DEFAULT value is cudaReadModeElementType (no conversion)
			*	@param[in] mode - readm mode to use
			*/
			void setReadMode(const cudaTextureReadMode& mode);
			/**
			*	Normalized coords yield indexing in range of [0,1)  for each respective dimension
			*	Non-normalized coords yield indexing in the range of [0,N-1] for each respective dimension
			*
			*	DEFAULT is non-normalized
			*	@param[in] useNormalizedCoords - set to true for coordinates in [0,1] range
			*/
			void setUseNormalizedCoords(const bool useNormalizedCoords);
			/** @returns texture reference */
			cudaTextureObject_t texture() const;
			/** @returns surface reference */
			cudaSurfaceObject_t surface() const;
			/** @returns reference to array memory */
			cudaArray* array() const;
			/** @returns array offset in x-direction */
			int xOffset() const;
			/** @returns array offset in y-direction */
			int yOffset() const;
			/**
			*	@param[in] r - memory region to reference
			*	@return DeviceMemory region with data defined by region @r
			*/
			std::shared_ptr<TextureMemory<T> > referenceRegion(const Region& r) const;
			/** See IMemory for documentation*/
			T  getValueAt(uint i, uint j, uint k) const;
			/** See IMemory for documentation*/
			Region getRegion() const;
			/** See IMemory for documentation*/
			operator std::string() const;
			/** See IMemory for documentation*/
			uint rows() 			const;
			/** See IMemory for documentation*/
			uint cols() 			const;
			/** See IMemory for documentation*/
			uint depth() 		const;
			/** See IMemory for documentation*/
			uint pitch() 		const;
			/** See IMemory for documentation*/
			uint depthPitch() 	const;
			/** See IMemory for documentation*/
			const Size& size()	const;
			/**
			*	@param[in] stream - sets the stream for memory to operate on
			*/
			void setStream(const cudaStream_t& stream);
		private:			
			/** Allocate cuda array */
			void allocate();
			/** Helper class to create surface and texture objecdts */
			void createTexturingObjects();
			/** Creates the texture object based on texture settings */
			void createTextureObject();
			/** offset in x-direction of this memory */
			int m_x_offset;
			/** offset in y-direction of this memory */
			int m_y_offset;
			/** Array holding data */
			cudaArray* m_cuArray;
			/** reference to exture to read data through*/
			cudaTextureObject_t m_texture;
			/** reference to surface to write data to*/
			cudaSurfaceObject_t m_surface;
			/** Descriptor */			
			cudaResourceDesc m_resDesc;
			/** Addressing mode near boundaries*/			
			cudaTextureAddressMode m_addressMode;
			/** Type of texture filtering: Neares neighboor or linear*/			
			cudaTextureFilterMode m_filterMode;
			/** Conversion on texture fetch to normalized float value OR read original value*/			
			cudaTextureReadMode m_readMode;
			/** Normalized coordinates : ex [0,N] --> [0,1.0f] */
			bool m_useNormalizedCoords;
			/** Indicate ownership of physical memory */
			bool m_memory_owner;
			/** Size in {rows, cols, depth} */
			Size m_size;
			/**  byte alignment in memory X-direction */
			uint m_pitch;
			/**
			* Pitch in nb elements until next valid memory row starts
			* This is generally == m_rows, until an object created by referenceRegion(...)
			* which may update m_rows of the new memory reference but the depthPitch remains the same
			*/
			uint m_depthPitch;
			/** Stream that the memory operates on */
			cudaStream_t m_stream;
		}; // END CLASS TextureMemory
} // namespace xpl
#include "TextureMemory.cut"