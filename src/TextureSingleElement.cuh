/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#pragma once
#include <cuda.h>

#include "Position.cuh"
#include "DataDescriptor.cuh"
#include "ISingleElement.cuh"
#include "DeviceStream.cuh"

namespace xpl
{
	
	/**
	*	Defines reading and writing of single elements to/from the device texture memory
	*/
	template<class T>
	class TextureSingleElement : public ISingleElement<T>
	{
	public:
		/**
		*	TextureSingleElement is configured with a data descriptor and a position
		*	This class reads from or writes to an element at 'p'
		*/
		TextureSingleElement(cudaArray* _array, std::shared_ptr<DeviceStream> _stream, int depthPitch)
		: 
			m_array(_array),
			m_stream(_stream),
			m_depthPitch(depthPitch),
			m_x_offset(0),
			m_y_offset(0),
			m_z_offset(0)
		{

		}
		~TextureSingleElement()	{}
		/**
		*	Sets the position
		*	@param[in] row
		*	@param[in] col
		*	@param[in] depth
		*/
		void setPosition(uint row, uint col, uint depth)
		{
			m_x_offset = col;
			m_y_offset = row;
			m_z_offset = depth;
		}
		/**
		* 	Returns a value at a buffer position
		*/
		virtual operator T() const
		{
			uint x_index = m_x_offset;
			uint y_index = m_y_offset + m_z_offset*m_depthPitch;
			T value;
			cudaMemcpyFromArrayAsync(&value, m_array, x_index*sizeof(T), y_index, sizeof(T), cudaMemcpyDeviceToHost, m_stream->get());

			return value;
		}
		/**
		* 	Set device to this value: a Positions indicated in constuctor
		*	@param value indicates the value to write to the device
		*	@return element object from which the value may be get and set
		*/
		virtual ISingleElement<T>& operator=(const T& value)
		{

			cudaMemcpyToArrayAsync(m_array, m_x_offset*sizeof(T), m_y_offset, &value, sizeof(T), cudaMemcpyHostToDevice, m_stream->get());
			return *this;
		}

	private:
		T* m_element_ptr;
		uint m_x_offset;
		uint m_y_offset;
		uint m_z_offset;
		uint m_depthPitch;
		cudaArray* m_array;
		std::shared_ptr<DeviceStream>  m_stream;

	};


}


