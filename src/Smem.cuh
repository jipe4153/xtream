/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Coverage.cuh"
#include "BlockStencil.cuh"
namespace xpl
{

	/**	
	*	Shared memory abstraction class:
	*		* dynamically allocates shared memory on the device
	*		* autmated caching for each block.
	*		* border mapping specified by a stencil)
	*
	*	The block of threads covers some data, the stencil in turns specifies how much border data to give access to.	
	*
	*	USAGE EXAMPLES(load to shared memory and read):	
	*
	*		DeviceBuffer<float> in(rows,cols);
	*		auto grid = DeviceGrid(in.size());
	*		Smem<float> smem( grid, BlockStencil(1,1));
	*
	*		auto kernel = xpl_device_lambda()
	*		{
	*			smem.cache(in);
	*			// ....
	*			// use smem here:
	*			float sum = 0.0f;
	*			for(int y : {-1,0,1})
	*				for(int x : {-1,0,1})
	*					sum += smem(threadIdx.y+y, threadIdx.x+x);
	*
	*			// do something more ...
	*		};
	*		device::execute(grid, kernel);
	*/	
	template<class T>
	class Smem
	{
		public:
			/**
			*	@param[in,out] grid - The grid which the kernel will execute over
			*	@param[in] dataCoverage - ie how much data to cover in X,Y,Z (excluding border values)
			*	@param[in] stencil - Stencil that defines how much border data to store in shared memory cache
			*/
			__host__ Smem(DeviceGrid& grid, 
							const Coverage& dataCoverage,
							const BlockStencil& stencil=BlockStencil());
			/**
			*	@param[in,out] grid - The grid which the kernel will execute over
			*	@param[in] stencil - Stencil that defines how much border data to store in shared memory cache
			*/
			__host__ Smem(DeviceGrid& grid, const BlockStencil& stencil=BlockStencil());
			__host__ ~Smem();
			/**
			* 	@return shared memory required size in bytes (multiples of 8 bytes)
			*/
			__host__ size_t byteSize();
			/**
			*	Defines the position in X,Y,Z from where to start caching a block
			*	This sets the from() and to() attributes.
			*
			*	@param[in] x_start - x-position (column) (fast memory direction in row-major)
			*	@param[in] y_start - y-position (row) 
			*	@param[in] z_start - z-position (depth) 

			*/
			__device__ __forceinline__ void position(int x_start, 
														int y_start=0,
														int z_start=0);
			/**
			* 	This function will cache a small porttion of 'src' data into shared memory
			*	This is done asynchronously
			*	@param[in] src - source data to cache into shared memory
			*/
			template<class DeviceType>
			__device__ __forceinline__ 
			void cache_async(const DeviceType& src);
			/**
			* 	This function will cache a small porttion of 'src' data into shared memory
			*	This is done synchronously
			*	@param[in] src - source data to cache into shared memory
			*/
			template<class DeviceType>
			__device__ __forceinline__ 
			void cache(const DeviceType& src);
			__device__ __forceinline__ T& operator()(int j);
			/**
			*	@param i row index of data
			*	@param j column index of data
			*	@return reference of value at index		
			*/
			__device__ __forceinline__ T& operator()(int i, int j);
			/**
			*	@param i row index of data
			*	@param j column index of data
			*	@param k depth index of data
			*	@return reference of value at index
			*/
			__device__ __forceinline__ T& operator()(int i, int j, int k);
			/**
			*	@param j column index of data
			*	@return value at index
			*/
			__device__ __forceinline__ const T operator()(int j) const;
			/**
			*	@param i row index of data
			*	@param j column index of data
			*	@return value at index		
			*/
			__device__ __forceinline__ const T operator()(int i, int j) const;
			/**
			*	@param i row index of data
			*	@param j column index of data
			*	@param k depth index of data
			*	@return value at index
			*/
			__device__ __forceinline__ const T operator()(int i, int j, int k) const;
			/**
			*	@return start indices of block
			*/
			__device__ __forceinline__ int3 from();
			/**
			*	@return from indices of block
			*/
			__device__ __forceinline__ int3 to();
			/**
			*	@return coverage of the thread block
			*/
			__device__ __forceinline__ uchar3 blockCoverage();
			/**
			*	@return coverage of the thread block
			*/
			__device__ __forceinline__ uchar3 smemCoverage();
			/**
			*	@return block stencil data
			*/
			__device__ __forceinline__ uchar3 blockStencil();
			
		private:
			/**
			*	@param[in] coverage
			*/
			__host__ void setMapping(const Coverage& coverage);
			template<class DeviceType>
			__device__ __forceinline__ 
			void cached_edge_block(const DeviceType& src);
			/**
			*	Set up standard region
			*/
			__device__ __forceinline__ void stdRegion();

			T* m_smem_ptr;
			// Start and ending indices
			int3 m_from;
			int3 m_to;
			// Neighbooring data stencil
			uchar3 m_stencil;
			// Shared memory size
			uchar3 m_SMEM_COV;
			// Block data coverage:
			uchar3 m_COV;
			// Total shared memory size:
			size_t m_byte_offset;
			/** Indicate if region has been defined or not **/
			bool m_regionDefined;
			
	}; // END SMEM CLASS

} // END NAMESPACE
/**Buffer to hold rerefence data*/
extern __shared__ uint8_t SMEM_BUFFER[];
#include "Smem.cut"


