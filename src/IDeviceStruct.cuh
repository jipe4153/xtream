/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

namespace xpl
{



	/**
	* @TODO: document
	*/
	template<class T>
	class IDeviceStruct
	{

	public:
		//virtual ~IDeviceStruct() {}

		//
		// 
		//
		virtual __device__ void write(T value, int i, int j, int k) = 0;
		virtual __device__ T read(int i, int j, int k) const = 0;

		//
		// Read operators
		virtual __device__ const T operator()(int j) const = 0;
		virtual __device__ const T operator()(int i, int j) const = 0;
		virtual __device__ const T operator()(int i, int j, int k) const = 0;

		virtual __host__ __device__ int rows() const = 0;
		virtual __host__ __device__ int cols() const = 0;
		virtual __host__ __device__ int depth() const = 0;
		virtual __host__ __device__ int pitch() const = 0;
		virtual __host__ __device__ int depthPitch() const = 0;

	};


} // END namespace