/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "DeviceGrid.cuh"
#include "SmemLimits.cuh"

/**
* @file device_lambda.cuh
*	Contains functionality for executing device side lambdas in a kernel.
*/

/** Helper macro for compatible lambda signatures */
#define xpl_device_lambda() [=]__device__() mutable

namespace xpl
{
	namespace device
	{
		/**
		*	Executes a lambda on the GPU
		*	@param[in] lambda - lambda to execute by each thread
		*/
		template<class Lambda>
		__global__ void execute_lambda_kernel(Lambda lambda)
		{
			//
			// Execute lambda on the device
			//
			lambda();
		}
		/**
		*	Executes a generic copy-capture lambda with no input arguments
		*
		*	@param[in] grid - Describes the number of blocks and number of threads in each block
		*	@param[in] stream - stream to schedule work on
		*	@param[in] lambda - lambda to execute by each thread
		*/
		template<class Lambda>
		void execute(const DeviceGrid& grid, 
							const DeviceStream& stream,
							const Lambda lambda )
		{
			ASSERT(grid.smemSize() < SMEM_LIMITS, "Shared memory exceeds device limits");
			execute_lambda_kernel<<< grid.blocks(), grid.threads(), grid.smemSize(),  stream.get()>>>(lambda);
			CUDA_ERROR_ASSERT();
		}
		/**
		*	Executes a generic copy-capture lambda with no input arguments
		*	Will use default stream0
		*	@param[in] grid - Describes the number of blocks and number of threads in each block
		*	@param[in] lambda - lambda to execute by each thread
		*/
		template<class Lambda>
		void execute(const DeviceGrid& grid, 
							const Lambda lambda )
		{
			execute(grid, DeviceStream(), lambda);
		}
		/**
		*	Executes a generic copy-capture lambda with no input arguments
		*	Will use default stream0
		*	@param[in] size - describes the desired mesh size to create a device grid over
		*	@param[in] lambda - lambda to execute by each thread
		*/
		template<class Lambda>
		void execute(const Size& size, 
							const Lambda lambda )
		{
			DeviceGrid grid(size);
			execute(grid, DeviceStream(), lambda);
		}
		/**
		*	Executes a generic copy-capture lambda with no input arguments

		*	@param[in] lambda - lambda to execute by each thread
		*/
		template<class Lambda>
		void execute(const Lambda lambda)
		{
			execute(DeviceGrid(), DeviceStream(), lambda);
		}
	} // end namespace
} // end namespace