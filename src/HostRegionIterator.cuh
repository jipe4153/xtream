/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Region.cuh"
#include "HostMemory.cuh"

#include "DeviceRegionIterator.cuh"
#include "TextureRegionIterator.cuh"
#include "PinnedRegionIterator.cuh"

namespace xpl
{
		template<class T>
		class DeviceBuffer;
		template<class T>
		class TextureBuffer;
		template<class T>
		class HostBuffer;
		template<class T>
		class PinnedBuffer;

		template<class T>
		class DeviceRegionIterator;
		template<class T>
		class PinnedRegionIterator;
		template<class T>
		class TextureRegionIterator;

		/**	
		*	HostRegionIterator class for the host (CPU)
		*	The region iterator sets up a memory region that can be read from or written to
		*/
		template<class T>
		class HostRegionIterator
		{
		public:
			HostRegionIterator(const HostMemory<T>& host_buffer, 
								const Region& r, 
								const std::shared_ptr<DeviceStream> stream = std::make_shared<DeviceStream>());
			~HostRegionIterator();
			/**	Sets this device region memory to the values of \a rhs */
			void operator=(const DeviceBuffer<T>& rhs);
			/**	Sets this device region memory to the values of \a rhs */
			void operator=(const TextureBuffer<T>& rhs);
			/**	Sets this device region memory to the values of \a rhs */
			void operator=(const HostBuffer<T>& rhs);
			/**	Sets this device region memory to the values of \a rhs */
			void operator=(const PinnedBuffer<T>& rhs);		
			/** Takes in a \a rhs region iterator and copies from its region into this one */
			void operator=(const DeviceRegionIterator<T>& rhs) const;		
			/** Takes in a \a rhs region iterator and copies from its region into this one */
			void operator=(const PinnedRegionIterator<T>& rhs) const;		
			/** Takes in a \a rhs region iterator and copies from its region into this one */
			void operator=(const TextureRegionIterator<T>& rhs) const;		
			/** Takes in a \a rhs region iterator and copies from its region into this one */
			void operator=(const HostRegionIterator<T>& rhs) const;

			HostMemory<T>& getMemory() 	const;
			/** @return this region iterators stream (derived from source buffer)*/
			const DeviceStream& stream() const;
		private:
			std::shared_ptr< HostMemory<T> > m_host_memory;
			std::shared_ptr<DeviceStream> m_stream;
			Region m_region;
		};	
		template<class T>
		std::ostream & operator<<(std::ostream & Str, const HostRegionIterator<T>& v);
} // END namespace
#include "HostRegionIterator.cut"