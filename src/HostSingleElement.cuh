/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include "Position.cuh"
#include "DataDescriptor.cuh"
#include "ISingleElement.cuh"

namespace xpl
{
	
	/**
	*	Defines reading and writing of single elements
	*/
	template<class T>
	class HostSingleElement : public ISingleElement<T>
	{
	public:
		/**
		*	HostSingleElement is configured with a data descriptor and a position
		*	This class reads from or writes to an element at 'p'
		*/
		HostSingleElement(T* data_ptr)
		{
			m_index = 0;
			m_element_ptr = data_ptr;
		}
		~HostSingleElement()
		{}

		void setDataIndex(uint index)
		{
			m_index = index;
		}
		/**
		* 	Returns a value at a buffer position
		*/
		virtual operator T() const
		{
			return m_element_ptr[m_index];
		}
		/**
		* 	Set device to this value: a Positions indicated in constuctor
		*	@param value indicates the value to write to the device
		*	@return element object from which the value may be get and set
		*/
		virtual ISingleElement<T>& operator=(const T& value)
		{
			m_element_ptr[m_index] = value;
			return *this;
		}

	private:
		//
		// Pointer to raw data
		//
		T* m_element_ptr;
		//
		// data index location
		//
		uint m_index;


	};


}


