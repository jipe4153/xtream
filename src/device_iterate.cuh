/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file device_iterate.cuh
*	Contains functionality for executing device side iterator-lambdas.
*/
#pragma once
#include "Size.cuh"
#include "device_lambda.cuh"
#include "Index.cuh"

#define xpl_iterator(INDEX) [=]__device__(INDEX) mutable

namespace xpl
{

	namespace device
	{
		/**
		*	Executes device-size Lambda for a given 'grid' on a specific stream
		*
		*	@param[in] grid - DeviceGrid to execute lambda
		*	@param[in] stream - DeviceStream to execut on
		*	@param[in] lambda - copy capture lambda to execute per thread
		*/
		template<class Lambda> 
		void iterate(const DeviceGrid& grid, const DeviceStream& stream, Lambda lambda)
		{

			const int rows = (int)grid.size().rows();
			const int cols = (int)grid.size().cols();
			const int depth = (int)grid.size().depth();

			auto iterateKernel = xpl_device_lambda()
			{
				int i = device::getY();
				int j = device::getX();
				int k = device::getZ();

				if( i < rows && j < cols && k < depth)
				{
					auto ind = xpl::Index(i,j,k);
					lambda(ind);
				}
			};
			device::execute(grid, stream, iterateKernel);
		}
		/**
		*	Executes device-size Lambda for a given 'grid' on the default DeviceStream (0)
		*
		*	@param[in] grid - DeviceGrid to execute lambda
		*	@param[in] lambda - copy capture lambda to execute per thread
		*/
		template<class Lambda> 
		void iterate(const DeviceGrid& grid, Lambda lambda)
		{
			iterate(grid, DeviceStream(), lambda);
		}
		/**
		*	Executes device-size Lambda for a given 'grid' on the default DeviceStream (0)
		*
		*	@param[in] meshSize - Size of DeviceGrid to execute lambda
		*	@param[in] lambda - copy capture lambda to execute per thread
		*/
		template<class Lambda> 
		void iterate(const xpl::Size& meshSize, Lambda lambda)
		{
			auto grid = DeviceGrid(meshSize);
			iterate(grid, DeviceStream(), lambda);
		}
	}
}