/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <memory>
#include <ostream>
#include <sstream>
#include "PinnedBuffer.cuh"
#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "DeviceStreamBase.cuh"
#include "ISingleElement.cuh"
#include "DataDescriptor.cuh"
#include "TextureRegionIterator.cuh"
#include "TextureSingleElement.cuh"
#include "TextureMemory.cuh"
#include "TextureStruct.cuh"
#include "Position.cuh"
#include "Region.cuh"
#include "compile.cuh"
#include "TextureBuffer.cuh"
#include <CommonAssert.h>
#include "Position.cuh"
#include "TextureMemory.cuh"
#include "HostBuffer.cuh"
#include "SupportedClassTypes.cuh"
#include "device_memcpy.cuh"
#include "iterator_memcpy.cuh"
#include "cross_memcpy.cuh"
#include "Index.cuh"


namespace xpl
{
	template<class T>
	class HostBuffer;
	template<class T>
	class DeviceBuffer;
	template<class T>
	class PinnedBuffer;
	/**
	*	TextureBuffer class for managing device side textures (read/write etc)
	*\n	Textures allow for 2D & 3D spatial data caching aswell as fast hardware addressing and interpolation.
	*\n	This class fetches (reads) data from an array via textures and writes them to the same array via surfaces.
	*\n
	*\n	-> Operators exists to directly modify individual elements from both host (CPU) and device (GPU kernels)
	*\n	-> Convenience functions for "for_each" operations, region iterators, and memcpy functions
	*\n	-> Device side operators and functions in conjunction with device_lambdas (see examples)
	*\n	-> supports up to 3 dimensional data {rows, cols, deph}
	*\n	The column dimension is always the fast X-dimension (memory layout wise)
	*/
	template<class T>
	class TextureBuffer : public DeviceStreamBase 
	{
	public:
		/**
		*	Construct a TextureBuffer with @N column elements (size = 1xNx1)
		*	@param[in] N - number of elements
		*/
		TextureBuffer(uint N);
		/**
		*	Construct a TextureBuffer size = [rows]x[cols]x[depth]
		*	@param[in] N - number of elements
		*/
		TextureBuffer(uint rows, uint cols, uint depth=1);
		/**
		*	Construct a TextureBuffer 
		*	@param[in] size - size of buffer to create
		*/
		TextureBuffer(const Size& size);
		/**
		*	Creates a TextureBuffer that operats on already existing device memory @device_memory
		*	@param[in] device_memory - input pre-allocated device-memory
		*/
		TextureBuffer(std::shared_ptr<TextureMemory<T> > device_memory);
		/**
		*	Creates a TextureBuffer that operats on already existing device memory @device_memory
		*	over a given region
		*	@param[in] device_memory - input pre-allocated device-memory
		*	@param[in] data_region - data_region to reference in @device_memory
		*/
		TextureBuffer(std::shared_ptr<TextureMemory<T> > device_memory, const Region& data_region);
		/**
		*	Creates a TextureBuffer from a HostBuffer, allocates a texture buffer and copies host
		*\n	memory to it 
		*	@param[in] hostBuffer - input host buffer to read from
		*/
		TextureBuffer(const HostBuffer<T>& hostBuffer);
		~TextureBuffer();
		/**
		*	@returns a new buffer object that covers data_region
		*/
		std::shared_ptr<TextureBuffer<T> > region(const Region& data_region) const;
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const PinnedBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const DeviceBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const TextureBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const HostBuffer<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*	ex: deviceBuffer = pinnedBuffer(someRegion);
		*/		
		void operator=(const PinnedRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const DeviceRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const TextureRegionIterator<T>& other);	
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const HostRegionIterator<T>& other);	
		/**
		*	Example usage: 
		*	buffer_a(sub_region) = buffer_b(sub_region);
		*	@returns a region iterator to region r
		*/
		TextureRegionIterator<T>& operator()(const Region& r);
		
		/**
		*	@returns reference to this buffers memory
		*/
		const TextureMemory<T>& memory() const;
		/**
		*	@returns reference to this buffers memory
		*/
	 	TextureMemory<T>& memory();
		/**
		*	Addressing mode of the texture at boundaries:
		*
		*	cudaAddressModeWrap 	Wrapping address mode
		*	cudaAddressModeClamp 	Clamp to edge address mode
		*	cudaAddressModeMirror 	Mirror address mode
		*	cudaAddressModeBorder 	Border address mode
		*	@param[in] mode - addressing mode to use
		*
		*	@NOTE this will re-create the 
		*/
		void setAddressMode(const cudaTextureAddressMode& mode);
		/**
		*	Nearest point (nearest neighboor) or linear pixel filtering
		*
		*	Modes:
		*	cudaFilterModePoint 	Point filter mode
		*	cudaFilterModeLinear 	Linear filter mode
		*	
		*	@param[in] mode - texture filtering mode
		*/
		void setFilterMode(const cudaTextureFilterMode& mode);
		/**
		*	Read mode determines if value should be converted to <float> and value range normalized when a
		*	texure fetch occurs
		*	
		*	A read of 8- or 16-bit data is converted to a float in the range of [0,1.0f] for unsigned integers
		*	and [-1.0f, 1.0f] for signed integers. (32-bit integer is undocumented in CUDA PG).
		*
		*	cudaReadModeElementType 	- Read texture as specified element type (original data)
		*	cudaReadModeNormalizedFloat - Read texture as normalized float (will convert to <float>)
		*
		*	DEFAULT value is cudaReadModeElementType (no conversion)
		*	@param[in] mode - readm mode to use
		*/
		void setReadMode(const cudaTextureReadMode& mode);
		/**
		*	Normalized coords yield indexing in range of [0,1)  for each respective dimension
		*	Non-normalized coords yield indexing in the range of [0,N-1] for each respective dimension
		*
		*	DEFAULT is non-normalized
		*	@param[in] useNormalizedCoords - set to true for coordinates in [0,1] range
		*/
		void setUseNormalizedCoords(const bool useNormalizedCoords);
		/**
		*	@return texture object reference (what we read from)
		*/
		cudaTextureObject_t texture() const;
		/**
		*	@return surface object reference (what we write to)
		*/
		cudaSurfaceObject_t surface() const;
		/**
		*	@return reference to underlying array
		*/
		cudaArray* array() const;
		/**
		*	@param[in] lambda - lambda function to execute on device, funciton must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void for_each(Lambda lambda);
		/**
		*	@param[in] lambda - device function to execute, function must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void iterate(Lambda lambda);
		/**
		*	@return string representation of buffer contents
		*/
		operator std::string() const;
		/**
		*	@return size of buffer
		*/
		Size size() const;		

		#if DEVICE_COMPILE
			/** @return number of rows in buffer*/
			__device__ __forceinline__ int rows() const;
			/** @return number of cols in buffer*/
			__device__ __forceinline__ int cols() const;
			/** @return number of depth in buffer*/
			__device__ __forceinline__ int depth() const;
			/** @return the memory pitch in x-direcdtion*/
			__device__ __forceinline__ int pitch() const;
			/** @return the memory pitch in the y-direction*/
			__device__ __forceinline__ int depthPitch() const;
			/** Writes @value to index (i,j,k) */
			__device__ __forceinline__ void write(T value, int i, int j, int k);
			/** Writes @value to index (i,j) */
			__device__ __forceinline__ void write(T value, int i, int j);
			/** Writes @value to index (j) */
			__device__ __forceinline__ void write(T value, int j);
			/**@return value at (i,j,k)*/
			__device__ __forceinline__ T read(int i, int j, int k) const;
			/**	
			*	Interpolated texture memory fetch at (x) = (column)
			*	@param[in] x - x-dimension / column wise floating point index
			*	@return interpolated value at (x) 
			*/
			__device__ __forceinline__ const T operator()(float x) const;
			/**	
			*	Interpolated texture memory fetch at (y,x,z) = (row, col, depth)
			*	@param[in] y - y-dimension / row wise floating point index
			*	@param[in] x - x-dimension / column wise floating point index
			*	@param[in] z - z-dimension / depth wise floating point index
			*	@return interpolated value at (y,x,z) 
			*/
			__device__ __forceinline__ const T operator()(float y, float x, float z=0.0f) const;
			/**@return scalar at (j)*/
			__device__ __forceinline__ const T operator()(int j) const;
			/**@return scalar at (i,j,k)*/
 			__device__ __forceinline__ const T operator()(int i, int j, int k=0) const;
			/**@return scalar at (j)*/
			__device__ __forceinline__ const T operator()(uint j) const;
			/**@return scalar at (i,j,k)*/
			__device__ __forceinline__ const T operator()(uint i, uint j, uint k=0) const;
			/**	@return scalar at xpl::Index */
			__device__ __forceinline__ const T operator()(const xpl::Index& ind) const;
			/**	
			*	Returns a SurfaceTexture object at (x), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	In the case of a surface write the values are floored to nearest integer:
			*	(j) = (floor(x))
			*
			*	@param[in] x - x-dimension / column wise floating point index
			*	@return surface texture object
			*/__device__ __forceinline__ SurfaceTexture<T>& operator()(float x);
			/**	
			*	Returns a SurfaceTexture object at (y,x,z), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	In the case of a surface write the values are floored to nearest integer:
			*	(i,j,k) = (floor(y), floor(x), floor(z))
			*	
			*	@param[in] y - y-dimension / row wise floating point index
			*	@param[in] x - x-dimension / column wise floating point index
			*	@param[in] z - z-dimension / depth wise floating point index
			*	@return surface texture object
			*/
			__device__ __forceinline__ SurfaceTexture<T>& operator()(float y, float x, float z=0.0f);
			/**	
			*	Returns a SurfaceTexture object at (j), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	
			*	@param[in] j - column index
			*	@return surface texture object
			*/
			__device__ __forceinline__ SurfaceTexture<T>& operator()(int j);
			/**	
			*	Returns a SurfaceTexture object at (i,j,k), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	
			*	@param[in] i - row wise index
			*	@param[in] j - column wise index
			*	@param[in] k - depth wise index
			*	@return surface texture object
			*/
			__device__ __forceinline__ SurfaceTexture<T>& operator()(int i, int j, int k=0);
			/**	
			*	Returns a SurfaceTexture object at (j), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	
			*	@param[in] j - column index
			*	@return surface texture object
			*/
			__device__ __forceinline__ SurfaceTexture<T>& operator()(uint j);
			/**	
			*	Returns a SurfaceTexture object at (i,j,k), this object can set (surface write) or get (texture fetch)
			*	a value from the underlying array.
			*	
			*	@param[in] i - row wise index
			*	@param[in] j - column wise index
			*	@param[in] k - depth wise index
			*	@return surface texture object
			*/
			__device__ __forceinline__ SurfaceTexture<T>& operator()(uint i, uint j, uint k=0);
			/**	@return scalar reference at xpl::Index */
			__device__ __forceinline__ SurfaceTexture<T>& operator()(const xpl::Index& ind);
			
			
		#elif HOST_COMPILE
			/** @return number of rows in buffer*/
			__host__ uint rows() const;
			/** @return number of cols in buffer*/
			__host__ uint cols() const;
			/** @return number of depth in buffer*/
			__host__ uint depth() const;
			/** @return the memory pitch in x-direcdtion*/
			__host__ uint pitch() const;
			/** @return the memory pitch in the y-direction*/
			__host__ uint depthPitch() const;		
			/** Writes @value to index (i,j,k)*/
			__host__ void write(T value, int i, int j, int k=0);
			/** Writes @value to index (j)*/
			__host__ void write(T value, int j);
			/**@return scalar at (i,j,k)*/
			__host__ T read(int i, int j, int k) const;
			/**@return scalar at (j)*/
			__host__  const T operator()(uint j) const;
			/**@return scalar at (i,j,k)*/
			__host__  const T operator()(uint i, uint j, uint k = 0) const;
			/**	@return scalar at xpl::Index */
			__host__ const T operator()(const xpl::Index& ind) const;
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0) = a(1) + b(4);
			*
			*	@param j column index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint i, uint j, uint k=0);
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0,0,3) = a(1,0,7) + b(4,2,23);
			*
			*	@param i row index of single element
			*	@param j column index of single element
			*	@param k depth index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint j);
			/**	@return scalar reference at xpl::Index */
			__host__ ISingleElement<T>& operator()(const xpl::Index& ind);
		#endif
	private:
		/** Container of device side functionality*/
		TextureStruct<T> device;
		/**
		*	Setups up a device structure for usage on the GPU device
		*	The device struct defines device side reading and writing to this DeviceBuffer
		*	@return a deviceStruct instantized by this DeviceBuffer
		*/
		void initDevice(const TextureMemory<T>& memory);
		/** Reference to memory of this buffer */
		std::shared_ptr<TextureMemory<T> > m_texture_memory;
		/** Manages reading and writing to single device elements from the host */
		std::shared_ptr<TextureSingleElement<T> > m_setter;
		/** Describes the data layout, provides linear indexing functionality */
		std::shared_ptr< DataDescriptor<T>  > m_dataDescriptor;
		/** Region iterator, manages inter region iteration **/
		std::shared_ptr<TextureRegionIterator<T> > m_region_iterator;
	};
	/**
	*	Convenience function for printing buffer contents
	*	@param[in] str - output stream
	*	@param[in] v - buffer to print out
	*	@return updated output stream for printing
	*/
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const TextureBuffer<T>& v);

}
#include "TextureBuffer.cut"