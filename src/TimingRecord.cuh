/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>
#include <vector>
#include <iostream>     
#include <iomanip>      
#include <sstream>
#include <limits>
#include <algorithm>
#include "EventTimings.cuh"      
#include <CommonAssert.h>
#include "DeviceEvent.cuh"      



namespace xpl
{
	namespace utils
	{
		/**
		*   Utility for managing multiple events over multiple timers.
		*	Commonly used for timing many kernels multiple times
		*	
		*	USAGE example (psuedo code):
		*
		*		Time 3 events multiple times:
		*	
		*		TimingRecord record("record name");
		*		record.setupEventTimer("kernel1", 100); // NOTE preallocating 100 timers
		*		record.setupEventTimer("kernel2", 100);
		*		record.setupEventTimer("kernel3", 100);
		*
		*		....
		*
		*		auto timer1 = record.get("kernel1")->newTimer();
		*		timer1->start();
		*		kernel1<<<...>>>(...);
		*		timer1->stop();
		*
		*		auto timer2 = record.get("kernel2")->newTimer();
		*		timer2->start();
		*		kernel2<<<...>>>(...);
		*		timer2->stop();
		*
		*		auto timer3 = record.get("kernel3")->newTimer();
		*		timer3->start();
		*		kernel3<<<...>>>(...);
		*		timer3->stop();
		*	
		*		...
		*	
		*		// Report to user:
		*		// Get elapsed time etc:
		*		std::cout << record; 
		*
		*		Example printout:
		*		Name: record name
		*
		*		[NAME]                        [mean]              [min]               [max]               [calls]                                                                                                       
		*		kernel1                       16.932705 ms        16.000896 ms        18.234272 ms        3                                                                                                             
		*		kernel2                       89.025612 ms        83.333664 ms        94.906883 ms        3                                                                                                             
		*		kernel3                       47.391384 ms        46.863041 ms        48.428959 ms        3        
		*
		*/
		class TimingRecord
		{

		public:
			/**
			*	Constructs a timing record
			*	@param[in] name - name of record
			*/
			TimingRecord(const std::string& name="TimingRecord")
			: m_monitorName(name)
			{
			}
			~TimingRecord()
			{
				m_timingCategories.clear();
			}
			/**
			*	Setup an event with @name and preallocate @NB_TIMINGS timer objects
			*	@param[in] name - name of event
			*	@param[in] NB_TIMNGS - number of events to preallocate
			*/
			void setupEventTimer(const std::string& name, const uint NB_TIMINGS=1)
			{
				bool exists = false;
				for(auto timings : m_timingCategories)
				{
					if(timings->name() == name)
						exists = true;
				}
				
				// Did not exist... create it:
				if(exists == false)
				{
					m_timingCategories.push_back( std::make_shared<EventTimings>(name, NB_TIMINGS)  );
				}
			}
			/**
			*	Get event timings by name tag
			*/
			std::shared_ptr<EventTimings> get(const std::string& name)
			{
				for(auto timings : m_timingCategories)
				{
					if(timings->name() == name)
						return timings;
				}
				ASSERT(false, "EventTimer did not exist...");
				return NULL;
			}
			/**
			*	 @return string summary of the record (timing data)
			*/
			operator std::string() const
			{
				std::stringstream ss;
				std::vector<int> cols = {0,30,50,70,90};
				std::string lineStr = std::string(200,' ');

				auto append = [&](const std::string& str, int colIdx)
				{
					
					int l= str.length();
					lineStr.replace(cols[colIdx], l, str);

				};
				ss << "\nName: " << m_monitorName << "\n";
				ss << "\n";
				append("[NAME]" , 0);
				append("[mean]" , 1);
				append("[min]"  , 2);
				append("[max]"  , 3);
				append("[calls]", 4);
				ss << lineStr;
				ss << "\n"; 

				for(auto eventTiming : m_timingCategories)
				{
					lineStr = std::string(200,' ');
					append(eventTiming->name(), 0);
					append(std::to_string(eventTiming->meanTime()) + " ms",1);
					append(std::to_string(eventTiming->minTime()) + " ms", 2);
					append(std::to_string(eventTiming->maxTime()) + " ms", 3);
					append(std::to_string(eventTiming->iterations()), 4);
					ss << lineStr;
					ss << "\n"; 

				}
				ss << "\n";
				std::string ans = "";
				ans = ss.str();
				return ans;
			}

		private:
			std::vector<std::shared_ptr<EventTimings> > m_timingCategories;		
			const std::string m_monitorName;


		};
		/**
		*	For printing a timing record: std::cout << timingRecord;
		*	 @return string summary of the record (timing data) in an output stream
		*/
		inline
		std::ostream & operator<<(std::ostream & Str, const TimingRecord& v) 
		{ 
			// print something from v to str, e.g: Str << v.getX();
			Str << (std::string)v;
			return Str;
		}

	} // end utils
}