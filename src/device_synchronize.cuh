/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>

namespace xpl
{
	namespace device
	{
		inline
		void synchronize()
		{
			cudaDeviceSynchronize();
		}
	}
}