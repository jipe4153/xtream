/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <memory>
#include <sstream>
#include <ostream>
#include "PinnedBuffer.cuh"
#include "TextureBuffer.cuh"
#include "HostBuffer.cuh"
#include "Region.cuh"
#include "DeviceSingleElement.cuh"
#include "DataDescriptor.cuh"
#include "DeviceRegionIterator.cuh"
#include "DeviceStruct.cuh"
#include "DeviceStreamBase.cuh"
#include "DeviceMemory.cuh"
#include "compile.cuh"
#include "DeviceBuffer.cuh"
#include <CommonAssert.h>
#include "Position.cuh"
#include "DeviceMemory.cuh"
#include "HostBuffer.cuh"
#include "SupportedClassTypes.cuh"
#include "iterator_memcpy.cuh"
#include "device_memcpy.cuh"
#include "cross_memcpy.cuh"
#include "device_foreach.cuh"
#include "Index.cuh"



namespace xpl
{
	template<class T>
	class PinnedBuffer;
	template<class T>
	class TextureBuffer;
	template<class T>
	class HostBuffer;
	/**
	*	DeviceBuffer for managing device side memory
	*\n
	*\n	-> Operators exists to directly modify individual elements from both host (CPU) and device (GPU kernels)
	*\n	-> Convenience functions for "for_each" operations, region iterators, and memcpy functions
	*\n	-> Device side operators and functions in conjunction with device_lambdas (see examples)
	*\n	-> DeviceBuffer supports up to 3 dimensional data {rows, cols, deph}
	*\n	The column dimension is always the fast X-dimension (memory layout wise)
	*\n
	*/	
	template<class T>
	class DeviceBuffer : public DeviceStreamBase 
	{
	public:
		/**
		*	Construct a DeviceBuffer with @N column elements (size = 1xNx1)
		*	@param[in] N - number of elements
		*/
		DeviceBuffer(uint N);
		/**
		*	Construct a DeviceBuffer size = [rows]x[cols]x[depth]
		*	@param[in] N - number of elements
		*/
		DeviceBuffer(uint rows, uint cols, uint depth=1);
		/**
		*	Construct a DeviceBuffer 
		*	@param[in] size - size of buffer to create
		*/
		DeviceBuffer(const Size& size);
		/**
		*	Creates a DeviceBuffer that operats on already exisiting device memory @device_memory
		*	@param[in] device_memory - input pre-allocated device-memory
		*/
		DeviceBuffer(std::shared_ptr<DeviceMemory<T> > device_memory);
		/**
		*	Creates a DeviceBuffer that operats on already exisiting device memory @device_memory
		*	over a given region
		*	@param[in] device_memory - input pre-allocated device-memory
		*	@param[in] data_region - data_region to reference in @device_memory
		*/
		DeviceBuffer(std::shared_ptr<DeviceMemory<T> > device_memory, const Region& data_region);
		/**
		*	Creates a DeviceBuffer from a HostBuffer, allocates a device buffer and copies host
		*\n	memory to it 
		*	@param[in] hostBuffer - input host buffer to read from
		*/
		DeviceBuffer(const HostBuffer<T>& hostBuffer);
		/** destroy object */
		~DeviceBuffer();
		/**
		*	@param[in] region - a region to iterate over
		*	@return a region iterator for the buffer
		*/
		DeviceRegionIterator<T>& operator()(const Region& region);
		/**
		*	@param[in] data_region - Data region specifier
		*	@return a buffer referencing the data specified by @data_region
		*/		
		std::shared_ptr<DeviceBuffer<T> > region(const Region& data_region) const;
		/**
		*	@param[in] lambda - device function to execute, funciton must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void for_each(Lambda lambda);
		/**
		*	@param[in] lambda - device function to execute, function must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void iterate(Lambda lambda);
		
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const PinnedBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const DeviceBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const TextureBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const HostBuffer<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*	ex: deviceBuffer = pinnedBuffer(someRegion);
		*/		
		void operator=(const PinnedRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const DeviceRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const TextureRegionIterator<T>& other);	
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const HostRegionIterator<T>& other);	
		/**
		*	@return reference to this buffers memory
		*/
		const DeviceMemory<T>& memory() const;
		/**
		*	@return reference to this buffers memory
		*/
	 	DeviceMemory<T>& memory();		
		/**
		*	@return string representation of the data
		*/
		operator std::string() const;
		/**
		*	@return size of the buffer
		*/
		Size size() const;

		#if DEVICE_COMPILE
			/** @return number of rows in buffer*/
			__device__ __forceinline__ int rows() const;
			/** @return number of cols in buffer*/
			__device__ __forceinline__ int cols() const;
			/** @return number of depth in buffer*/
			__device__ __forceinline__ int depth() const;
			/** @return the memory pitch in x-direcdtion*/
			__device__ __forceinline__ int pitch() const;
			/** @return the memory pitch in the y-direction*/
			__device__ __forceinline__ int depthPitch() const;
			/** @return GPU device pointer */
			__device__ __forceinline__ T* data() const;
			/** 
			*	Writes @value to index (i,j,k)
			*/
			__device__ __forceinline__ void write(T value, int i, int j, int k);
			/**
			*	@return value at (i,j,k)
			*/
			__device__ __forceinline__ T read(int i, int j, int k) const;
			/**
			*	@return reference to element at (j)
			*/
			__device__ __forceinline__ T& operator()(int j);
			/**
			*	@return reference to element at (i,j)
			*/
			__device__ __forceinline__ T& operator()(int i, int j);		
			/**
			*	@return reference to element at (i,j,k)
			*/
			__device__ __forceinline__ T& operator()(int i, int j, int k);
			/**	@return scalar reference at xpl::Index */
			__device__ __forceinline__ T& operator()(const xpl::Index& ind);
			/**
			*	@return scalar at (j)
			*/
			__device__ __forceinline__ const T operator()(int j) const;
			/**
			*	@return scalar at (i,j)
			*/
			__device__ __forceinline__ const T operator()(int i, int j) const;
			/**
			*	@return scalar at (i,j,k)
			*/
			__device__ __forceinline__ const T operator()(int i, int j, int k) const;			
			/**	@return scalar at xpl::Index */
			__device__ __forceinline__ const T operator()(const xpl::Index& ind) const;
		#elif HOST_COMPILE
			/** @return number of rows in buffer*/
			__host__ uint rows() const;
			/** @return number of cols in buffer*/
			__host__ uint cols() const;
			/** @return number of depth in buffer*/
			__host__ uint depth() const;
			/** @return the memory pitch in x-direcdtion*/
			__host__ uint pitch() const;
			/** @return the memory pitch in the y-direction*/
			__host__ uint depthPitch() const;		
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint j) const;
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint i, uint j) const;
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint i, uint j, uint k) const;
			/**	@return scalar at xpl::Index */
			__host__ const T operator()(const xpl::Index& ind) const;
			/** @return GPU device pointer */
			__host__ T* data() const;
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0) = a(1) + b(4);
			*
			*	@param j column index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint j);
			/*
			*	Allows for element-wise data WRITE access 
			*	a(0,0) = a(1,0) + b(4,2);
			*
			*	@param i row index of single element
			*	@param j column index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint i, uint j);
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0,0,3) = a(1,0,7) + b(4,2,23);
			*
			*	@param i row index of single element
			*	@param j column index of single element
			*	@param k depth index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint i, uint j, uint k);
			/**	@return scalar reference at xpl::Index */
			__host__ ISingleElement<T>& operator()(const xpl::Index& ind);
		#endif

		/**
		*	Maps a DeviceBuffer to @data_ptr input data
		*\n	this takes care of the @pitch in x-direction and in the y-direction @depthPitch
		* 	param[in] data_ptr 	- pointer to contiguos memory region
		* 	param[in] size 		- size in rows x cols x depth
		* 	param[in] pitch 	- memory pitch in x-direction in number of <T> elements
		* 	param[in] depthPitch- memory pitch in y-direction in number of <T> elements
		*/
		static DeviceBuffer<T> map(T* data_ptr, const Size& size, uint pitch=0, uint depthPitch=0);
	private:
		/**
		*	Setups up a device structure for usage on the GPU device
		*	The device struct defines device side reading and writing to this DeviceBuffer
		*	@param[in] memory - a de vice memory descriptor
		*/
		void initDevice(const DeviceMemory<T>& memory);
		/** Data container available for processing operations on the device*/
		DeviceStruct<T> m_device;
		/** Device memory pointer */
		std::shared_ptr<DeviceMemory<T> > m_device_memory;
		/** Describes data region of this buffer*/
		Region m_region;
		/** Manages reading and writing to single device elements from the host */
		std::shared_ptr< DeviceSingleElement<T> > m_setter;
		/** Describes the data layout, provides linear indexing functionality*/
		//std::shared_ptr< DataDescriptor<T>  > m_dataDescriptor;
		/** Region iterator, manages inter region iteration **/
		std::shared_ptr<DeviceRegionIterator<T> > m_region_iterator;
	};
	/**
	*	Convenience function for printing buffer contents
	*	@param[in] str - output stream
	*	@param[in] v - buffer to print out
	*	@return updated output stream for printing
	*/
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const DeviceBuffer<T>& v);

}
#include "DeviceBuffer.cut"
