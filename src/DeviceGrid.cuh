/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Coverage.cuh"
#include "Size.cuh"
#include "DeviceMapping.cuh"

namespace xpl
{
	/**
	*	Takes mesh-size information and a thread to data mapping class to elude how many blocks
	*\n	and how many threads in each block is needed to fullfill this mapping to this mesh-size
	*\n
	*\n	Produces blocks in a grid and threads in a block information
	*/
	class DeviceGrid
	{
	public:
		/**
		*	@param[in] meshSize - size in rows X cols X depths to map blocks to
		*	@param[in] mapping - Thread block to data coverage mapping
		*/
		DeviceGrid(const Size& meshSize, const DeviceMapping& mapping)
		: m_mapping(mapping), m_totSmemSize(0), m_size(meshSize)
		{
			// Grid configuration
			m_blocks = mapping.getGridDim(meshSize);
		}
		/**
		*	@param[in] meshSize - size in rows X cols X depths to map blocks to
		*  	NOTE: uses default thread to data mapping
		*/
		DeviceGrid(const Size& meshSize=Size(1,1,1))
		: DeviceGrid(meshSize, DeviceMapping(meshSize) )
		{
		}
		/**
		*	@param[in] blocks - describes NB blocks in X,Y, and Z (a grid)
		*	@param[in] threads - thread block dimensions in X,Y,Z
		*  	NOTE: uses default thread to data mapping
		*/
		DeviceGrid(dim3 blocks, dim3 threads)
		: m_blocks(blocks), m_mapping(threads.x,threads.y, threads.z), m_totSmemSize(0), 
			m_size(blocks.y*threads.y, blocks.x*threads.x, blocks.z*threads.z)
		{
		}
		/** @return block data  */
		dim3 blocks() const
		{
			return m_blocks;
		}
		/** @return thread block dimensions */
		dim3 threads() const
		{
			return m_mapping.blockDim();
		}
		/** @return DeviceGrid info: grid, block, and shared memory usage info string */
		operator std::string()
		{

			std::string str = "";
			str += "\n blockDim.x = " + std::to_string(threads().x);
			str += "\n blockDim.y = " + std::to_string(threads().y);
			str += "\n blockDim.z = " + std::to_string(threads().z);
			str += "\n gridDim.x = " + std::to_string(blocks().x);
			str += "\n gridDim.y = " + std::to_string(blocks().y);
			str += "\n gridDim.z = " + std::to_string(blocks().z);
			str += "\n SMEM byte size = " + std::to_string(smemSize());

			return str;
		}
		/** @return DeviceMapping */
		DeviceMapping deviceMapping() const
		{
			return m_mapping;
		}

		Size size() const
		{
			return m_size;
		}
		/** @return shared memory size usage per block by this grid */
		size_t smemSize() const
		{
			return m_totSmemSize;
		}
		/** 
		*	Registers shared memory in bytes to be potentially allocated by the grid upon kernel launch
		*	@param[in] smemSize - size of additinal shared memory to dynamically allocate
		*	@return previous shared memory usage size  
		*/
		size_t registerSmemSize(size_t smemSize)
		{
			size_t prev = m_totSmemSize;
			m_totSmemSize+=smemSize;
			return prev;
		}
	private:
		dim3 m_blocks;
		DeviceMapping m_mapping;		
		size_t m_totSmemSize;
		const Size m_size;
	};

} // END namespace