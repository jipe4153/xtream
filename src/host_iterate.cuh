/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Size.cuh"

namespace xpl
{
	namespace host
	{
		/**
		*	Host lambda iterator over index
		*	@param[in] meshSize - size to iterated over, lambda will be called with each index in 'meshSize'
		*	@param[in] lambda - host function to execute, function must take in xpl::Index
		*/
		template<class Lambda> 
		void iterate(const xpl::Size& meshSize, Lambda lambda);
	}
}
#include "host_iterate.cut"