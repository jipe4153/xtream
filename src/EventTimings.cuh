/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include <vector>
#include <string>
#include <memory>
#include "Timer.cuh"      
#include <cuda.h>
#include <CommonAssert.h>
#include <limits>
#include <algorithm>
#include <vector>
#include <iostream>     
#include <iomanip>      

namespace xpl
{
	namespace utils
	{
		/**
		*	Utility for timing an event multiple times asynchronously
		*
		*	The Event is given a name tag and a number of Timers are assigned to each event.
		*
		*	Usage:
		*	EventTimings myKernelTimer("myKernel");
		*	....
		*	FOR_EACH_CALL_TO_KERNEL
		*	{
		*		auto timer = myKernelTimer.newTimer();
		*		timer->start();
		*		myKernel<<<....>>>(...);
		*		timer->stop();
		*	}
		*	float meanTime = myKernelTimer.meanTimer();
		*	
		*/
		class EventTimings
		{

		public:
			/**
			*	@param[in] eventName - name of the event
			*	@param[in] NB_TIMINGS - number of timings to preallocate (calling newTimer() will allocate if needed)
			*/
			EventTimings(const std::string& eventName, const uint NB_TIMINGS=1)
			: m_eventName(eventName), m_timingIter(0)
			{
				for(uint i = 0; i < NB_TIMINGS; i++)
					m_timingList.push_back(std::make_shared<Timer> () );
			}
			 ~EventTimings()
			{
				m_timingList.clear();
			}
			/**
			*   @return reference to an unused timer
			*/
			std::shared_ptr<Timer> newTimer()
			{

				ASSERT(m_timingIter <= m_timingList.size(), "Invalid number of timings");

				// If need to create more timers:
				if(m_timingIter==m_timingList.size())
				{
					m_timingList.push_back(std::make_shared<Timer>());
				}

				std::shared_ptr<Timer> timer = m_timingList[m_timingIter];
				m_timingIter++;

				return timer;
			}
			/*
			*	@return name of event
			*/
			std::string name() const
			{
				return m_eventName;
			}
			/**
			*   @return reference to the latest timer
			*/
			std::shared_ptr<Timer> lastTimer()
			{
				return m_timingList.back();
			}
			/*
			*	@return max time measured
			*/
			float maxTime() const
			{
				float max = std::numeric_limits<float>::lowest();
				for(auto timer : m_timingList)
				{
					if(timer->finished() )
					{
						float t = timer->elapsedMS();
						max = t > max ? t : max;
					}
				}
				return max;
			}
			/*
			*	@return min time measured
			*/
			float minTime() const
			{
				float min = std::numeric_limits<float>::max();
				for(auto timer : m_timingList)
				{
					if(timer->finished() )
					{
						float t = timer->elapsedMS();
						min = t < min ? t : min;
					}
				}
				return min;
			}
			/*
			*	@return mean time of all measured calls
			*/
			float meanTime() const
			{
				float sum  = 0.0f;
				uint validTimers = 0;
				for(auto timer : m_timingList)
				{
					if(timer->finished() )
					{
						float t = timer->elapsedMS();
						sum += t;
						validTimers++;
					}
				}
				if( validTimers == 0)
					return 0.0f;

				return sum / float(validTimers);
			}

			/*
			*	@return median time
			*/
			float medianTime() const
			{
				// Copy into intermediate buffer:
				std::vector<float> v;
				for(auto timer : m_timingList)
				{
					if(timer->finished() )
					{
						v.push_back(timer->elapsedMS());
					}
				}
				size_t n = v.size() / 2;
				std::nth_element(v.begin(), v.begin()+n, v.end());
				float median = v[n];
				return median;			
			}
			/*
			*	@return number of timers used
			*/
			uint iterations() const
			{
				return m_timingIter;
			}

		private:
			std::vector<std::shared_ptr<Timer>> m_timingList;
			std::string m_eventName;
			uint m_timingIter;
		};
	} // namespace utils
} // namespace xtream