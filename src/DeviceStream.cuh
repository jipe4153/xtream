/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>
#include <CommonAssert.h>
#include "DeviceEvent.cuh"
#include <device_synchronize.cuh>


namespace xpl
{
	class DeviceEvent;

	/** Type of stream, default stream0 or other concurrent stream */
	enum StreamType	{	default_stream,	concurrent_stream };
	class DeviceStream
	{
	public:
		/*
		*	The device stream defaults to the stream 0 which is not concurrent
		*
		*	@param[in] stype - type of stream: default/concurrent
		*	@param[in] name - name of the stream (TBI)
		*/
		DeviceStream(const StreamType& stype = default_stream, const std::string& name="")
		: m_name(name), m_stream(0)
		{
			if( stype != default_stream )
			{
				cudaStreamCreate(&m_stream);
				CUDA_ERROR_ASSERT();
			}
		}
		/**
		*	@param[in] stream - existing stream to use
		*	@param[in] name - name of the stream (TBI)
		*/
		DeviceStream(const cudaStream_t stream, const std::string& name="")
		: m_stream(stream),  m_name(name)
		{
		}
		~DeviceStream()
		{
			//
			// Default stream zero cannot be destroyed
			//
			if( m_stream  )
				cudaStreamDestroy(m_stream);
		}
		/*
		*	Makes all future work submitted to stream wait until event reports 
		*	completion before beginning execution. This synchronization will be 
		*	performed efficiently on the device.The event eventmay be from a 
		*	different context than stream in which case this function will perform cross-device synchronization.
		*/
		void waitForEvent(const DeviceEvent& event) const
		{
			cudaStreamWaitEvent(m_stream, event.get(), 0);
			CUDA_ERROR_ASSERT();
		}
		/*
		*	Synchronize the stream
		*/
		void synchronize() const
		{
			cudaStreamSynchronize(m_stream);
			CUDA_ERROR_ASSERT();
		}
		/**
		*	@return cuda stream reference
		*/
		cudaStream_t get() const
		{
			return m_stream;
		}
	private:
		cudaStream_t m_stream;
		std::string m_name;
	};
} // namespace xpl