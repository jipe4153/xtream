/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "host_memcpy.cuh"
#include "device_foreach.cuh"
#include "host_iterate.cuh"

namespace xpl
{
	template<class T>
	HostBuffer<T>::HostBuffer(const Size& size)
	:	 
	m_host_memory(std::make_shared<HostMemory<T> >(size)),
	m_rows(m_host_memory->rows()),
	m_cols(m_host_memory->cols()),
	m_depth(m_host_memory->depth()),
	m_pitch(m_host_memory->pitch()),
	m_depthPitch(m_host_memory->depthPitch())
	{
		m_single_element_setter = std::make_shared<HostSingleElement<T> >( m_host_memory->data() );
	}
	template<class T>
	HostBuffer<T>::HostBuffer(std::shared_ptr<HostMemory<T> > host_memory)
	: 
	m_host_memory(host_memory),
	m_rows(m_host_memory->rows()),
	m_cols(m_host_memory->cols()),
	m_depth(m_host_memory->depth()),
	m_pitch(m_host_memory->pitch()),
	m_depthPitch(m_host_memory->depthPitch())
	{
		m_single_element_setter = std::make_shared<HostSingleElement<T> >( m_host_memory->data() );
	}
	template<class T>
	HostBuffer<T>::HostBuffer(uint rows, uint cols, uint depth)
	: HostBuffer<T>(Size(rows, cols, depth))
	{

	}
	template<class T>
	HostBuffer<T>::HostBuffer(uint N)
	: HostBuffer<T>(Size(1,N,1))
	{
	}
	template<class T>
	HostBuffer<T>::HostBuffer(const DeviceBuffer<T>& deviceBuffer)
	: HostBuffer<T>(deviceBuffer.size())
	{
		// Copy memory contents:
		*this = deviceBuffer;
	}
	template<class T>
	HostBuffer<T>::HostBuffer(const TextureBuffer<T>& texBuffer)
	: HostBuffer<T>(texBuffer.size())
	{
		// Copy memory contents:
		*this = texBuffer;
	}
	template<class T>
	HostBuffer<T>::HostBuffer(const PinnedBuffer<T>& pinnedBuffer)
	: HostBuffer<T>(pinnedBuffer.size())
	{
		// Copy memory contents:
		*this = pinnedBuffer;
	}
	template<class T>
	HostBuffer<T>::HostBuffer(std::shared_ptr<HostMemory<T> > host_memory, const Region& data_region)
	: HostBuffer<T>(host_memory->referenceRegion(data_region))
	{
		//
		// Make a reference to a sub-region of this memory
		//
	}
	template<class T>
	HostBuffer<T>::~HostBuffer()
	{
	}
	template<class T>
	std::shared_ptr<HostBuffer<T>> HostBuffer<T>::create(T* data_ptr, const Size& size, uint pitch, uint depthPitch)
	{
		pitch = pitch==0 ? size.cols() : pitch;
		depthPitch = depthPitch==0 ? size.rows() : depthPitch;
		auto hostMem = std::make_shared<HostMemory<T>>(data_ptr, size, pitch, depthPitch);
		return std::make_shared<HostBuffer<T>>(hostMem);
	}
	template<class T>
	std::shared_ptr<HostBuffer<T>> HostBuffer<T>::create(std::vector<T>& input)
	{
		Size size(1,input.size());
		return create(input.data(), size);
	}
	template<class T>
	HostBuffer<T> HostBuffer<T>::map(T* data_ptr, const Size& size, uint pitch, uint depthPitch)
	{
		pitch = pitch==0 ? size.cols() : pitch;
		depthPitch = depthPitch==0 ? size.rows() : depthPitch;

		auto hostMem = std::make_shared<HostMemory<T>>(data_ptr, size, pitch, depthPitch);
		return HostBuffer<T>(hostMem);
	}
	template<class T>
	HostBuffer<T> HostBuffer<T>::map(std::vector<T>& input)
	{
		Size size(1,input.size());
		return map(input.data(), size);
	}
	template<class T>
	void HostBuffer<T>::operator=(const HostBuffer<T>& other)
	{
		host::memcpy(*this, other);
	}
	template<class T>
	void HostBuffer<T>::operator=(const PinnedBuffer<T>& other)
	{
		cross::memcpy( *this, other); // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const DeviceBuffer<T>& other)
	{
		cross::memcpy( *this, other);  // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const TextureBuffer<T>& other)
	{
		cross::memcpy( *this, other);  // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const HostRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const PinnedRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const DeviceRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void HostBuffer<T>::operator=(const TextureRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	
	template<class T>
	HostRegionIterator<T>& HostBuffer<T>::operator()(const Region& r)
	{
		m_region_iterator.reset(new HostRegionIterator<T>(*m_host_memory, r, this->getStream() ));
		return *m_region_iterator;
	}
	template<class T>
	std::shared_ptr<HostBuffer<T> > HostBuffer<T>::region(const Region& r) const
	{
		// 
		// Create new DeviceBuffer which references this memory:
		std::shared_ptr<HostBuffer<T> > buffer_Region = std::make_shared<HostBuffer<T> > (m_host_memory, r);
		return buffer_Region;
	}
	template<class T>
	const HostMemory<T>& HostBuffer<T>::memory() const
	{
		return *m_host_memory;
	}
	template<class T>
	HostMemory<T>& HostBuffer<T>::memory()
	{
		return *m_host_memory;
	}
	
	template<class T>
	HostBuffer<T>::operator std::string() const
	{
		std::stringstream ss;
		ss << "Martix dimensions: " << rows() << " x " << cols() << " x " << depth();
		// Iterate
		for(uint k = 0; k < depth(); k++)
		{
			ss << std::endl << "Matrix segment: " << k << std::endl;
			for(uint i = 0; i < rows(); i++)
			{
				ss << std::endl;
				for(uint j = 0; j < cols(); j++)
				{
					
					float value = this->operator()(i,j,k);
					ss << std::setw(3) << value << " ";
				}
			}
		}

		std::string ans = "";
		ans = ss.str();
		return ans;
	}

	template<class T>
	uint HostBuffer<T>::rows() const
	{
		return m_rows;
	}
	template<class T>
	uint HostBuffer<T>::cols() const
	{
		return m_cols;
	}
	template<class T>
	uint HostBuffer<T>::depth() const
	{
		return m_depth;
	}
	template<class T>
	uint HostBuffer<T>::pitch() const
	{
		return m_pitch;
	}
	template<class T>
	uint HostBuffer<T>::depthPitch() const
	{
		return m_depthPitch;
	}
	template<class T>
	Size HostBuffer<T>::size() const
	{
		return memory().size();
	}
	template<class T>
	const T HostBuffer<T>::operator()(uint j) const
	{
		return this->operator()(0,j,0);
	}
	template<class T>
	const T HostBuffer<T>::operator()(uint i, uint j) const
	{
		return this->operator()(i,j,0);
	}
	template<class T>
	const T HostBuffer<T>::operator()(uint i, uint j, uint k) const
	{
		ASSERT(i < m_rows, "row index out of bounds");
		ASSERT(j < m_cols, "col index out of bounds");
		ASSERT(k < m_depth, "depth index out of bounds");
		uint index = j + i*m_pitch + k*m_depthPitch*m_pitch;
		T* ptr = memory().data();
		return ptr[index];
	}
	template<class T>
	T& HostBuffer<T>::operator()(uint j)
	{
		return this->operator()(0,j,0);
	}
	template<class T>
	T& HostBuffer<T>::operator()(uint i, uint j)
	{
		return this->operator()(i,j,0);
	}
	template<class T>
	T& HostBuffer<T>::operator()(uint i, uint j, uint k)
	{
		ASSERT(i < m_rows, "row index out of bounds");
		ASSERT(j < m_cols, "col index out of bounds");
		ASSERT(k < m_depth, "depth index out of bounds");

		uint index = j + i*m_pitch + k*m_depthPitch*m_pitch;
		T* ptr = memory().data();
		return ptr[index];
	}
	template<class T>
	T& HostBuffer<T>::operator()(const xpl::Index& ind)
	{
		return (*this)(ind.y(), ind.x(), ind.z());
	}
	template<class T>
	const T HostBuffer<T>::operator()(const xpl::Index& ind) const
	{
		return (*this)(ind.y(), ind.x(), ind.z());
	}
	template<class T>
	template<class Lambda> void HostBuffer<T>::iterate(Lambda lambda)
	{
		host::iterate(size(), lambda);
	}
	template<class T>
	template<class Lambda>
	void HostBuffer<T>::for_each(Lambda lambda)
	{

		T* ptr = memory().data();
		for(uint k = 0; k < this->depth(); k++)
		{
			for(uint i = 0; i < this->rows(); i++)
			{
				for(uint j = 0; j < this->cols(); j++)
				{
					uint index = j + i*pitch() + k*depthPitch()*pitch();
					T val = ptr[index];
					lambda(val);
					ptr[index] = val;
				}
			}
		}
	}
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const HostBuffer<T>& v) 
	{ 
		// print something from v to str, e.g: Str << v.getX();
		Str << (std::string)v;
		return Str;
	}
} // END xpl