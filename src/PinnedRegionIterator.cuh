/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/

#pragma once
#include "Region.cuh"
#include "PinnedMemory.cuh"
#include "DeviceStream.cuh"

#include "DeviceRegionIterator.cuh"
#include "TextureRegionIterator.cuh"
#include "HostRegionIterator.cuh"

namespace xpl
{

	template<class T>
	class HostRegionIterator;
	template<class T>
	class DeviceRegionIterator;
	template<class T>
	class TextureRegionIterator;
	/**	
	*	PinnedRegionIterator class for the device (GPU)
	*	The region iterator sets up a memory region that can be read from or written to
	*/
	template<class T>
	class PinnedRegionIterator
	{

	public:
		PinnedRegionIterator(	PinnedMemory<T>& pinned_memory, 
								std::shared_ptr<const DeviceStream > stream, 
								const Region& r);
		~PinnedRegionIterator();
		/**	Sets this device region memory to the values of \a rhs */
		void operator=(const DeviceBuffer<T>& rhs);
		/**	Sets this device region memory to the values of \a rhs */
		void operator=(const TextureBuffer<T>& rhs);
		/**	Sets this device region memory to the values of \a rhs */
		void operator=(const HostBuffer<T>& rhs);
		/**	Sets this device region memory to the values of \a rhs */
		void operator=(const PinnedBuffer<T>& rhs);
		/** Takes in a \a rhs region iterator and copies from its region into this one */
		void operator=(const DeviceRegionIterator<T>& rhs) const;
		/** Takes in a \a rhs region iterator and copies from its region into this one */
		void operator=(const PinnedRegionIterator<T>& rhs) const;
		/** Takes in a \a rhs region iterator and copies from its region into this one */
		void operator=(const TextureRegionIterator<T>& rhs) const;
		/** Takes in a \a rhs region iterator and copies from its region into this one */
		void operator=(const HostRegionIterator<T>& rhs) const;

		PinnedMemory<T>& getMemory() 	const;
		const DeviceStream& stream() const;
	private:
		std::shared_ptr< PinnedMemory<T> > m_pinned_memory;
		std::shared_ptr<const DeviceStream> m_stream;
	}; // END class
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const PinnedRegionIterator<T>& v);
} // END namespace xpl
#include "PinnedRegionIterator.cut"