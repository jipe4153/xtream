/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Size.cuh"
#include "Position.cuh"

namespace xpl
{
	class Region
	{
	public:
		Region(const Position& position, const Size& size)
		: m_position(position), m_size(size)
		{
		}
		Region()
		: m_position(0,0,0), m_size(0,0,0)
		{

		}
		/** 
		*	Sets position and size of region based on indices in x (refers to columns)
		*	and indices in y (referes to rows)
		*/
		Region(uint y_from, uint y_to, uint x_from, uint x_to)
		: m_position(x_from,y_from,0), m_size(y_to-y_from+1, x_to-x_from+1, 1)
		{

		}
		/** 
		*	Sets position and size of region based on indices in x (refers to columns)
		*	and indices in y refers to rows,
		*	and indices in z refers to depth
		*/
		Region(uint y_from, uint y_to, uint x_from, uint x_to, uint z_from, uint z_to)
		: m_position(x_from,y_from, z_from ), m_size(y_to-y_from+1, x_to-x_from+1, z_to-z_from+1)
		{

		}
		Position position() const
		{
			return m_position;
		}
		Size size() const
		{
			return m_size;
		}
		bool isInside(const Position& p) const
		{
			uint x_high = (m_position.x() + m_size.cols())-1;
			uint x_low = m_position.x();

			uint y_high = (m_position.y() + m_size.rows())-1;
			uint y_low = m_position.y();

			uint z_high = (m_position.z() + m_size.depth())-1;
			uint z_low = m_position.z();

			bool ok = 
					p.x() <= x_high && p.x() >= x_low &&
					p.y() <= y_high && p.y() >= y_low &&
					p.z() <= z_high && p.z() >= z_low;

			return ok;
		}
		static bool SameSize(const Region& ra, const Region& rb)
		{
			if( ra.size() == rb.size() )
				return true;
			else
				return false;
		}
	private:
		Position m_position;
		Size m_size;

	};
} // END xpl