/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/


#pragma once
#include <memory>
#include "Position.cuh"
#include "Region.cuh"
#include "Position.cuh"
#include "HostMemory.cuh"
#include "Size.cuh"
#include <ostream>
#include "HostSingleElement.cuh"
#include "PinnedBuffer.cuh"
#include "DeviceBuffer.cuh"
#include "TextureBuffer.cuh"
#include "HostBuffer.cuh"
#include "HostRegionIterator.cuh"
#include "HostSingleElement.cuh"
#include "HostMemory.cuh"
#include <memory>
#include <vector>
#include <iostream>     
#include <iomanip>      
#include "cross_memcpy.cuh"
#include "iterator_memcpy.cuh"
#include "HostBuffer.cuh"
#include "Index.cuh"


namespace xpl
{
	template<class T>
	class TextureBuffer;
	template<class T>
	class DeviceBuffer;
	template<class T>
	class PinnedBuffer;
	/**
	*	HostBuffer for managing host side (CPU) memory
	*\n
	*\n	This buffer is a convenience memory container and mangement on the host side,
	*\n	it is not intended to provide high performance processing features on the CPU.
	*\n
	*\n	That being said, the HostBuffer class is convenient for transferring data to-and from
	*\n	different GPU oriented buffer types, hence a typical used case would be to map existing
	*\n	CPU data to a HostBuffer class and let it alleviate memory transfers.
	*\n
	*\n	Basic featuress:
	*\n	-> Operators exists to directly modify individual elements from host (CPU)
	*\n	-> Convenience functions for "for_each" operations, region iterators, and memcpy functions
	*\n	-> Device side operators and functions in conjunction with device_lambdas (see examples)
	*\n	-> DeviceBuffer supports up to 3 dimensional data {rows, cols, deph}
	*\n	The column dimension is always the fast X-dimension (memory layout wise)
	*/		
	template<class T>
	class HostBuffer : public DeviceStreamBase 
	{
	public:
		/**
		*	Construct a HostBuffer with @N column elements (size = 1xNx1)
		*	@param[in] N - number of elements
		*/
		HostBuffer(uint N);
		/**
		*	Construct a HostBuffer size = [rows]x[cols]x[depth]
		*	@param[in] N - number of elements
		*/
		HostBuffer(uint rows, uint cols, uint depth=1);
		/**
		*	Construct a HostBuffer size = [rows]x[cols]x[depth]
		*	@param[in] N - number of elements
		*/
		HostBuffer(const Size& size);
		/**
		*	Creates a HostBuffer that operates on already exisiting device memory @device_memory
		*\n	over a given region
		*	@param[in] device_memory - input pre-allocated device-memory
		*	@param[in] data_region - data_region to reference in @device_memory
		*/
		HostBuffer(std::shared_ptr<HostMemory<T> > host_memory);
		/**
		*	Creates a HostBuffer that operates on already exisiting device memory @device_memory
		*\n	over a given region
		*	@param[in] device_memory - input pre-allocated device-memory
		*	@param[in] data_region - data_region to reference in @device_memory
		*/
		HostBuffer(std::shared_ptr<HostMemory<T> > host_memory, const Region& data_region);
		/**
		*	Creates a HostBuffer from a device buffer, allocates a host buffer and copies device
		*\n	memory to it (note more efficient to pre allocate host buffer and do: h_buff = d_buff)
		*	@param[in] deviceBuffer - input device buffer to read from
		*/
		HostBuffer(const DeviceBuffer<T>& deviceBuffer);
		/**
		*	Creates a HostBuffer from a texture buffer, allocates a host buffer and copies device
		*\n	memory to it (note more efficient to pre allocate host buffer and do: h_buff = d_buff)
		*	@param[in] deviceBuffer - input device buffer to read from
		*/
		HostBuffer(const TextureBuffer<T>& texBuffer);
		/**
		*	Creates a HostBuffer from a pinned buffer, allocates a host buffer and copies pinned
		*\n	memory to it (note more efficient to pre allocate host buffer and do: h_buff = d_buff)
		*	@param[in] pinnedBuffer - input device buffer to read from
		*/
		HostBuffer(const PinnedBuffer<T>& pinnedBuffer);
		~HostBuffer();
		/**
		*	Example usage: 
		*	buffer_a(sub_region) = buffer_b(sub_region);
		*	@returns a region iterator to region r
		*/
		HostRegionIterator<T>& operator()(const Region& r);
		/**
		*	@param[in] data_region - Data region specifier
		*	@return a buffer referencing the data specified by @data_region
		*/		
		std::shared_ptr<HostBuffer<T> > region(const Region& data_region) const;
		/**
		*	@param[in] lambda - host function to execute, function must input value (Ex: [=](T& val))
		*/
		template<class Lambda> void for_each(Lambda lambda);
		/**	@return reference to scalar at xpl::Index */
		T& operator()(const xpl::Index& ind);
		/**	@return scalar at xpl::Index */
		const T operator()(const xpl::Index& ind) const;
		/**
		*	@param[in] lambda - host function to execute, function must take in xpl::Index
		*/
		template<class Lambda> void iterate(Lambda lambda);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const PinnedBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const DeviceBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const TextureBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const HostBuffer<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*	ex: deviceBuffer = pinnedBuffer(someRegion);
		*/		
		void operator=(const PinnedRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const DeviceRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const TextureRegionIterator<T>& other);	
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const HostRegionIterator<T>& other);	
		/** @return reference to this buffers memory */
		const HostMemory<T>& memory() const;
		/** @return reference to this buffers memory */
		HostMemory<T>& memory();
		/** @return string representation of the data */
		operator std::string() const;
		/** @return number of rows in buffer*/
		uint rows() const;
		/** @return number of cols in buffer*/
		uint cols() const;
		/** @return number of depth in buffer*/
		uint depth() const;
		/** @return the memory pitch in x-direcdtion*/
		uint pitch() const;
		/** @return the memory pitch in the y-direction*/
		uint depthPitch() const;		
		/** @return size of the buffer */		
		Size size() const;
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0) = a(1) + b(4);
		*
		*	@param j column index of single element
		*	@return Single element modifier object
		*/
		T& operator()(uint j);
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0,0) = a(1,0) + b(4,2);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@return Single element modifier object
		*/
		T& operator()(uint i, uint j);
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0,0,3) = a(1,0,7) + b(4,2,23);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@param k depth index of single element
		*	@return Single element modifier object
		*/
		T& operator()(uint i, uint j, uint k);
		/**	@return scalar at (j) */
		const T operator()(uint j) const;
		/**	@return scalar at (i,j) */
		const T operator()(uint i, uint j) const;
		/**	@return scalar at (i,j,k) */
		const T operator()(uint i, uint j, uint k) const;
		/**
		*	Creates a host buffer with memory mapped to @data_ptr
		*	@param[in] data_ptr - data ptr to create mapped host buffer from
		*	@param[in] size - size of buffer
		*	@param[in] pitch - memory pich in X-dimension if anya
		*	@param[in] depthPitch - memory pich in Y-dimension if any
		*	@return pointer to new HostBuffer with mapped memory
		*/
		static std::shared_ptr<HostBuffer<T>> create(T* data_ptr, const Size& size, uint pitch=0, uint depthPitch=0);
		/**
		*	Creates a host buffer with memory mapped to @input vectors data
		*	@param[in] input - input vector to map HostBuffer to
		*	@return pointer to new HostBuffer with mapped memory
		*/
		static std::shared_ptr<HostBuffer<T>> create(std::vector<T>& input);
		/**
		*	Maps a HostBuffer to @data_ptr input data
		*	@param[in] data_ptr - data ptr to map HostBuffer to
		*	@param[in] size - size of buffer
		*	@param[in] pitch - memory pich in X-dimension if anya
		*	@param[in] depthPitch - memory pich in Y-dimension if any
		*	@return HostBuffer object
		*/
		static HostBuffer<T> map(T* data_ptr, const Size& size, uint pitch=0, uint depthPitch=0);
		/**
		*	Maps a host buffer to an input vector
		*	@param[in] input - input vector to map HostBuffer to
		*	@return HostBuffer object
		*/
		static HostBuffer<T> map(std::vector<T>& input);
	private:
		//
		// Reference to memory of this buffer
		//
		std::shared_ptr<HostMemory<T> > m_host_memory;
		//
		// Single element-wise ops
		//
		std::shared_ptr<HostSingleElement<T> > m_single_element_setter;
		// Iterator used for copying data between regions
		std::shared_ptr<HostRegionIterator<T> > m_region_iterator;
		// row elements
		const uint m_rows;
		// column elements
		const uint m_cols;
		// depth elements in Z-dim
		const uint m_depth;
		// X-dim memory pitch elements
		const uint m_pitch;
		// Y-dim depth pitch
		const uint m_depthPitch;
	};

	template<class T>
	std::ostream & operator<<(std::ostream & Str, const HostBuffer<T>& v);

} // END xpl
#include "HostBuffer.cut"
