/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <CommonAssert.h>
#include "cuda.h"

namespace xpl
{

	template<class T>
	void memcpyTextureToHost(HostMemory<T>& dst,const TextureMemory<T>& src, cudaStream_t stream)
	{
		ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
		//
		// If it's a 3D copy, assert that all rows are included in the depth pitch
		ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");
		ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");
		cudaMemcpy2DFromArrayAsync(	dst.data(), 
									dst.pitch()*sizeof(T), 
									src.array(), 
									src.xOffset(), 
									src.yOffset(),
									src.cols()*sizeof(T),
									src.rows()*src.depth(),
									cudaMemcpyDeviceToHost,
									stream);
		CUDA_ERROR_ASSERT();
	}
	template<class T>
	void memcpyHostToTexture(TextureMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
	{
		ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
		//
		// If it's a 3D copy, assert that all rows are included in the depth pitch
		ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");
		ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");

		cudaMemcpy2DToArrayAsync(	dst.array(), 
									dst.xOffset()*sizeof(T), 
									dst.yOffset(),
									src.data(), 
									src.pitch()*sizeof(T),
									src.cols()*sizeof(T), 
									src.rows()*src.depth(),
									cudaMemcpyHostToDevice,
									stream);
		CUDA_ERROR_ASSERT();
	}
}  // end namespace