/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "HostMemory.cuh"

namespace xpl
{
	namespace host
	{
		template<class T>
		void memcpy(HostBuffer<T>& dst, const HostBuffer<T>& src)
		{
			ASSERT(dst.size() == src.size(), "Cannot memcpy between buffers of different size");

			// @TODO change this to C std::memcpy if possible
			for(uint k = 0; k < dst.depth(); k++)
				for(uint i = 0; i < dst.rows(); i++)
					for(uint j = 0; j < dst.cols(); j++)
					{
						dst(i,j,k) = src(i,j,k);
					}

		}

		template<class T>
		void memcpy(HostMemory<T>& dst, const HostMemory<T>& src)
		{

			ASSERT(dst.size() == src.size(), "Cannot memcpy between buffers of different size");
			// @TODO change this to C std::memcpy if possible
			for(uint k = 0; k < dst.depth(); k++)
				for(uint i = 0; i < dst.rows(); i++)
					for(uint j = 0; j < dst.cols(); j++)
					{
						uint dst_index = j + i*dst.pitch() + k * dst.pitch() * dst.depthPitch();
						dst.data()[dst_index] = src.getValueAt(i,j,k);
					}

		}
	} // END host namespace
} // end xpl namespace
