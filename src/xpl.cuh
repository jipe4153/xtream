/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file xpl.cuh
*/
#pragma once

#include "DeviceBuffer.cuh"
#include "HostBuffer.cuh"
#include "TextureBuffer.cuh"
#include "PinnedBuffer.cuh"
#include "Smem.cuh"
#include "device_lambda.cuh"