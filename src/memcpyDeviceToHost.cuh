/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <CommonAssert.h>
#include "cuda.h"

namespace xpl
{

		/**
		*	memcpy from device buffer to host buffer
		*/
		template<class T>
		void memcpyDeviceToHost(HostMemory<T>& dst, const DeviceMemory<T>& src, cudaStream_t stream)
		{
			ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
			//
			// If it's a 3D copy, assert that all rows are included in the depth pitch
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() );
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() );
			
			cudaMemcpy2DAsync	(
			 					dst.data(),				// dst ptr
			 					dst.pitch()*sizeof(T),	// dst pitch in bytes
			 					src.data(),				// src ptr
			 					src.pitch()*sizeof(T),	// src pich in bytes
								src.cols()*sizeof(T), 	// width in bytes
								src.rows()*src.depth(),				// height / rows
								cudaMemcpyDeviceToHost,
								stream
								);
			 CUDA_ERROR_ASSERT();
		}
		template<class T>
		void memcpyDeviceToHost(HostMemory<T>& dst, const PinnedMemory<T>& src, cudaStream_t stream)
		{
			ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
			//
			// If it's a 3D copy, assert that all rows are included in the depth pitch
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() );
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() );

			cudaMemcpy2DAsync	(
			 					dst.data(),				// dst ptr
			 					dst.pitch()*sizeof(T),	// dst pitch in bytes
			 					src.data(),				// src ptr
			 					src.pitch()*sizeof(T),	// src pich in bytes
								src.cols()*sizeof(T), 	// width in bytes
								src.rows()*src.depth(),				// height / rows
								cudaMemcpyDeviceToHost,
								stream
								);
			 CUDA_ERROR_ASSERT();
		}
		/*
		*	Copy data from host buffer to device buffer
		*/
		template<class T>
		void memcpyHostToDevice(DeviceMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
		{
			ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
			//
			// If it's a 3D copy, assert that all rows are included in the depth pitch
			bool b1 = !(src.depth() > 1);
			bool b2 = ((src.depth() > 1) && (src.rows() == src.depthPitch()) );

			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() 
				<< "\n" << b1 << "  - "<< b2 );
			
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), 
				"3D copies of this dimension not supported (cuda API lacking) " 
				<< "\n src size = " 
				<< src.size() << " deptPitch = " << src.depthPitch()
				<< "\n dst size = " 
				<< dst.size() << " deptPitch = " << dst.depthPitch() );
			
			cudaMemcpy2DAsync	(
 					dst.data(),					// dst ptr
 					dst.pitch()*sizeof(T),		// dst pitch in bytes
 					src.data(),					// src ptr
 					src.pitch()*sizeof(T),		// src pich in bytes
					src.cols()*sizeof(T), 		// width in bytes
					src.rows()*src.depth(),		// height / rows
					cudaMemcpyHostToDevice,
					stream
					);
			CUDA_ERROR_ASSERT();

		}
		template<class T>
		void memcpyHostToDevice(PinnedMemory<T>& dst, const HostMemory<T>& src, cudaStream_t stream)
		{
			ASSERT(dst.size() == src.size(), "Dimensions missmatch, cannot memcpy");
			//
			// If it's a 3D copy, assert that all rows are included in the depth pitch
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (src.rows() == src.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");
			ASSERT( !(src.depth() > 1) || ((src.depth() > 1) && (dst.rows() == dst.depthPitch()) ), "3D copies of this dimension not supported (cuda API lacking)");

			cudaMemcpy2DAsync	(
 					dst.data(),					// dst ptr
 					dst.pitch()*sizeof(T),		// dst pitch in bytes
 					src.data(),					// src ptr
 					src.pitch()*sizeof(T),		// src pich in bytes
					src.cols()*sizeof(T), 		// width in bytes
					src.rows()*src.depth(),		// height / rows
					cudaMemcpyHostToDevice,
					stream
					);
			CUDA_ERROR_ASSERT();

		}


}