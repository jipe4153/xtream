/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
/**
*	Shared memory limitations by compute capability
*	https://docs.nvidia.com/cuda/pdf/CUDA_C_Programming_Guide.pdf
*	PG-02829-001_v9.2 
*	Table 14 Technical Specifications per Compute Capability
*/

#ifdef __CUDA_ARCH__
	#if __CUDA_ARCH__ >= 700
	   #define SMEM_LIMITS 96*1024*1024
	#elif __CUDA_ARCH__ >= 600
	   #define SMEM_LIMITS 48*1024*1024
	#elif __CUDA_ARCH__ >= 500
	   #define SMEM_LIMITS 48*1024*1024
	#elif __CUDA_ARCH__ >= 300
	   #define SMEM_LIMITS 48*1024*1024
	#elif __CUDA_ARCH__ >= 200
	#define SMEM_LIMITS 32*1024*1024
	#elif !defined(__CUDA_ARCH__) 
	   #error Unsupported GPU arch (CUDA ARCH not defined)
	#endif 

#endif

#ifndef SMEM_LIMITS
#define SMEM_LIMITS 48*1024*1024
#endif


