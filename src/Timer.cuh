/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>
#include <CommonAssert.h>
#include <limits>
#include <algorithm>
#include <vector>
#include <iostream>     
#include <iomanip>      
#include "DeviceEvent.cuh"      
#include "DeviceStream.cuh"

namespace xpl
{
	namespace utils
	{
		/**
		*	Utilities class for performing GPU timing measurement
		*	Call start() and stop() on a given stream to get accurate timing between the 2.
		*	Calls default to the default stream 0.
		*
		*	usage:
		*	
		*	Timer timer;
		*	timer.start();
		*	kernel<<<...>>>(...);
		*	timer.stop();
		*	float time_millisecs = timer.elapsedMS();
		*
		*/
		class Timer
		{
		public:
			/** Construct a timer */
			Timer()
			: m_timerStopped(false)
			{
			}
			~Timer()
			{}
			/** 
			*	Starts timing by recording a DeviceEvent 
			*	@param[in] stream - stream to record event on (defaults to stream 0)
			*/
			void start(const DeviceStream& stream=DeviceStream())
			{ 
				m_startEvent.record(stream.get());
			}
			/** 
			*	Stops timing by recording a DeviceEvent 
			*	@param[in] stream - stream to record event on (defaults to stream 0)
			*/
			void stop(const DeviceStream& stream=DeviceStream())
			{
				m_stopEvent.record(stream.get());
				m_timerStopped = true;
			}
			/** @return true if timer has been stopped */
			bool finished() const
			{
				return m_timerStopped;
			}
			/** @return elapsed GPU time in milliseconds between startp & stop */
			float elapsedMS() const
			{
				cudaEventSynchronize(m_stopEvent.get());
				return Timer::elapsedMS(m_startEvent, m_stopEvent);
			}
			/** 
			*	@param[in] startEvent - starting event
			*	@param[in] stopEvent - stop event
			*	@return elapsed GPU time in milliseconds between startEvent and stopEvent 
			*/
			static float elapsedMS(const DeviceEvent& startEvent, const DeviceEvent& stopEvent)
			{
				float milliseconds = 0;
				cudaEventElapsedTime(&milliseconds, startEvent.get(), stopEvent.get());
				return milliseconds;
			}
		private:
			/** Start event object **/
			DeviceEvent m_startEvent;
			/** Stop event object **/
			DeviceEvent m_stopEvent;
			/** Tells if stop event has been recorded **/
			bool m_timerStopped;
		}; // END CLASS

	} // END UTILS
} // END XPL