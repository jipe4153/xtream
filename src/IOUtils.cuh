/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>
#include <vector>
#include <iostream>     
#include <iomanip>      
#include <sstream>
#include <limits>
#include <algorithm>
#include <HostBuffer.cuh>
#include <readRaw.h>
#include <writeRaw.h>
#include <iostream>
#include <cstdint>
#include <cstring>
namespace xpl
{
	namespace utils
	{
		template<class T>
		HostBuffer<T> readRaw(const std::string& fileName)
		{
			auto rawBuff = util::readRaw<T>(fileName);
			ASSERT(std::min(rawBuff.numBuffers,rawBuff.depth)==1, "Only supports reading 3D buffers");

			uint depth = std::max(rawBuff.numBuffers,rawBuff.depth);
			// Create host buffer of same size:
			xpl::Size sz(rawBuff.rows, rawBuff.cols, depth);
			uint N = sz.getNbElements();
			HostBuffer<T> h_buff(sz);
			fflush(stdout);
			ASSERT(N > 0, "");
			// copy to h_buff
			std::memcpy(h_buff.memory().data(), rawBuff.data_ptr.get(), N*sizeof(T) );
			return h_buff;
		}
		template<class T>
		void writeRaw(const HostBuffer<T>& buff, const std::string& fileName)
		{
			util::writeRaw(buff.memory().data(),
					 1,
					 buff.rows(),
					 buff.cols(),
					 buff.depth(),
					 fileName);
		}
	} // end utils
} // end xpl