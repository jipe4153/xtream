/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "IBuffer.cuh"
// (*_*)
//#include "DeviceBuffer.cuh"
#include "DeviceMemory.cuh"
#include "TextureMemory.cuh"
#include "kernel_memcpy.cuh"
#include "TextureStruct.cuh"
#include "DeviceStruct.cuh"
#include <memory>

namespace xpl
{
	/**
	*	Copies the data from src to dst
	*	the stream used will be that of the src (default is stream0)
	*/
	namespace device
	{
		
		// Memcopies to a device
		template<class T>
		void memcpy(DeviceBuffer<T>& dst, const DeviceBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(DeviceBuffer<T>& dst, const PinnedBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(DeviceBuffer<T>& dst, const TextureBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		//
		// Define all copies to a pinned buffer
		//
		template<class T>
		void memcpy(PinnedBuffer<T>& dst, const PinnedBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(PinnedBuffer<T>& dst, const DeviceBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(PinnedBuffer<T>& dst, const TextureBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		//
		// To texture
		template<class T>
		void memcpy(TextureBuffer<T>& dst, const PinnedBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(TextureBuffer<T>& dst, const DeviceBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		template<class T>
		void memcpy(TextureBuffer<T>& dst, const TextureBuffer<T>& src)
		{
			kernel::memcpy_call(dst, src, src.stream().get());
		}
		//
		// Memory to memory copies
		//
		template<class T>
		void memcpy(DeviceMemory<T>& dst, const DeviceMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), DeviceStruct<T>(src), stream);			
		}
		template<class T>
		void memcpy(DeviceMemory<T>& dst, const PinnedMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), DeviceStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(DeviceMemory<T>& dst, const TextureMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), TextureStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(PinnedMemory<T>& dst, const DeviceMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), DeviceStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(PinnedMemory<T>& dst, const PinnedMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), DeviceStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(PinnedMemory<T>& dst, const TextureMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(DeviceStruct<T>(dst), TextureStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(TextureMemory<T>& dst, const DeviceMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(TextureStruct<T>(dst), DeviceStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(TextureMemory<T>& dst, const PinnedMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(TextureStruct<T>(dst), DeviceStruct<T>(src), stream);
		}
		template<class T>
		void memcpy(TextureMemory<T>& dst, const TextureMemory<T>& src, const cudaStream_t stream = 0)
		{
			kernel::memcpy_call(TextureStruct<T>(dst), TextureStruct<T>(src), stream);
		}
		

	}
}



