/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "device_foreach.cuh"
#include "device_iterate.cuh"
namespace xpl
{

	template<class T>
	TextureBuffer<T>::TextureBuffer(const Size& size)
	:	m_texture_memory( std::make_shared<TextureMemory<T> >(size) )
	{
		initDevice(*m_texture_memory);
		m_setter = std::make_shared<TextureSingleElement<T> >( array(), getStream(), depthPitch() );
	}
	template<class T>
	TextureBuffer<T>::TextureBuffer(std::shared_ptr<TextureMemory<T> > texture_memory) 
	: m_texture_memory(texture_memory)
	{
		initDevice(*m_texture_memory);
		m_setter = std::make_shared<TextureSingleElement<T> >( array(), getStream(), depthPitch() );
	}
	template<class T>
	TextureBuffer<T>::TextureBuffer(std::shared_ptr<TextureMemory<T> > device_memory, const Region& data_region)
	: TextureBuffer<T>( device_memory->referenceRegion(data_region) )  
	{
	}
	template<class T>
	TextureBuffer<T>::TextureBuffer(uint rows, uint cols, uint depth)
	: TextureBuffer(Size(rows, cols, depth))
	{
	}
	template<class T>
	TextureBuffer<T>::TextureBuffer(uint N)
	: TextureBuffer(Size(1, N, 1))
	{
	}
	template<class T>
	TextureBuffer<T>::TextureBuffer(const HostBuffer<T>& hostBuffer)
	: TextureBuffer<T>(hostBuffer.size())
	{
		*this = hostBuffer;
	}
	template<class T>
	TextureBuffer<T>::~TextureBuffer()
	{
	}	
	template<class T>
	void TextureBuffer<T>::operator=(const PinnedBuffer<T>& other)
	{
		device::memcpy( *this, other); // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const DeviceBuffer<T>& other)
	{
		device::memcpy( *this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const TextureBuffer<T>& other)
	{
		device::memcpy( *this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const HostBuffer<T>& other)
	{
		cross::memcpy( *this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const PinnedRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const DeviceRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const TextureRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	void TextureBuffer<T>::operator=(const HostRegionIterator<T>& other)
	{
		iterator::memcpy(*this, other);  // copy to this
	}
	template<class T>
	std::shared_ptr<TextureBuffer<T> > TextureBuffer<T>::region(const Region& data_region) const
	{
		// 
		// Create new TextureBuffer which references this memory:
		std::shared_ptr<TextureBuffer<T> > buffer_Region = std::make_shared<TextureBuffer<T> > (m_texture_memory, data_region);
		return buffer_Region;
	}
	template<class T>
	TextureRegionIterator<T>& TextureBuffer<T>::operator()(const Region& r)
	{
		m_region_iterator.reset(new TextureRegionIterator<T>(*m_texture_memory, m_stream, r) );
		return *m_region_iterator;
	}
	template<class T>
	void TextureBuffer<T>::setAddressMode(const cudaTextureAddressMode& mode)
	{
		m_texture_memory->setAddressMode(mode);
	}
	template<class T>
	void TextureBuffer<T>::setFilterMode(const cudaTextureFilterMode& mode)
	{
		m_texture_memory->setFilterMode(mode);
	}
	template<class T>
	void TextureBuffer<T>::setReadMode(const cudaTextureReadMode& mode)
	{
		m_texture_memory->setReadMode(mode);
	}
	template<class T>
	void TextureBuffer<T>::setUseNormalizedCoords(const bool useNormalizedCoords) 
	{
		m_texture_memory->setUseNormalizedCoords(useNormalizedCoords);
	}
	template<class T>
	cudaTextureObject_t TextureBuffer<T>::texture() const 
	{ 
		return m_texture_memory->texture(); 
	}
	template<class T>
	cudaSurfaceObject_t TextureBuffer<T>::surface() const	
	{ 
		return m_texture_memory->surface(); 
	}
	template<class T>
	cudaArray* TextureBuffer<T>::array() const 
	{ 
		return m_texture_memory->array(); 
	}	
	template<class T>
	const TextureMemory<T>& TextureBuffer<T>::memory() const
	{
		return *m_texture_memory;		
	}
	template<class T>
	TextureMemory<T>& TextureBuffer<T>::memory() 
	{
		return *m_texture_memory;		
	}
	template<class T>
	Size TextureBuffer<T>::size() const
	{
		return memory().size();
	}
	template<class T>
	TextureBuffer<T>::operator std::string() const
	{
		HostBuffer<T> host_copy(this->size());
		host_copy = *this;
		this->stream().synchronize();

		return (std::string)host_copy;
	}

	#if DEVICE_COMPILE
		template<class T>
		__device__ __forceinline__ void TextureBuffer<T>::write(T value, int i, int j, int k)
		{
			device.write(value,i,j,k);
		}
		template<class T>
		__device__ __forceinline__ void TextureBuffer<T>::write(T value, int i, int j)
		{
			device.write(value,i,j);
		}
		template<class T>
		__device__ __forceinline__ void TextureBuffer<T>::write(T value, int j)
		{
			device.write(value,j);
		}
		template<class T>
		__device__ __forceinline__ T TextureBuffer<T>::read(int i, int j, int k) const
		{
			return device.read(i,j,k);
		}
		template<class T>
		__device__ __forceinline__ int TextureBuffer<T>::rows() const
		{
			return device.rows();
		}
		template<class T>
		__device__ __forceinline__ int TextureBuffer<T>::cols() const
		{
			return device.cols();
		}
		template<class T>
		__device__ __forceinline__ int TextureBuffer<T>::depth() const
		{
			return device.depth();
		}
		template<class T>
		__device__ __forceinline__ int TextureBuffer<T>::pitch() const
		{
			return device.pitch();
		}
		template<class T>
		__device__ __forceinline__ int TextureBuffer<T>::depthPitch() const
		{
			return device.depthPitch();
		}
		
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(float x) const
		{
			return device( 0.0f, x, 0.0f);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(float y, float x, float z) const
		{
			return device( y, x, z);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(int j) const
		{
			return device( 0, j, 0);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(int i, int j, int k) const
		{
			return device( i, j, k);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(uint j) const
		{
			return device( 0, (int)j, 0);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(uint i, uint j, uint k) const
		{
			return device( (int)i, (int)j, (int)k);
		}
		template<class T>
		__device__ __forceinline__ const T TextureBuffer<T>::operator()(const xpl::Index& ind) const
		{
			return device( (int)indx.y(), (int)ind.x(), (int)ind.z());
		}
		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(float x)
		{
			return device(0.0f,x,0.0f);
		}

		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(float y, float x, float z)
		{
			return device(y,x,z);
		}

		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(int j)
		{
			return device( 0.0f, (float)j, 0.0f);
		}
		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(int i, int j, int k)
		{
			return device( (float)i, (float)j, (float)k);
		}
		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(uint j)
		{
			return device( 0.0f, (float)j, 0.0f);
		}
		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(uint i, uint j, uint k)
		{
			return device( (float)i, (float)j, (float)k);
		}
		template<class T>
		__device__ __forceinline__ SurfaceTexture<T>& TextureBuffer<T>::operator()(const xpl::Index& ind)
		{
			return device((float)ind.y(), (float)ind.x(), (float)ind.z());
		}
	#elif HOST_COMPILE
		template<class T>
		uint TextureBuffer<T>::rows() const 
		{ 
			return m_texture_memory->size().rows();
		}
		template<class T>
		uint TextureBuffer<T>::cols() const 
		{ 
			return m_texture_memory->size().cols();
		}
		template<class T>
		uint TextureBuffer<T>::depth() const 
		{ 
			return m_texture_memory->size().depth();
		}
		template<class T>
		uint TextureBuffer<T>::pitch() const 
		{ 
			return m_texture_memory->pitch();
		}
		template<class T>
		uint TextureBuffer<T>::depthPitch() const 
		{ 
			return m_texture_memory->depthPitch();
		}
		template<class T>
		__host__ void TextureBuffer<T>::write(T value, int i, int j, int k)
		{
			 ISingleElement<T>& se = this->operator()(i,j,k);
			 se = value;
		}
		template<class T>
		__host__ void TextureBuffer<T>::write(T value, int j)
		{
			write(value,0,j,0);
		}
		template<class T>
		__host__  const T TextureBuffer<T>::operator()(uint j) const
		{
			return m_texture_memory->getValueAt(0,j,0);
		}
		template<class T>
		__host__  const T TextureBuffer<T>::operator()(uint i, uint j, uint k) const
		{
			return m_texture_memory->getValueAt(i,j,k);
		}
		template<class T>
		__host__ ISingleElement<T>& TextureBuffer<T>::operator()(uint i, uint j, uint k)
		{
			m_setter->setPosition(i,j,k);
			return *(m_setter);
		}
		template<class T>
		__host__ ISingleElement<T>& TextureBuffer<T>::operator()(uint j)
		{
			m_setter->setPosition(0,j,0);
			return *(m_setter);
		}
		template<class T>
		__host__ ISingleElement<T>& TextureBuffer<T>::operator()(const xpl::Index& ind)
		{
			return (*this)((uint)ind.y(), (uint)ind.x(), (uint)ind.z());
		}
		template<class T>
		__host__ const T TextureBuffer<T>::operator()(const xpl::Index& ind) const
		{
			return m_texture_memory->getValueAt(ind.y(), ind.x(), ind.z());
		}
	#endif
	
	template<class T>
	template<class Lambda> 
	void TextureBuffer<T>::iterate(Lambda lambda)
	{
		auto grid = DeviceGrid(size());
		device::iterate(grid, stream(), lambda);
	}
	template<class T>
	void TextureBuffer<T>::initDevice(const TextureMemory<T>& memory)
	{
		device = TextureStruct<T>(memory);
	}
	template<class T>
	template<class Lambda>
	void TextureBuffer<T>::for_each(Lambda lambda)
	{
		device::for_each( *this, lambda);
	}
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const TextureBuffer<T>& v) 
	{ 
		// print something from v to str, e.g: Str << v.getX();
		Str << (std::string)v;
		return Str;
	}
} // END XPL