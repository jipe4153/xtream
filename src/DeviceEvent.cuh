/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <cuda.h>
#include <CommonAssert.h>

namespace xpl
{
	/**
	*	@TODO move to separate file
	*/
	class DeviceEvent
	{
	public:
		DeviceEvent(const std::string name="")
		: m_name(name)
		{
			cudaEventCreate(&m_event);
			CUDA_ERROR_ASSERT();
		}
		~DeviceEvent()
		{
			cudaEventDestroy(m_event);
			CUDA_ERROR_ASSERT();
		}
		void record(const cudaStream_t& stream) const
		{
			cudaEventRecord (get(), stream); 			
			CUDA_ERROR_ASSERT();
		}
		cudaEvent_t get() const
		{
			return m_event;
		}

	private:
		cudaEvent_t m_event;
		std::string m_name;

	};

}