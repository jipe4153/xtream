/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "TextureMemory.cuh"
#include "IDeviceStruct.cuh"
#include "SurfaceTexture.cuh"

namespace xpl
{
	template<class T>
	class TextureStruct
	{
	public:
		/**
		*	This class contains device side functionality for reading from and writing to texture memory
		*	@þaram[in] memory - texture memory to setup the texture struct from
		*/
		TextureStruct(const TextureMemory<T>& memory);
		TextureStruct();
		/**
		*	@param[in] y - float row index to read or write from/to
		*	@param[in] x - float col index to read or write from/to
		*	@param[in] z - float depth index to read or write from/to
		*	@return a surface texture object which a scalar can read from or write to
		*/
		__device__ __forceinline__ SurfaceTexture<T>& operator()(float y, float x, float z=0.0f);
		/**
		*	@param[in] x - float col index to read or write from/to
		*	@return a surface texture object which a scalar can read from or write to
		*/
		__device__ __forceinline__ SurfaceTexture<T>& operator()(float x);
		/**
		*	Device-Operator to read data element at index
		*	@param j column index of data
		*	@return value at index
		*/
		__device__ __forceinline__ const T operator()(int j) const;
		/**
		*	Device-Operator to read data element at index
		*	@param i row index of data
		*	@param j column index of data
		*	@param k depth index of data
		*	@return value at index
		*/
		__device__ __forceinline__ const T operator()(int i, int j, int k=0) const;
		/**
		*	Device-Operator to read data element at index
		*	@param x floating point column index of data
		*	@return value at index
		*/
		__device__ __forceinline__ const T operator()(float x) const;
		/**
		*	Device-Operator to read data element at index
		*	@param y floating point row index of data
		*	@param x floating point column index of data
		*	@param z floating point depth index of data
		*	@return value at index		
		*/
		__device__ __forceinline__ const T operator()(float y, float x, float z=0.0f) const;		
		/** 
		*	Writes to an array via a surface object
		*	@param[in] value - will be written to a surface at (i,j,k)
		*	@param[in] i - row index to write to
		*	@param[in] j - column index to write to
		*	@param[in] k - depth index to write to
		*/
		__device__ __forceinline__ void write(T value, int i, int j, int k=0);
		/** 
		*	Writes to an array via a surface object
		*	@param[in] value - will be written to a surface at (j)
		*	@param[in] j - column index to write to
		*/
		__device__ __forceinline__ void write(T value, int j);
		/** 
		*	Reads from an array via a texture object
		*	@param[in] i - row index to fetch from
		*	@param[in] j - col index to fetch from
		*	@param[in] k - depth index to fetch from
		*	@return texture fetched value from array at (x,y,z)
		*/
		__device__ __forceinline__ T read(int i, int j, int k) const;
		/** 		
		*	Reads from an array via a texture object
		*	@param[in] y - float row index to fetch from
		*	@param[in] x - float col index to fetch from
		*	@param[in] z - float depth index to fetch from
		*	@return texture fetched value from array at (y,x,z)
		*/
		__device__ __forceinline__ T read(float y, float x, float z=0.0f) const;
		/** 		
		*	Reads from an array via a texture object
		*	@param[in] x - float col index to fetch from
		*	@return texture fetched value from array at (x)
		*/
		__device__ __forceinline__ T read(float x) const;
		/** @return the number of rows in the buffer*/
		__host__ __device__ __forceinline__ int rows() const;
		/** @return the number of columns in the buffer*/
		__host__ __device__ __forceinline__ int cols() const;
		/** @return the depth or Z-dimension of the buffer*/
		__host__ __device__ __forceinline__ int depth() const;
		/** @return the memory pitch in the x-dimension*/
		__host__ __device__ __forceinline__ int pitch() const;
		/** @return the memory pitch in the y-dimension*/
		__host__ __device__ __forceinline__ int depthPitch() const;
	private:
		/**
		*	Private helper function the setup the SurfaceTexture IO object
		*	@param[in] y - float row index to read or write from/to
		*	@param[in] x - float col index to read or write from/to
		*	@param[in] z - float depth index to read or write from/to
		*	@return a surface texture object which a scalar can read from or write to
		*/
		__device__ __forceinline__ SurfaceTexture<T>&  getTextureIO(float y, float x, float z);
		/** Manages read/writing  from/to and texture or surface object*/
		SurfaceTexture<T> m_surfaceTexture;
	};
} // END xpl
#include "TextureStruct.cut"
