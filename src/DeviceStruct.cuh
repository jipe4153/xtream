/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "DeviceMemory.cuh"
#include "PinnedMemory.cuh"

namespace xpl
{

	template<class T>
	class DeviceMemory;

	template<class T>
	class PinnedMemory;

	template<class T>
	class DeviceStruct 
	{
	public:
		DeviceStruct(const DeviceMemory<T>& device_memory);
		DeviceStruct(const PinnedMemory<T>& pinned_memory);
		DeviceStruct();

		__device__ __forceinline__ void write(T value, int i, int j, int k);
		__device__ __forceinline__ T read(int i, int j, int k) const;
		/**
		*	Device-Operator to reference data element at index
		*	@param j index of data
		*	@return reference of value at index
		*/
		__device__ __forceinline__ T& operator()(int j);
		/**
		*	Device-Operator to reference data element at index
		*	@param i row index of data
		*	@param j column index of data
		*	@return reference of value at index		
		*/
		__device__ __forceinline__ T& operator()(int i, int j);
		/**
		*	Device-Operator to reference data element at index
		*	@param i row index of data
		*	@param j column index of data
		*	@param k depth index of data
		*	@return reference of value at index
		*/
		__device__ __forceinline__ T& operator()(int i, int j, int k);
		/**
		*	Device-Operator to read data element at index
		*	@param j column index of data
		*	@return value at index
		*/
		__device__ __forceinline__ const T operator()(int j) const;
				/**
		*	Device-Operator to read data element at index
		*	@param i row index of data
		*	@param j column index of data
		*	@return value at index		
		*/
		__device__ __forceinline__ const T operator()(int i, int j) const;
		/**
		*	Device-Operator to read data element at index
		*	@param i row index of data
		*	@param j column index of data
		*	@param k depth index of data
		*	@return value at index
		*/
		__device__ __forceinline__ const T operator()(int i, int j, int k) const;
		//__device__ __forceinline__ void write(T val, int i, int j, int k) const;
		__host__ __device__ __forceinline__ int rows() const;
		__host__ __device__ __forceinline__ int cols() const;
		__host__ __device__ __forceinline__ int depth() const;
		__host__ __device__ __forceinline__ int pitch() const;
		__host__ __device__ __forceinline__ int depthPitch() const;
		__host__ __device__ __forceinline__ T* data() const;

	private:

		T* m_data;
		int m_rows;
		int m_cols;
		int m_depth;
		int m_pitch;
		int m_depthPitch;
	};
}

#include "DeviceStruct.cut"
