/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include <stdint.h>
namespace xpl
{

	/**
	*	Define a block wide area of accessible data cells in X,Y, and Z directions
	*	around the data that a block covers
	*
	*	Examples:
	*	BlockStencil(0,0) - signifies identity (covers same data as block)
	*	BlockStencil(1,1) - signifies size+2 in X&Y dimension,
	*		Each thread will get access to (at least) surrounding 3x3 area
	*	BlockStencil(2,2) - signifies size+4 in X&Y dimension,
	*		Each thread will get access to (at least) surrounding 5x5 area
	*	BlockStencil(1,0) - signifies size+2 in X dimension
	*			Each thread gains acccess to values in surrounding 1x3 area
	*/
	class BlockStencil
	{
	public:
		/**
		*	@param[in] _x - number accessible data cells in X-dimension in both directions [j-x, j, j+x]
		*	@param[in] _y - number accessible data cells in Y-dimension in both directions [i-y, i, i+y]
		*	@param[in] _z - number accessible data cells in Z-dimension in both directions [k-z, k, k+z]
		*/
		BlockStencil(uint8_t _x, uint8_t _y, uint8_t _z)
		: x(_x), y(_y), z(_z)
		{}
		/**
		*	@param[in] _x - number accessible data cells in X-dimension in both directions [j-x, j, j+x]
		*	@param[in] _y - number accessible data cells in Y-dimension in both directions [i-y, i, i+y]
		*/
		BlockStencil(uint8_t _x, uint8_t _y)
		: BlockStencil(_x,_y,0)
		{}
		/**
		*	@param[in] _x - number accessible data cells in X-dimension in both directions [j-x, j, j+x]
		*/
		BlockStencil(uint8_t _x)
		: BlockStencil(_x,0,0)
		{}
		BlockStencil()
		: BlockStencil(0,0,0)
		{}
		uint8_t x;
		uint8_t y;
		uint8_t z;
	};
} // END XPL

