/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Size.cuh"
#include "ISingleElement.cuh"
#include "IMemory.cuh"
#include "Region.cuh"

namespace xpl
{

	template<class T>
	class PinnedBuffer;
	template<class T>
	class TextureBuffer;
	template<class T>
	class HostBuffer;
	template<class T>
	class DeviceBuffer;

	/**
	*	IBuffer interface that all buffers adhere to
	*	@TODO: finish up documentaiton
	*/
	template<class T>
	class IBuffer 
	{

	public:
		virtual ~IBuffer() {};


		virtual void operator=(const PinnedBuffer<T>& other) = 0;
		virtual void operator=(const DeviceBuffer<T>& other) = 0;
		virtual void operator=(const TextureBuffer<T>& other) = 0;
		virtual void operator=(const HostBuffer<T>& other) = 0;
		/**
		*	@return string representation of the data, formatted for printing
		*/
		virtual operator std::string() const = 0;
		/**
		*	@return the number of row elements
		*/
		virtual uint rows() const = 0;
		/**
		*	@return the number of column elements
		*/
		virtual uint cols() const = 0;
		/**
		*	@return the Depth in the Z-direction
		*/
		virtual uint depth() const = 0;
		/**
		*	The pitch is the byte-alignment pitch in the fast memory direction
		*	@return the number of pitch elements in the X-direction (fast direction)
		*/
		virtual uint pitch() const = 0;
		/**
		*	Example: An element with index (i,j,k) would be accessed with:
		*	T value = data()[j + i*pitch() + k * pitch()*depthPitch() ];
		*	
		*	@return the pitch in number of elements in the the depth (Z-direction)
		*/
		virtual uint depthPitch() const = 0;
		/**
		*	@returns the buffer size information, same as rows/cols/depth
		*/
		virtual Size size() const = 0;
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0) = a(1) + b(4);
		*
		*	@param j column index of single element
		*	@return Single element modifier object
		*/
		//virtual ISingleElement<T>& operator()(uint j) = 0;
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0,0) = a(1,0) + b(4,2);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@return Single element modifier object
		*/
		//virtual ISingleElement<T>& operator()(uint i, uint j) = 0;
		/**
		*	Allows for element-wise data WRITE access 
		*	a(0,0,3) = a(1,0,7) + b(4,2,23);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@param k depth index of single element
		*	@return Single element modifier object
		*/
		//virtual ISingleElement<T>& operator()(uint i, uint j, uint k) = 0;

		#if __CUDA_ARCH__
		#else

		/**
		*	Allows for element-wise data READ access 
		*	a(0) = a(1) + b(4);
		*
		*	@param j column index of single element
		*	@return single scalar constant
		*/
		virtual /*__host__*/ const T operator()(uint j) const = 0;
		/**
		*	Allows for element-wise data READ access 
		*	a(0,0) = a(1,0) + b(4,2);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@return single scalar constant
		*/
		virtual /*__host__*/ const T operator()(uint i, uint j) const = 0;
		/**
		*	Allows for element-wise data READ access 
		*	a(0,0,3) = a(1,0,7) + b(4,2,23);
		*
		*	@param i row index of single element
		*	@param j column index of single element
		*	@param k depth index of single element
		*	@return single scalar constant
		*/
		virtual /*__host__*/ const T operator()(uint i, uint j, uint k) const = 0;
		#endif

	};

};











