/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once
#include "Position.cuh"
#include <sstream>
#include <ostream>

namespace xpl
{
	
	class Size
	{

	public:
		Size()
		: m_rows(0), m_cols(0), m_depth(0)
		{

		}
		Size(	uint rows, 
					uint cols,
					uint depth)
		: m_rows(rows), m_cols(cols), m_depth(depth)
		{
		}
		Size(	uint rows, 
					uint cols) 
		: m_rows(rows), m_cols(cols), m_depth(1)
		{
		}
		Size(const Size& other) 
		: m_rows(other.rows()), m_cols(other.cols()), m_depth(other.depth())
		{
		}
		~Size()
		{}
		uint getNbElements() const
		{
			return m_rows * m_cols * m_depth;	
		}
		bool operator==(const Size& other) const
		{
			bool same = m_rows == other.m_rows &&
						m_cols == other.m_cols &&
						m_depth == other.m_depth;
			return same;
		}
		bool operator!=(const Size& other) const
		{
			bool same = (*this)==other;
			return !same;
		}
		bool isInSide(const Position& p) const
		{
			bool ok = 
					p.x() < m_cols &&
					p.y() < m_rows &&
					p.z() < m_depth ;

			return ok;
		}
		uint rows() const 
		{
			return m_rows;
		}
		uint cols() const 
		{
			return m_cols;
		}
		uint depth() const 
		{
			return m_depth;
		}
		Size operator()(uint rows, uint cols, uint depth)
		{
			return Size(rows, cols, depth);
		}
		
	private:
		uint m_rows;
		uint m_cols;
		uint m_depth;
	};
	/**
	*	ToString implementation
	*/
	inline
	std::ostream & operator<<(std::ostream & Str, const Size& s) 
	{ 
		// print something from v to str, e.g: Str << v.getX();
		Str << " rows: " << s.rows();
		Str << " cols: " << s.cols();
		Str << " depth: " << s.depth();
		return Str;
	}



}