/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#pragma once

#include <memory>
#include <ostream>
#include <sstream>
#include "Region.cuh"
#include "HostSingleElement.cuh"
#include "DataDescriptor.cuh"
#include "DeviceRegionIterator.cuh"
#include "DeviceStruct.cuh"
#include "DeviceStreamBase.cuh"
#include "DeviceBuffer.cuh"
#include "TextureBuffer.cuh"
#include "device_foreach.cuh"
#include <CommonAssert.h>
#include "Position.cuh"
#include "PinnedMemory.cuh"
#include "HostBuffer.cuh"
#include "device_memcpy.cuh"
#include "cross_memcpy.cuh"
#include "iterator_memcpy.cuh"


namespace xpl
{
	template<class T>
	class HostBuffer;
	template<class T>
	class DeviceBuffer;
	template<class T>
	class TextureBuffer;
	/**
	*	Pinned buffer class for fast zero copy memory management (cudaHostAllocMapped).
	*\n
	*\n	These are buffers that can be efficiently read/written to/from by both the host (CPU) and device (GPU).
	*\n	
	*\n	-> Operators exists to directly modify individual elements from both host (CPU) and device (GPU kernels)
	*\n	-> Convenience functions for "for_each" operations, region iterators, and memcpy functions
	*\n	-> Device side operators and functions in conjunction with device_lambdas (see examples)
	*\n	-> buffer supports up to 3 dimensional data {rows, cols, deph}
	*\n		->The column dimension is always the fast X-dimension (memory layout wise)
	*\n	NOTE: for safe CPU data manipulation post GPU launches the CPU thread may query the device stream of
	*\n		this buffer.
	*/	
	template<class T>
	class PinnedBuffer : public DeviceStreamBase 
	{
	public:
		/**
		*	Construct a pinned buffer with @N column elements (size = 1xNx1)
		*	@param[in] N - number of elements
		*/
		PinnedBuffer(uint N);
		/**
		*	Construct a pinned buffer size = [rows]x[cols]x[depth]
		*	@param[in] N - number of elements
		*/
		PinnedBuffer(uint rows, uint cols, uint depth=1);
		/**
		*	Construct a pinned buffer 
		*	@param[in] size - size of buffer to create
		*/
		PinnedBuffer(const Size& size);
		/**
		*	Creates a pinned buffer that operates on already exisiting device memory @device_memory
		*	@param[in] device_memory - input pre-allocated device-memory
		*/
		PinnedBuffer(std::shared_ptr<PinnedMemory<T> > pinned_memory);
		/**
		*	Creates a pinned buffer that operates on already exisiting device memory @device_memory
		*	over a given region
		*	@param[in] device_memory - input pre-allocated device-memory
		*	@param[in] data_region - data_region to reference in @device_memory
		*/
		PinnedBuffer(std::shared_ptr<PinnedMemory<T> > pinned_memory, const Region& data_region);
		/**
		*	Creates a PinnedBuffer from a HostBuffer, allocates a pinned buffer and copies host
		*\n	memory to it 
		*	@param[in] hostBuffer - input host buffer to read from
		*/
		PinnedBuffer(const HostBuffer<T>& hostBuffer);
		~PinnedBuffer();
		/**
		*	@returns a buffer object that covers data_region
		*/
		std::shared_ptr<PinnedBuffer<T> > region(const Region& data_region) const;
		/**
		*	Example usage: 
		*	buffer_a(sub_region) = buffer_b(sub_region);
		*	@returns a region iterator to region r
		*/
		PinnedRegionIterator<T>& operator()(const Region& r);
		/**
		*	@param[in] lambda - device function to execute, funciton must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void for_each(Lambda lambda);
		/**
		*	@param[in] lambda - device function to execute, function must input value (Ex: [=]__device__(T& val))
		*/
		template<class Lambda> void iterate(Lambda lambda);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const PinnedBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const DeviceBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const TextureBuffer<T>& other);
		/**
		*	Copies contents of @other to this buffer
		*	@param[in] other buffer to read from
		*/
		void operator=(const HostBuffer<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*	ex: deviceBuffer = pinnedBuffer(someRegion);
		*/		
		void operator=(const PinnedRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const DeviceRegionIterator<T>& other);
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const TextureRegionIterator<T>& other);	
		/**
		*	Copies contents of @other REGION to this buffer
		*	@param[in] other buffer REGION to read from
		*/		
		void operator=(const HostRegionIterator<T>& other);	
		/**
		*	@return reference to this buffers memory
		*/
		const PinnedMemory<T>& memory() const;
		/**
		*	@return reference to this buffers memory
		*/
		PinnedMemory<T>& memory();
		/**
		*	@return string representation of the data
		*/
		operator std::string() const;
		/**
		*	@return size of the buffer
		*/
		Size size() const;

		#if DEVICE_COMPILE
			__device__ __forceinline__ int rows() const;
			__device__ __forceinline__ int cols() const;
			__device__ __forceinline__ int depth() const;
			__device__ __forceinline__ int pitch() const;
			__device__ __forceinline__ int depthPitch() const;
			__device__ __forceinline__ T* data() const;
			__device__ __forceinline__ void write(T value, int i, int j, int k);
			__device__ __forceinline__ T read(int i, int j, int k) const;
			/**
			*	@return reference to element at (j)
			*/
			__device__ __forceinline__ T& operator()(int j);
			/**
			*	@return reference to element at (i,j)
			*/
			__device__ __forceinline__ T& operator()(int i, int j);		
			/**
			*	@return reference to element at (i,j,k)
			*/
			__device__ __forceinline__ T& operator()(int i, int j, int k);
			/**	@return scalar reference at xpl::Index */
			__device__ __forceinline__ T& operator()(const xpl::Index& ind);
			/**
			*	@return scalar at (j)
			*/
			__device__ __forceinline__ const T operator()(int j) const;
			/**
			*	@return scalar at (i,j)
			*/
			__device__ __forceinline__ const T operator()(int i, int j) const;
			/**
			*	@return scalar at (i,j,k)
			*/
			__device__ __forceinline__ const T operator()(int i, int j, int k) const;
			/**	@return scalar at xpl::Index */
			__device__ __forceinline__ const T operator()(const xpl::Index& ind) const;
		#elif HOST_COMPILE
			__host__ uint rows() const;
			__host__ uint cols() const;
			__host__ uint depth() const;
			__host__ uint pitch() const;
			__host__ uint depthPitch() const;		
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint j) const;
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint i, uint j) const;
			/** See IBuffer.cu documentation */
			__host__ const T operator()(uint i, uint j, uint k) const;
			/**	@return scalar at xpl::Index */
			__host__ const T operator()(const xpl::Index& ind) const;
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0) = a(1) + b(4);
			*
			*	@param j column index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint j);
			/*
			*	Allows for element-wise data WRITE access 
			*	a(0,0) = a(1,0) + b(4,2);
			*
			*	@param i row index of single element
			*	@param j column index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint i, uint j);
			/**
			*	Allows for element-wise data WRITE access 
			*	a(0,0,3) = a(1,0,7) + b(4,2,23);
			*
			*	@param i row index of single element
			*	@param j column index of single element
			*	@param k depth index of single element
			*	@return Single element modifier object
			*/
			__host__ ISingleElement<T>& operator()(uint i, uint j, uint k);
			/**	@return scalar reference at xpl::Index */
			__host__ ISingleElement<T>& operator()(const xpl::Index& ind);
		#endif
	private:
		/**
		*	Setups up a device structure for usage on the GPU device
		*	The device struct defines device side reading and writing to this DeviceBuffer
		*	@return a deviceStruct instantized by this DeviceBuffer
		*/
		void initDevice(const PinnedMemory<T>& memory);
		/** Data container available for processing operations on the device*/
		DeviceStruct<T> device;
		/** pinned memory pointer */
		std::shared_ptr<PinnedMemory<T> > m_pinned_memory;
		/** Manages reading and writing to single device elements from the host */
		std::shared_ptr<HostSingleElement<T> > m_single_element_setter;
		/** Describes the data layout, provides linear indexing functionality*/
		std::shared_ptr< DataDescriptor<T>  > m_dataDescriptor;
		/** Region iterator, manages inter region iteration **/
		std::shared_ptr<PinnedRegionIterator<T> > m_region_iterator;
	};
	template<class T>
	std::ostream & operator<<(std::ostream & Str, const PinnedBuffer<T>& v);

} // END XPL
#include "PinnedBuffer.cut"


