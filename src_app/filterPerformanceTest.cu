/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file filterPerformanceTest.cu
*	This sample app runs mean-filtering with 3 different implementations and measures performance
* \n -> Filter using shared memory
* \n -> Filter without shared memory (aka "naive implementation")
* \n -> Filter using texture memory (aka "naive implementation")
*/

#include <xpl.cuh>
#include <stdio.h>  
#include <stdlib.h>
#include "TimingRecord.cuh"
#include "Options.h"
#include "Configurator.h"
#include "cudaSharedMemoryMeanFilterReference.cuh"


using namespace xpl;
using namespace utils; 
using namespace std;


/**
*	Measure performance while filtering using shared memory
*/
void filterWithSmem(const DeviceBuffer<float>& in, 
					DeviceBuffer<float> out,
					TimingRecord& record,
					int filterSize,
					int NB_ITERS)
{

	int fBorder = (filterSize-1)/2;
	int fs = -fBorder;
	int fe = fBorder;
	// Create device grid
	auto grid = DeviceGrid(in.size(), DeviceMapping(32,8)); 
	Smem<float> smem( grid, BlockStencil(fBorder, fBorder));
	//
	// Filter using shared memory
	//
	auto smem_filter = xpl_device_lambda()
	{
		smem.cache(in);

		int i = device::getY();
		int j = device::getX();

		if(i < in.rows()-fBorder && j < in.cols()-fBorder && i >= fBorder && j >= fBorder)
		{
			float sum = 0.0f;
			for(int y=fs; y <= fe; y++)
			{
				for(int x=fs; x <= fe; x++)
				{
					float val = smem(threadIdx.y+y, threadIdx.x+x);
					sum += val;
				}
			}
			out(i,j) = sum;  
		}
		else
		{
			if(i < in.rows() && j < in.cols() )
				out(i,j) = in(i,j);
		}
	};
	//
	// Execute kernel a few iterations and measure performance
	for(int i = 0; i < NB_ITERS; i++)
	{
		auto timer = record.get("smem_filter")->newTimer();
		timer->start();
		device::execute(grid, smem_filter);
		timer->stop();
	}
}

void filterWithoutSmem(const DeviceBuffer<float>& in, 
					DeviceBuffer<float> out,
					TimingRecord& record,
					int filterSize,
					int NB_ITERS)
{

	int fBorder = (filterSize-1)/2;
	int fs = -fBorder;
	int fe = fBorder;
	//
	// filter, no shared memory
	//
	auto no_smem_filter = xpl_device_lambda()
	{
		int i = device::getY();
		int j = device::getX();

		if(i < in.rows()-fBorder && j < in.cols()-fBorder && i >= fBorder && j >= fBorder)
		{
			float sum = 0.0f;
			for(int y=fs; y <= fe; y++)
			{
				for(int x=fs; x <= fe; x++)
				{
					float val = in(i+y, j+x);
					sum += val;
				}
			}
			out(i,j) = sum;  
		}
		else
		{
			if(i < in.rows() && j < in.cols() )
				out(i,j) = in(i,j);
		}
	};
	auto grid = DeviceGrid(in.size());
	//
	// Execute kernel a few iterations and measure performance
	for(int i = 0; i < NB_ITERS; i++)
	{
		auto timer = record.get("no_smem_filter")->newTimer();
		timer->start();
		device::execute(grid, no_smem_filter);
		timer->stop();
	}


}


void filterWithTextureMemory(const TextureBuffer<float>& tex_in, 
							DeviceBuffer<float> out,
							TimingRecord& record,
							int filterSize,
							int NB_ITERS)
{

	int fBorder = (filterSize-1)/2;
	int fs = -fBorder;
	int fe = fBorder;
	//
	// Filter using texture memory
	//
	auto texture_filter = xpl_device_lambda()
	{
		
		int i = device::getY();
		int j = device::getX();
		
		float sum = 0.0f; 
		for(int y=fs; y <= fe; y++)
		{
			for(int x=fs; x <= fe; x++)
			{
				float val = tex_in(i+y, j+x);
				sum += val;
			}
		}
		if( i < out.rows() && j < out.cols() )
			out(i,j) = sum;  
	};
	auto grid = DeviceGrid(tex_in.size());
	//
	// Execute kernel a few iterations and measure performance
	for(int i = 0; i < NB_ITERS; i++)
	{
		auto timer = record.get("texture_filter")->newTimer();
		timer->start();
		device::execute(grid, texture_filter);
		timer->stop();
	}
}
int main(int argc, char* argv[])
{

	util::Configurator config;

	auto options = config.optionController();

	options.setApplicationInfo("Test performance of different filters", "./bin/filterPerformanceTest [OPTIONS]");


	util::IntOption rowsOP("rows","r",util::Argument::REQUIRED, 1024, "Defines the number of rows to use in ROWS by COLS buffer");
	util::IntOption colsOP("cols","c",util::Argument::REQUIRED, 1024, "Defines the number of cols to use in ROWS by COLS buffer");
	util::IntOption filterSizeOP("filterSize","f",util::Argument::REQUIRED, 21, "Defines the filter size to use");
	util::IntOption testIters("testIters","i",util::Argument::REQUIRED, 5, "Defines the number of measurement iterations");
	util::FlagOption help("help","h",false, "Prints this help");

	options.addOption(&rowsOP);
	options.addOption(&colsOP);
	options.addOption(&filterSizeOP);
	options.addOption(&testIters);
	options.addOption(&help);
	options.parseCommandline(argc, argv);

	if( (bool)help)
	{
		options.printUsage();
		return 0;
	}
	std::cout << "\n Settings: ";
	options.printSettings();

	int rows = rowsOP;
	int cols = colsOP;

	int NB_ITERS = testIters;
	int filterSize = filterSizeOP;

	auto sz_str = std::to_string(filterSize);
	TimingRecord record("Filtering performance - size: " + sz_str + "x" + sz_str);
	record.setupEventTimer("smem_filter", 100);
	record.setupEventTimer("no_smem_filter", 100);
	record.setupEventTimer("texture_filter", 100);
	record.setupEventTimer("raw_cuda_filter", 100);

	DeviceBuffer<float> in(rows,cols);
	TextureBuffer<float> tex_in(rows,cols);
	DeviceBuffer<float> out(in.size());
	
	in.for_each([=]__device__(float& val){	val = 1.0f;}  );
	tex_in.for_each([=]__device__(float& val){	val = 1.0f;}  );
	//
	// Filter and do performance measurement:
	// 
	filterWithSmem(in, out, record, filterSize, NB_ITERS);
	filterWithoutSmem(in, out, record, filterSize, NB_ITERS);
	filterWithTextureMemory(tex_in, out, record, filterSize, NB_ITERS);

	cudaSharedMemoryMeanFilterReference(rows, cols, filterSize, record, NB_ITERS);

	CUDA_ERROR_ASSERT();

	// Get elapsed time etc:
	std::cout << record; 
	return 0;
} // END MAIN




