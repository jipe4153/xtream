/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file simpleTensorAddition.cu
*	This sample app performas vector addition A = B + C on the device
*/

#include <xpl.cuh>

using namespace xpl;

int main(int argc, char* argv[])
{
	int rows = 256;
	int cols = 256;
	int depth = 256;
	// Vectors A,B,C
	DeviceBuffer<int> A(rows,cols,depth);
	DeviceBuffer<int> B(A.size());
	DeviceBuffer<int> C(A.size());
	// Set B to 1 and C to '2' using for each lambda call
	B.for_each([=]__device__(int& val){val=1;});
	C.for_each([=]__device__(int& val){val=2;});
	//
	// Setup vector addition kernel:
	auto tensorAdditionKernel = xpl_device_lambda()
	{
		// (i,j,k) = (row,col,depth)
		int i = device::getY();
		int j = device::getX();
		int k = device::getZ();
		if( i < A.rows() && j < A.cols() && k < A.depth() )
		{
			A(i,j,k) = B(i,j,k) + C(i,j,k);
		}
	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(A.size(), tensorAdditionKernel);
	// Test:
	// We expect 'A' to be 1+2

	HostBuffer<int> h_A(A);

	bool ok = true;
	h_A.for_each( [&](int& val)
		{
			if(val!=3)
			{
				ok = false;
			}
		});
	std::cout << "\n Status: " << (ok ? "PASS" : "FAIL") << "\n";
}