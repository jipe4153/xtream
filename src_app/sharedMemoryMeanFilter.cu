/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file sharedMemoryMeanFilter.cu
*	This is CUDA reference code doing mean filtering using shared memory
*\n	Please see cudaSharedMemoryMeanFilterReference.cuh for a pure CUDA reference.
*/
#include <xpl.cuh>

using namespace xpl;

int main(int argc, char* argv[])
{
	int rows = 4096;
	int cols = 4096;
	// Input / output matrices
	DeviceBuffer<float> in(rows,cols);
	DeviceBuffer<float> out(in.size());
	
	in.for_each([=]__device__(float& val){val=1.0f;});
	out.for_each([=]__device__(float& val){val=0.0f;});

	// define grid, map to output size
	auto grid = DeviceGrid(out.size());
	// Setup shared memory, block stencil defines access to neighboring elements:
	Smem<float> smem( grid, BlockStencil(1,1));
	//
	// Setup vector increment kernel:
	auto meanFilter = xpl_device_lambda()
	{
		// Row/col indices (i,j)
		int i = device::getY();
		int j = device::getX();
		// Cache input data in shared memory
		smem.cache(in);
		//
		// Compute mean in 3x3 neighboorhood
		float sum = 0.0f;
		for(int y : {-1,0,1})
			for(int x : {-1,0,1})
				sum += smem(threadIdx.y + y, threadIdx.x + x);

		float mean = sum/9.0f;
		// output
		if( i < out.rows() && j < out.cols())
			out(i,j) = mean;

	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(grid, meanFilter);
	//
	// Test print corners:
	DeviceStream().synchronize(); // sync default stream
	std::cout << "\nTop-left corner: \n" << out(Region(0,10,0,10)) << "\n";
	std::cout << "\nTop-Right corner: \n" << out(Region(0,10,cols-10,cols-1)) << "\n";
	//
	// Validation:
	// Copy inner region of device output buffer to validate expected value
	Region innerRegion(1,rows-2, 1,cols-2);
	HostBuffer<float> h_out(innerRegion.size());
	h_out = out(innerRegion);

	bool ok = true;
	h_out.for_each([&]__host__(float& val)
	{
		if( std::abs(val-1.0f) > float(1E-6) )
		{
			ok =false;
		}
	});
	std::cout << "\nTest: " << (ok ? "PASS" : "FAIL") << "\n";
}