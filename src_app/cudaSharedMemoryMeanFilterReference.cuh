/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file cudaSharedMemoryMeanFilterReference.cuh
*	This is CUDA reference code doing mean filtering using shared memory
*	NOTE this is for reference only, does not contain XPL code
*/
#include "TimingRecord.cuh"

/**
*	Function sets up rows X cols buffers, sets input value to 1.0f and 
*	performs 3x3 filtering over these.
*/
void cudaSharedMemoryMeanFilterReference(int rows, int cols, 
										int filterSize, 
										xpl::utils::TimingRecord& record,  
										int NB_ITERS=1
										);

