
/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file mapToStdVectors.cu
*	
*/
#include <xpl.cuh>
#include <vector>

using namespace xpl;

void vectorAddition(std::vector<int>& vA, std::vector<int>& vB, std::vector<int>& vC)
{
    // Map to xtream HostBuffer objects
    auto mA = xpl::HostBuffer<int>::map(vA);
	auto mB = xpl::HostBuffer<int>::map(vB);
	auto mC = xpl::HostBuffer<int>::map(vC);		
    // Device Vectors A,B,C
    DeviceBuffer<int> A(mA); 
    DeviceBuffer<int> B(mB);
    DeviceBuffer<int> C(mC);
    // A = B + C
    auto vectorAdditionKernel = xpl_iterator(const Index& ind)
    {
    	A(ind) = B(ind) + C(ind);
    };
    // Execute the kernel, create grid mapped to output buffer 'A'
    device::iterate(A.size(), vectorAdditionKernel);
    // Copy back host memory
    mA = A;
}
int main(int argc,char* argv[])
{

	const int N = 1024*1024*8; // ~ 8m elements
	std::vector<int> vA(N);
	std::vector<int> vB(N);
	std::vector<int> vC(N);

	// Initialize B & C
	for(int& b : vB) 
	{
		b = 1;
	}
	for(int& c : vC) 
	{
		c = 2;
	}
	vectorAddition(vA, vB, vC);
	//
	// Test a
	bool ok = true;
	for(auto a : vA)
	{
		if(a != 3)
		{
			ok = false;
			break;
		}
	}
	std::cout << "Status: " << std::string(ok ? "PASS" : "FAIL") << "\n";

	return 0;
}

