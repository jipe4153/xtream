/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file simpleVectorAddition.cu
*	This sample app performs vector addition A = B + C on the device
*/

#include <xpl.cuh>
using namespace xpl;
int main(int argc, char* argv[])
{
	int N = 1024*256;
	// Vectors A,B,C
	DeviceBuffer<int> A(N); // Create 1xN vector
	DeviceBuffer<int> B(N);
	DeviceBuffer<int> C(N);
	// Set B to 1 and C to '2' using for each lambda call
	B.for_each([=]__device__(int& val){val=1;});
	C.for_each([=]__device__(int& val){val=2;});
	//
	// Setup vector addition kernel:
	auto vectorAdditionKernel = xpl_device_lambda()
	{
		// Index in X-dimension
		int j = device::getX(); // same as threadIdx.x + blockIdx.x*blockDim.x
		if( j < A.cols() )
		{
			A(j) = B(j) + C(j);
		}
	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(A.size(), vectorAdditionKernel);
	// Test:
	// We expect 'A' to be 1+2
	bool ok = true;
	for(int j = 0; j < N; j++)
	{
		// Read 'A' values directly from host side code
		if( A(j) != 3)
		{
			ok = false;
			break;
		}
	}
	std::cout << "\n Status: " << (ok ? "PASS" : "FAIL") << "\n";
}