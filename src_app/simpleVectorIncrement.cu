/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
/**
* @file simpleVectorIncrement.cu
*	This sample app performs vector increment on 'A' - A = A + 1
*/
#include <xpl.cuh>
using namespace xpl;

int main(int argc, char* argv[])
{
	int N = 1024*256;
	// Vectors A
	DeviceBuffer<int> A(1,N);
	A.for_each([=]__device__(int& val){val=1;});
	//
	// Setup vector increment kernel:
	auto vectorIncrementKernel = xpl_device_lambda()
	{
		// Index in X-dimension
		int j = device::getX(); // same as threadIdx.x + blockIdx.x*blockDim.x
		if( j < A.cols() )
		{
			A(j) = A(j) + 1;
		}
	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(DeviceGrid(A.size()), vectorIncrementKernel);
	// Test:
	// We expect 'A' to be 1+2
	bool ok = true;
	for(int j = 0; j < N; j++)
	{
		// Read 'A' values directly from host side code
		if( A(j) != 2)
		{
			ok = false;
			break;
		}
	}
	std::cout << "\n Status: " << (ok ? "PASS" : "FAIL") << "\n";
}