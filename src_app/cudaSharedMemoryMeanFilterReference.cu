/*******************************************************
* Copyright (c) 2013-2018, HPC Sweden
* Author: Jimmy Pettersson jimmy@hpcsweden.se
* All rights reserved.
*
* This file is distributed under 3-clause BSD license.
********************************************************/
#include "DeviceBuffer.cuh"
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>

#include "cudaSharedMemoryMeanFilterReference.cuh"



void check(float* host_ptr, int rows, int cols, int stencilSize)
{

    // Error test
    bool ok = true;
    for(int i = stencilSize; i < rows-stencilSize; i++)
    {
    	for(int j = stencilSize; j < cols-stencilSize; j++)
    	{
    		float val = host_ptr[j+i*cols];
    		if( std::abs(val-1.0f) > float(1E-6) )
    		{
    			ok = false;
    			printf("\n FAIL at: %d %d - %0.1f",i,j, val);
    		}

    		if(ok == false)
    			break;
    	}
    	if(ok == false)
    		break;
    }
    printf("\nTest: %s", (ok ? "PASS" : "FAIL"));





}


__global__ void memSetKernel(float* ptr, int rows, int cols, int pitch, float value)
{

	int j = threadIdx.x + blockIdx.x*blockDim.x;
	int i = threadIdx.y + blockIdx.y*blockDim.y;

	if( i < rows && j < cols)
	{
		ptr[j  + i * pitch] = value;
	}
}
// Reference to shared memory:
extern __shared__ uint8_t SMEM_BUFFER[];
__global__ void meanFilterKernel(float* in_ptr, 
								float* out_ptr,
								int rows,
								int cols,
								int pitch,
								int stencilSize)
{

	// Pointer to shared memory
	float* smem = (float*)SMEM_BUFFER;

	int x = threadIdx.x + blockIdx.x*blockDim.x;
	int y = threadIdx.y + blockIdx.y*blockDim.y;
	// Compute from / to indices of shared memory block
	int2 fromIdx;
	int2 toIdx;
	int2 smemSize;
	smemSize.x = blockDim.x+2*stencilSize;
	smemSize.y = blockDim.y+2*stencilSize;
	fromIdx.x = blockIdx.x*blockDim.x - stencilSize;
	fromIdx.y = blockIdx.y*blockDim.y - stencilSize;
	toIdx.x = fromIdx.x + smemSize.x - 1;
	toIdx.y = fromIdx.y + smemSize.y - 1;
	//
	// Fill shared memory:
	for(int i = fromIdx.y + threadIdx.y; i <= toIdx.y; i+=blockDim.y) // Y
	{
		for(int j = fromIdx.x+threadIdx.x; j <= toIdx.x; j+=blockDim.x) // X
		{
			// Clamp out of bounds values to ZERO
			bool doClamp = i < 0 || j < 0 || i >= rows || j >= cols;
			float val = doClamp ? 0.0f : in_ptr[j + i*pitch];
			
			// compute block local data indices:
			int sx = j - fromIdx.x;
			int sy = i - fromIdx.y;
			// store to smem:
			smem[sx + (sy)*smemSize.x] = val;
		}
	}
	__syncthreads();

	float sum = 0.0f;
	for(int sy = -stencilSize; sy <= stencilSize; sy++)
		for(int sx = -stencilSize; sx <= stencilSize; sx++)
		{
			sum += smem[ threadIdx.x + sx + stencilSize + (threadIdx.y + sy + stencilSize)*smemSize.x];
		}

	float rec_N = (float)(stencilSize*2+1)*(stencilSize*2+1);
	float mean = sum/rec_N;
	if( y < rows && x < cols)
	{
		out_ptr[x  + y * pitch] = mean;
	} // END if in bounds
}

void cudaSharedMemoryMeanFilterReference(int rows, int cols, 
										int filterSize, 
										xpl::utils::TimingRecord& record,  
										int NB_ITERS)
{
	//
	// Allocate buffers:
	size_t pitchInBytes = 0;
	float* in_ptr = NULL;
	float* out_ptr = NULL;
    cudaMallocPitch(	(void**)&(in_ptr),  &pitchInBytes, cols*sizeof(float),	rows);
    cudaMallocPitch(	(void**)&(out_ptr), &pitchInBytes, cols*sizeof(float),	rows);
    int pitch = pitchInBytes / sizeof(float);
    //
    // Define block size and grid size:
    int DIM_X = 32;
    int DIM_Y = 8;
    int X_BLOCKS = (DIM_X + cols - 1)/DIM_X; 
    int Y_BLOCKS = (DIM_Y + rows - 1)/DIM_Y; 
    dim3 grid(X_BLOCKS, Y_BLOCKS);
    dim3 threads(DIM_X, DIM_Y);
    CUDA_ERROR_ASSERT();
	//
	// Set 'in' values to ONE    
    memSetKernel<<< grid, threads, 0, 0>>>(in_ptr, rows, cols, pitch, 1.0f);
    CUDA_ERROR_ASSERT();

	// Set 'out' values to ZERO
    memSetKernel<<< grid, threads, 0, 0>>>(out_ptr, rows, cols, pitch, 0.0f);
    CUDA_ERROR_ASSERT();


    int stencilSize = (filterSize-1)/2;
    int elementsPerBlock = (DIM_X + 2*stencilSize) * (DIM_Y + 2*stencilSize);
    size_t bytesPerBlock = elementsPerBlock * sizeof(float);

	for(int i = 0; i < NB_ITERS; i++)
	{
		auto timer = record.get("raw_cuda_filter")->newTimer();
		timer->start();
	    meanFilterKernel<<< grid, threads, bytesPerBlock, 0>>>(in_ptr, out_ptr, rows, cols, pitch, stencilSize);
	    timer->stop();
	}

    CUDA_ERROR_ASSERT();

    float* host_ptr = (float*)malloc(rows*cols*sizeof(float));
    // Copy to host:
    cudaMemcpy2DAsync	(
 					host_ptr,					// dst ptr
 					cols*sizeof(float),			// dst pitch in bytes
 					out_ptr,					// src ptr
 					pitch*sizeof(float),		// src pich in bytes
					cols*sizeof(float), 		// width in bytes
					rows,						// height / rows
					cudaMemcpyDeviceToHost
					);

    CUDA_ERROR_ASSERT();
    check(host_ptr, rows, cols, stencilSize);
    free(host_ptr);
    cudaFree(in_ptr);
    cudaFree(out_ptr);
}