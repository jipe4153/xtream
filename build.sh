#!/bin/bash

git submodule update --init --recursive

# Empty input
if [ -z "$1" ] || [ "$1" == "RELEASE_MODE" ] ; then
	# DEFAULT
	build_var=RELEASE_MODE
elif [ "$1" == "DEBUG_MODE" ] || [ "$1" == "GPU_DEBUG_MODE" ] ; then
	build_var=$1
else
	echo "Valid arguments are RELEASE_MODE (default), DEBUG_MODE, GPU_DEBUG_MODE"
	exit
fi


mkdir build 
cd build/ 
cmake -D"$build_var"=ON ../
make -j11