[Xtream intro]: ../../doc/xtream_intro.pdf
[Utility package]: https://gitlab.com/jipe4153/std_utils
[googletest]: https://gitlab.com/jipe4153/googletest


# Brief #

Xtream is a header based C++ CUDA productivity library.

Core ***features***:

- Simplfied memory management for host, device, texture, pinned, and shared memory.
- Array slicing and inter-slice memory copies
- Simplified kernel creation using lambdas and iterator types.
- Automagic shared memory creation and caching
- Transparent host/device element access
- Thin wrappers: Full control and zero-overhead compared to native CUDA

Transferring data to the GPU and performing an operation can be as easy as in below ***snippet***:

```cpp
// Snippet from mapToStdVectors sample code:
void vectorAddition(std::vector<int>& vA, std::vector<int>& vB, std::vector<int>& vC)
{
    // Map to xtream HostBuffer objects
    auto mA = xpl::HostBuffer<int>::map(vA);
    auto mB = xpl::HostBuffer<int>::map(vB);
    auto mC = xpl::HostBuffer<int>::map(vC);		
    // Device Vectors A,B,C
    xpl::DeviceBuffer<int> A(mA); 
    xpl::DeviceBuffer<int> B(mB);
    xpl::DeviceBuffer<int> C(mC);
    // A = B + C
    auto vectorAdditionKernel = xpl_iterator(const xpl::Index& ind)
    {
    	A(ind) = B(ind) + C(ind);
    };
    // Execute the kernel, create grid mapped to output buffer 'A'
    xpl::device::iterate(A.size(), vectorAdditionKernel);
    // Copy back host memory
    mA = A;
}

```
A more complex use-case like filtering an image using ***shared memory*** can be done with just a few lines:
```cpp
// snippets from src_app/sharedMemoryMeanFilter.cu
void filterImage(const DeviceBuffer<float>& in,
                DeviceBuffer<float>& out)
{
	// define grid, map to output size
	auto grid = DeviceGrid(out.size());
	// Setup shared memory, block stencil defines access to neighboring elements:
	Smem<float> smem( grid, BlockStencil(1,1));
	auto meanFilter = xpl_device_lambda()
	{
		// Row/col indices (i,j)
		int i = device::getY();
		int j = device::getX();
		// Cache input data in shared memory
		smem.cache(in);
		// Compute mean in 3x3 neighboorhood
		float sum = 0.0f;
		for(int y : {-1,0,1})
			for(int x : {-1,0,1})
				sum += smem(threadIdx.y + y, threadIdx.x + x);
		float mean = sum/9.0f;
		// output
		if( i < out.rows() && j < out.cols())
			out(i,j) = mean;

	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(grid, meanFilter);
}
```


See code snippets below or [Xtream intro] for a quick introduction.

This repo features the following:

- **src/** - Core source code
- **src_test/** - Test code
- **src_app/** - Sample applications demonstrating usage

# Binary installation #

Xtream is header based so installation is very simple (all you need to do is include what's under xtream/src/).

Nonetheless binaries for installation have been compiled on a linux debian system (.deb, tar.gz, and .sh installation files). 
 Xtream is packaged using cmake so doing a proper install will mean that cmake can find xtream as an installed package on your system.

```sh
# Get the binaries (separate repo)
git clone git@gitlab.com:jipe4153/xtream-bin.git
cd xtream-bin/
# Debian install:
sudo dpkg -i xtream-1.2.3-Linux_x86_64.deb
# If not debian, you may simple untar:
tar -xvzf xtream-1.2.3-Linux_x86_64.tar.gz
```

# Installation from source #

To install from source just run the installation script:

```sh
# To get xtream with all of its source dependencies use "xtream-dev" meta repo:
git clone git@gitlab.com:jipe4153/xtream-dev.git
cd xtream-dev
# just do:
./install.sh
```

# Using xtream in your application #

If using cmake you need only find the package:

```cmake
find_package(xtream REQUIRED)
....
# Linking with your application:
add_executable(yourApp yourCode.cu)
target_link_libraries(yourApp PRIVATE xtream)
```
If not using cmake simply include *PREFIX*/include/xtream-*VERSION* (ex /usr/local/include/xtream-1.2.3/) to your build system.


# Creating DEB/RPM package: #

```sh
cd xtream
./build.sh
cd build
# Default is to create a DEB package:
make install_binary
# ...
# xtream/build/xtream-1.2.3-Linux_x86_64.deb generated
```
To build RPM packages do:

```sh
cmake -DPACKAGE_GENERATOR=RPM ../
make install_binary
```
# Building manually #

In order to manually build the tests and sample code etc do:
```sh
cd xtream
mkdir build 
cd build/ 
# Setup submodules (only needed once)
git submodule update --init --recursive
cmake  ../
# Build tests and source applications
make -j11
# build the documentation (doxygen)
make doc
```

***NOTE:*** [Utility package] and [googletest] are required to be installed first, exact version is managed by xtream-dev meta package.


To ***run applications***

```sh
cd xtream/build
# Run the tests:
./bin/xtream_test
# Run apps
./bin/simpleVectorAddition
./bin/<SOME_APP>
```

Different cmkale build modes:

- ***Release*** - standard release with full optimizations (default)  (-DCMAKE_BUILD_TYPE=Release)
- ***Debug*** - Host side asserts and CUDA error checking on from host are performed (-DCMAKE_BUILD_TYPE=Debug)
- ***GPU Debug*** - Host AND device side asserts are applied (-DCMAKE_BUILD_TYPE=Debug -DGPU_DEBUG_MODE=ON)

Apply these modes to the test and src_apps using:

```sh
cd xtream/build
cmake -DCMAKE_BUILD_TYPE=Debug ../
make -j11
```

# Build requirements #

- ***CUDA toolkit*** To build your own application you will need the CUDA toolkit >= 8.0.
- ***CMake*** Xtream has been tested with cmake >= 10.2

# Code snippets #

Lets look at some code snippets, its recommended to look into [Xtream intro] for more details.

## Memory basics: Array slicing and handy operators ##

Global memory types:
- ***HostBuffer*** 	– Regular CPU memory container
- ***DeviceBuffer*** 	– Regular pitched GPU memory
- ***TextureBuffer*** – Texture fetching, writes to surfaces
- ***PinnedBuffer*** 	– host mapped memory (“zero-copy”)
 
```cpp
#include <xpl.cuh>
using namespace xpl;                        // Xtream Performance Library

uint rows = 33;
uint cols = 37;
uint depth = 44;

HostBuffer<int> h1(rows, cols, depth);      // xpl buffers support up to 3 dimensions
DeviceBuffer<int> d1(h1.size());
TextureBuffer<int> t1(h1.size());       

d1 = h1;                                    // Copy host to device
h1 = d1;                                    // Copy device to host
DeviceBuffer<int> d2(h1);                   // instantiate and copy in one-step

Region subRegion(0,9,0,9);                  // 10x10 sub-region (0:9,0:9)
DeviceBuffer<int> d3(subRegion.size());
d3 = h1(subRegion);                         // Copy array slice 'h1' to 'd3' (host to device)
d1(subRegion) = d3;                         // Copy to sub-region (device to device)
h2(subRegion) = d3;                         // Copy to sub-region (device to host)

d3(0,8) = d1(0,22,3) + h1(1,2) + t1(0,33);  // Seamless device/host access
std::cout << d3(0,8);                       // print value
std::cout << d3;                            // print 10x10 matrix
std::cout << d3(Region(0,3,0,3));           // print 4x4 region
```

## Vector addition ##

Another full example of doing vector addition using the generic *xpl_device_lambda()* to create a kernel.

```cpp
//  taken from simpleVectorAddition.cu
#include <xpl.cuh>
using namespace xpl;
int main(int argc, char* argv[])
{
	int N = 1024*256;
	// Vectors A,B,C
	DeviceBuffer<int> A(N); // Create 1xN vector
	DeviceBuffer<int> B(N);
	DeviceBuffer<int> C(N);
	// Set B to 1 and C to '2' using for each lambda call
	B.for_each([=]__device__(int& val){val=1;});
	C.for_each([=]__device__(int& val){val=2;});
	// Setup vector addition kernel:
	auto vectorAdditionKernel = xpl_device_lambda()
	{
		// Index in X-dimension
		int j = device::getX(); // same as threadIdx.x + blockIdx.x*blockDim.x
		if( j < A.cols() )
		{
			A(j) = B(j) + C(j);
		}
	};
	// Execute the kernel, create grid mapped to output buffer 'A'
	device::execute(A.size(), vectorAdditionKernel);
	// We expect 'A' to be 1+2
	bool ok = true;
	for(int j = 0; j < N; j++)
	{
		// Read 'A' values directly from host side code
		if( A(j) != 3) { ok = false; break; }
	}
	std::cout << "\n Status: " << (ok ? "PASS" : "FAIL") << "\n";
}
```

### Shared memory ###
Xtream features simplified shared memory caching, which makes using shared memory is dead simple for the most common applications.

```cpp
int rows = 4096;
int cols = 4096;
// Input / output matrices
DeviceBuffer<float> in(rows,cols);
DeviceBuffer<float> out(in.size());
in.for_each([=]__device__(float& val){val=1.0f;});
out.for_each([=]__device__(float& val){val=0.0f;});
// define grid, map to output size
auto grid = DeviceGrid(out.size());
// Setup shared memory, block stencil defines access to neighboring elements:
Smem<float> smem( grid, BlockStencil(1,1));
// Setup vector increment kernel:
auto meanFilter = xpl_device_lambda()
{
	// Row/col indices (i,j)
	int i = device::getY();
	int j = device::getX();
	// Cache input data in shared memory
	smem.cache(in);
	//
	// Compute mean in 3x3 neighboorhood
	float sum = 0.0f;
	for(int y : {-1,0,1})
		for(int x : {-1,0,1})
			sum += smem(threadIdx.y + y, threadIdx.x + x);
	float mean = sum/9.0f;
	// output
	if( i < out.rows() && j < out.cols())
		out(i,j) = mean;
};
// Execute the kernel, create grid mapped to output buffer 'A'
device::execute(grid, meanFilter);
```



Note that xpl::Smem can be configured to handle more complex cases.